if (!String.prototype.format) {
	String.prototype.format = function () {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function (match, number) {
			return typeof args[number] != 'undefined'
					? args[number]
					: match
					;
		});
	};
}

$(function() {
   $("eml").each(function() {
       var email = $(this).html() + '@fave.bg';
       $(this).after('<a href="mailto:'+email+'" style="font-weight: bold;">'+email+'</a>');
       $(this).remove();
   });
});