<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Notifications extends CI_Controller {

	public function __construct() {
		parent::__construct();
//		$this->load->model('post_model');
//		$this->load->model('tips_model');
	}

	public function index() {
		$title = "Test";
		$data_seo = array(
			"title" => $title
		);
		$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
		$seo = $this->seo_model->make_seo($data_seo);
		$header_data = array_merge($header_data, $seo);
		$this->load->view('main_templates/header', $header_data);
		$this->load->view('index_content');
		$this->load->view('users/notifications/msgs');
//					$this->load->view('posts/post_content', $data);
//					$this->load->view('main_templates/masonry', $data);
		$this->load->view('main_templates/footer');
		$this->load->view('main_templates/common_footer');
	}

}

?>