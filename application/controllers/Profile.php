<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('post_model');
		$this->load->model('tips_model');
	}

	public function index($selector, $type = "") {
		$this->session->set_userdata('last_url', base_url(uri_string()));
		$result = $this->users_model->selector_sex($selector);
		if ($result["sex"] == "b" && $type == "") {
			$type = "catalog";
		}
		$this->users_model->alias_profile($selector, $type);
	}

	public function dofollow() {
		if (IS_AJAX) {
			if ($this->session->userdata('logged_in')) {
				$selector = $this->input->post("selector");
				$result = $this->users_model->toggle_follow($selector);
				echo json_encode($result);
			} else {
				echo json_encode(array("type" => "login"));
			}
		}
	}
}

?>