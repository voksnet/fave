<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url', 'file'));
		$this->load->model('upload_model');
		$this->load->model('tips_model');
		$this->load->model('post_model');
	}

	public function index() {
		$this->session->set_userdata('last_url', base_url(uri_string()));
		$this->users_model->not_logged(true, base_url() . "login");
		//load categories and 
		$data['categories'] = $this->post_model->get_all_cats();
		if ($this->session->userdata("alias") != null && $this->session->userdata("alias") != "") {
			$data["success_page"] = base_url() . $this->session->userdata("alias") . "#uploaded";
		} else {
			$data["success_page"] = base_url() . $this->config->item("my_urlparams_user") . "/" . $this->session->userdata("selector") . "#uploaded";
		}



		$title = $this->lang->line("fe_seo_upload");
		$data_seo = array(
			"title" => $title
		);
		$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
		$seo = $this->seo_model->make_seo($data_seo, true);
		$header_data = array_merge($header_data, $seo);
		$this->load->view('main_templates/header', $header_data);

		$this->load->view('upload/upload', $data);
		$this->load->view('main_templates/simple_footer');
		$this->load->view('main_templates/common_footer');
	}

	public function delTree($dir) {
		$files = array_diff(scandir($dir), array('.', '..'));
		foreach ($files as $file) {
			(is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}

	private function unknowError($message = "") {
		$error = array();
		if ($message == "") {
			$message = "Unknown error";
		}
		$error["files"][0]['error'] = $message;

		echo json_encode($error);
		die();
	}

	public function do_upload() {
		if ($this->session->userdata('logged_in')) {
			//$upload_path_url = base_url() . $this->config->item("my_temp_upload_folder");
			$config['upload_path'] = FCPATH . $this->config->item("my_temp_upload_folder");
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size'] = $this->config->item("my_max_filesize_upload");
			$config['min_width'] = $this->config->item("fe_imageupload_min_w");
			$config['min_height'] = $this->config->item("fe_imageupload_min_h");
			if ($this->input->post("web_image") != "") {
				$up_result = $this->upload_model->upload_web_image($this->input->post("web_image"));
				if (isset($up_result["unknown_error"])) {
					$this->unknowError();
				};
				$files = $up_result["files"];
				//$data = $up_result["data"];
				$new_filename = $up_result["new_filename"];
//				//MYTODO: convert to function
//				$mime_types = array(
//					// images
//					'image/png' => 'png',
//					'image/jpeg' => 'jpe',
//					'image/jpeg' => 'jpeg',
//					'image/jpeg' => 'jpg'
////					,'image/gif' => 'gif'
//				);
//				error_reporting(0);
//				$remote_image_info = getimagesize($this->input->post("web_image"));
//				$env = ENVIRONMENT;
//				enviroment($env);
//				if ($remote_image_info=="") {
//					$remote_image_info= $this->curl_mime($this->input->post("web_image"));
//				}
//				if (isset($remote_image_info["mime"]) && stristr($remote_image_info["mime"], "image")) {
//					//check if image type is valid
//					if ($mime_types[$remote_image_info["mime"]] != "") {
//						//check if it is in allowed dimmensions
//						if ($remote_image_info[0] >= $this->config->item("my_imageupload_min_w") && $remote_image_info[1] >= $this->config->item("my_imageupload_min_h")) {
//							
//							if (!isset($remote_image_info["image_data"])) {
//								$image = file_get_contents($this->input->post("web_image"));
//							} else {
//								$image = $remote_image_info["image_data"];
//							}
//							if (!is_dir(FCPATH . $this->config->item("my_temp_upload_folder"))) {
//								mkdir(FCPATH . $this->config->item("my_temp_upload_folder"), 0755, true);
//							};
//							$new_upload = FCPATH . $this->config->item("my_temp_upload_folder") . $new_filename . "." . $mime_types[$remote_image_info["mime"]];
//							file_put_contents($new_upload, $image);
//							$data['full_path'] = $new_upload;
//							$data['file_name'] = $new_filename . "." . $mime_types[$remote_image_info["mime"]];
//							$files = $this->upload_model->imageResize($data);
//							
//							
//							if (file_exists($new_upload)) {
//								unlink($new_upload);
//							}
//						} else {
//							$this->unknowError();
//						}
//					} else {
//						$this->unknowError();
//					}
//				} else {
//					$this->unknowError();
//				}
			} else {
				$new_filename = md5($this->session->userdata('selector') . time() . rand(1000, time()) . $this->input->ip_address());
				$config['file_name'] = "tmp_" . $new_filename; //new file name ignoring the input filename 
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload()) {

					$existingFiles = get_dir_file_info($config['upload_path']);
					$foundFiles = array();
					$f = 0;
					foreach ($existingFiles as $fileName => $info) {
						if ($fileName != 'thumbs') {//Skip over thumbs directory
							if (!$this->upload->is_allowed_dimensions()) {
								$foundFiles[$f]['error'] = $this->lang->line("fe_imageupload_small_image");
							} else {
								$foundFiles[$f]['error'] = null;
							}
							$f++;
						}
					}
					if (!$this->upload->is_allowed_dimensions()) {
						$foundFiles[0]['error'] = $this->lang->line("fe_imageupload_small_image");
					}
					$this->output
							->set_content_type('application/json')
							->set_output(json_encode(array('files' => $foundFiles)));
				} else {
					$data = $this->upload->data();
					$files = $this->upload_model->imageResize($data);
				}
			}
			//this is why we put this in the constants to pass only json data
			if (!isset($files["files"][0]['error']) || is_null($files["files"][0]['error'])) {

				$add_tip = false;
				$insert_data = [];
				$insert_data["sex"] = $this->input->post("sex");
				$insert_data["comment"] = $this->input->post("comment");
				$insert_data["tags"] = trim($this->input->post("tags"), ",");

				if (stristr($insert_data["tags"], ",")) {
					$tags_array = explode(",", $insert_data["tags"]);
					$tags_count = 0;
					foreach ($tags_array as $tag_key => $tag_string) {
						$tag_string = mb_convert_case(trim($tag_string), MB_CASE_LOWER, "UTF-8");
						$tags_array[$tag_key] = $tag_string;
						$this->check_tag($tag_string);
						$tags_count++;
					}
					if ($tags_count > $this->config->item("my_max_tags")) {
						$this->unknowError(sprintf($this->lang->line("fe_upload_too_many_tags"), $this->config->item("my_max_tags")));
					}
					$insert_data["tags"] = implode(",", $tags_array);
				} else {
					$insert_data["tags"] = mb_convert_case(trim($insert_data["tags"]), MB_CASE_LOWER, "UTF-8");
					$this->check_tag($insert_data["tags"]);
				}

				$insert_data["categoriesFK"] = $this->input->post("category");

				//make sponsored post
				if ($this->input->post('sponsored') != null && (($this->session->userdata("sex") == "b" && $this->input->post('url') != null && trim($this->input->post('url') != "")) || ( $this->input->post('url') != null && trim($this->input->post('url') != "") && ($this->session->userdata("user_type") == "admin")))) {
					$insert_data["url"] = trim($this->input->post('url'));
					$insert_data["post_type"] = "sponsored";

					if ($this->input->post('post_price') != 0 && $this->input->post('post_price') > 0 && $this->input->post('post_currency') != "") {
						$insert_data["post_price"] = $this->input->post('post_price');
						$insert_data["post_currency"] = $this->input->post('post_currency');
					}
				} else
				if ($this->input->post('sponsored') == null && $this->input->post('url') != null && trim($this->input->post('url')) != "" && $this->input->post('post_price') != null && $this->input->post('post_currency') != null) {
					//make a tip
					$add_tip = true;
				}

				$insert_data["usersFK"] = $this->session->userdata('id');
				$insert_data["users_selectorFK"] = $this->session->userdata('selector');
				$insert_data["image"] = $new_filename . ".jpg";

				//validate tags
//				if (stristr($insert_data["tags"], ",")) {
//					$tags_array = explode(",", $insert_data["tags"]);
//					$tags_count = 0;
//					foreach ($tags_array as $tag_key => $tag_string) {
//						$tag_string = mb_convert_case(trim($tag_string), MB_CASE_LOWER, "UTF-8");
//						$tags_array[$tag_key] = $tag_string;
//						$this->check_tag($tag_string);
//						$tags_count++;
//					}
//					if ($tags_count > $this->config->item("my_max_tags")) {
//						$this->unknowError(sprintf($this->lang->line("fe_upload_too_many_tags"), $this->config->item("my_max_tags")));
//					}
//					$insert_data["tags"] = implode(",", $tags_array);
//				} else {
//					$insert_data["tags"] = mb_convert_case(trim($insert_data["tags"]), MB_CASE_LOWER, "UTF-8");
//					$this->check_tag($insert_data["tags"]);
//					$tags_array[] = $insert_data["tags"];
//				}
//
//				$cats = $this->post_model->get_all_cats(true, false, true);
//				$insert_data["cat_strings"] = ",";
//				$cats_in_post = explode(",", $insert_data["category"]);
//				foreach ($cats_in_post as $c_v) {
//					if (isset($cats[$c_v])) {
//						$insert_data["cat_strings"] .= $cats[$c_v] . ",";
//					}
//				}
//				$insert_data["usersFK"] = $this->session->userdata('id');
//				$insert_data["users_selectorFK"] = $this->session->userdata('selector');
//				$insert_data["image"] = $new_filename . ".jpg";
//				$insert_data["category"] = $insert_data["category"];
//				$insert_data["categoriesFK"] = "," . $insert_data["category"] . ",";
//				$insert_data["tags"] = "," . $insert_data["tags"] . ",";
//				$insert_data["tags_array"] = $tags_array;
//				if ($this->session->userdata("sex") == "b" && isset($post_data['url']) && trim($post_data['url'] != "")) {
//					$data["url"] = trim($post_data['url']);
//					$data["post_type"] = "sponsored";
//					if ($post_data['post_price'] != 0 && $post_data['post_price'] > 0 && $post_data['post_currency'] != "") {
//						$data["post_price"] = $post_data['post_price'];
//						$data["post_currency"] = $post_data['post_currency'];
//					}
//				}
//				$image_data = getimagesize(FCPATH . $this->config->item("my_upload_big_images") . substr($insert_data["image"], 0, 2) . "/" . $insert_data["image"]);
//				$insert_data["image_width"] = $image_data[0];
//				$insert_data["image_height"] = $image_data[1];

				if ($this->session->userdata('logged_in')) {
					$post_id = $this->post_model->addPost($insert_data);
					if ($add_tip) {
						//add tip
						$tips_data = array();
						$tips_data["post_id"] = $post_id;
						$tips_data["cat_id"] = $insert_data["categoriesFK"];
						$tips_data["tip_image_url"] = trim($this->input->post("web_image"));
						$tips_data["tip_url"] = trim($this->input->post('url'));;
						$tips_data["currency"] = $this->input->post('post_currency');
						$tips_data["tip_price"] = $this->input->post ('post_price');
						$tips_data["usersFK"] = $insert_data["usersFK"];
						$tips_data["users_selectorFK"] = $insert_data["users_selectorFK"];
						$tips_data["tip_brand"] = $this->session->userdata('brand_name');
						$this->tips_model->add_tip($tips_data);
					}
				}
			}
			if (IS_AJAX) {
				if ($this->session->userdata("alias") != null && trim($this->session->userdata("alias")) != "") {
					$redirect = base_url() . $this->session->userdata('alias') . "#uploaded";
				} else {
					$redirect = base_url() . $this->config->item("my_urlparams_user") . "/" . $this->session->userdata('selector') . "#uploaded";
				}
				echo json_encode(array("files" => $files, "redirect" => $redirect));
//this has to be the only data returned or you will get an error.
				//if you don't give this a json array it will give you a Empty file upload result error
				//it you set this without the if(IS_AJAX)...else... you get ERROR:TRUE (my experience anyway)
				// so that this will still work if javascript is not enabled
			} else {
				//$file_data['upload_data'] = $data;
				//$this->load->view('upload/upload_success', $file_data);
			}
		} else {
			//$foundFiles[$f]['error'] = $this->lang->line("fe_imageupload_small_image");
			if (IS_AJAX) {
				$error = array();
				$error["files"][0]['error'] = $this->lang->line("fe_session_error_user");
				echo json_encode($error);
			}
		}
	}

	private function check_tag($tag) {
		if (mb_strlen($tag, "utf8") < $this->config->item("my_min_tag_length")) {
			$this->unknowError(sprintf($this->lang->line("fe_upload_short_tag"), $this->config->item("my_min_tag_length"), $this->config->item("my_max_tag_length")));
		} else if (mb_strlen($tag, "utf8") > $this->config->item("my_max_tag_length")) {
			$this->unknowError(sprintf($this->lang->line("fe_upload_long_tag"), $this->config->item("my_min_tag_length"), $this->config->item("my_max_tag_length")));
		}
	}

	public function deleteImage($file) {//gets the job done but you might want to add error checking and security
		//die(FCPATH . 'uploads/' . $file);
		$file = urldecode($file);
		$success = false;
		$success = unlink(FCPATH . $this->config->item("my_temp_upload_folder") . $file);
		//info to see if it is doing what it is supposed to
		$info = new StdClass;
		$info->sucess = $success;
		$info->path = base_url() . $this->config->item("my_temp_upload_folder") . $file;
		$info->file = is_file(FCPATH . $this->config->item("my_temp_upload_folder") . $file);

		if (IS_AJAX) {
			//I don't think it matters if this is set but good for error checking in the console/firebug
			echo json_encode(array($info));
		} else {
			//here you will need to decide what you want to show for a successful delete        
			$file_data['delete_data'] = $file;
			$this->load->view('upload/delete_success', $file_data);
		}
	}

	public function remote_filesize() {
		if (IS_AJAX) {
			$this->load->helper("remote-file");
			$filesize = curl_get_file_size($this->input->post("url"));
			$result = array();
			if ($filesize == -2) {
				$filesize = $this->config->item("my_max_filesize_upload");
			}
			if ($filesize != -1 && $filesize != 0) {
				$max_size = round($this->config->item("my_max_filesize_upload") / 1000, 0);
				$formated_size = round($filesize / 1000, 0);
				if ($filesize > $this->config->item("my_max_filesize_upload")) {
					$result["filesize"] = $filesize;
					$result["error"] = true;
					$result["error_message"] = sprintf($this->lang->line("fe_imageupload_big_file"), $formated_size, $max_size, $this->lang->line("fe_imageupload_big_file"));
				} else {
					$result["filesize"] = $filesize;
					$result["error"] = false;
				}
			} else {
				$result["filesize"] = 0;
				$result["error"] = true;
				$result["error_message"] = $this->lang->line("fe_imgupload_invalid_url");
			}
			echo json_encode($result);
		}
	}

	public function search_tags() {
		if (IS_AJAX) {
			if (strlen($this->input->post("term")) >= 2) {
				$data = $this->post_model->suggestTags($this->input->post("term"));
				if ($data != false) {
					$result = [];
					foreach ($data as $tag) {
						array_push($result, array("value" => $tag));
					}
					echo json_encode($result);
				}
			}
		}
	}

	public function remote_images() {
		//count_post_tips($post_id)
		if (IS_AJAX) {
			$result = $this->upload_model->get_remote_images($this->input->post("url"));
			echo json_encode($result);
		}
	}

}
