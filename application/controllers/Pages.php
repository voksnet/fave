<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->model('post_model');
//        $this->load->model('tips_model');
        
        $this->lang->load("faq");
    }
    
    function index($page) {
        $data = new stdClass();
        $data->num_sidebar_items = 3;
        
        if ($page == "") {
            show_404();
        } else {
            switch($page) {
                case "faq":
                    $data->title = $this->lang->line("faq_title");
                    $data->subtitle = $this->lang->line("faq_subtitle");
                    $data->content = $this->load->view("pages/faq", '', true);
                    break;
                case "contacts":
                    $data->title = $this->lang->line("contacts_title");
                    $data->subtitle = $this->lang->line("contacts_subtitle");
                    $data->content = $this->load->view("pages/contacts", '', true);
                    break;
                case "terms":
                    $data->title = $this->lang->line("terms_title");
                    $data->subtitle = $this->lang->line("terms_subtitle");
                    $data->content = $this->load->view("pages/terms", '', true);
                    $data->num_sidebar_items = 5;
                    break;
                case "help":
                    $data->title = $this->lang->line("help_title");
                    $data->subtitle = $this->lang->line("help_subtitle");
                    $data->content = $this->load->view("pages/help", '', true);
                    break;
                case "about":
                    $data->title = $this->lang->line("about_title");
                    $data->subtitle = $this->lang->line("about_subtitle");
                    $data->content = $this->load->view("pages/about", '', true);
                    $data->num_sidebar_items = 2;
                    break;
                case "how":
                    $data->title = $this->lang->line("how_title");
                    $data->subtitle = $this->lang->line("how_subtitle");
                    $data->content = $this->load->view("pages/how", '', true);
                    $data->num_sidebar_items = 2;
                    break;
                default:
                    show_404();
            }
        }
        
        $data_seo = array(
            "title" => $data->title,
            "image" => base_url() . $this->lang->line("fe_seo_default_image"),
            "description" => mb_substr(str_replace("\n", " ", strip_tags($data->content)), 0, 160) . "...", //$this->lang->line("fe_seo_site_description")
            "image:width" => "1200",
            "image:height" => "630"
        );
		
        $header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
        $seo = $this->seo_model->make_seo($data_seo);
        $header_data = array_merge($header_data, $seo);
        $this->load->view('main_templates/header', $header_data);
        $this->load->view('index_content');
        $posts = $this->post_model->get_msnry(array("featured_static" => true, "limit" => $data->num_sidebar_items - 1));
        $data->posts = $posts["posts"];

        $this->load->view("pages/wrapper", array("data" => $data));

        $this->load->view('main_templates/footer');
        $this->load->view('main_templates/common_footer');
    }

    function do_contact() {

            $ret = array("error" => false);

            if (filter_input(INPUT_POST, "last_name") != "") { // honeypot check fail
                    echo json_encode($ret);
                    return;
            }

            $params = new stdClass();
            $params->name = filter_input(INPUT_POST, "first_name");
            $params->email = filter_input(INPUT_POST, "email");
            $params->message = filter_input(INPUT_POST, "message");
            $params->subject = "Fave.bg contact";

            $this->load->model("mailer_model", "mailer", true);
            $result = $this->mailer->internal($params, ["bobev@zero.bg"]);

            if (!is_int($result)) {
                    $error_text = $this->lang->line("fe_contacts_sent_error");
                    if ($result !== false) {
                            $error_text .= "<br>" . $result;
                    }
                    $ret = array("error" => true, "text" => $error_text);
                    echo json_encode($ret);
                    return;
            }

            echo json_encode($ret);
            return;
    }

}

?>
