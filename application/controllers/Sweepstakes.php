<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sweepstakes extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect();
        $this->load->view("sweepstakes/sweepstakes");
    }

    public function enter() {
        $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
        
        header("Content-Type: application/json;charset=utf-8");
        
        if (!$email) {
            echo json_encode(array("error" => true, "text" => "Невалиден имейл адрес.\n\nМоля опитайте отново."));
            return;
        }
        
        $res = $this->db->get_where("sweepstakes", array("email" => $email, "del" => "no"));
        if ($res->num_rows()) {
            echo json_encode(array("error" => true, "text" => "Вие вече сте записани за играта.\n\nА харесахте ли и споделихте ли страницата?"));
            return;
        }
        
        $data = array(
            "email" => $email
        );
        
        $user = $this->db->get_where("users", array("email" => $email, "del" => "0"));
        if ($user->num_rows()) {
            $data['userFK'] = $user->row()->id;
        }
        
        $this->db->insert("sweepstakes", $data);
        
        echo json_encode(array("error" => false));
    }
}

?>