<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class scrape extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('scrape_model');
	}
	public function index($switch,$brand="",$limit=80) {
		switch ($switch) {
			case "scrape":
				echo "insert scrape\n";
				$this->scrape_model->add_scrape($this->input->post("data"));
				break;
			case "add_posts":
				if ($brand != "" && $brand != "0") {
					echo "add_random posts from brand: ".$brand."\n";
					$this->scrape_model->add_posts($limit, $brand);
				} else {
					echo "add_random_posts\n";
					$this->scrape_model->add_posts($limit);
				}
				break;
		}
	}

}

?>