<div class="page-faq">
    <?php
    $faq = $this->lang->line("faq_content");

    foreach ($faq as $fq) {
        echo '<div>';
        echo '<div class="question">'.$fq['question'].'</div>';
        echo '<div class="answer"><div>'.nl2br($fq['answer']).'</div></div>';
        echo '</div>';
    }

    ?>
</div>

<script>
    $(function() {
        $('.page-faq .answer').hide();
        $(".page-faq .question").click(function() {
            $(this).parent().siblings().find('.answer').hide();
            if($(this).next('.answer').is(':visible')) {
                $(this).next('.answer').slideUp('fast');
            } else {
                $(this).next('.answer').slideDown('fast');
            }
        });
    });
</script>