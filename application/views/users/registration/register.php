<div class="clear_header"></div>
<div class="wrap-fullwidth the_content">
	<div class="single-content full">
		<div id="content">
			<div class="register-container ml">

				<h1><?= $register_type == "register" ? $this->lang->line('fe_register_push_2') : $this->lang->line('fe_register_edit'); ?></h1>
				<?= isset($has_error)?'<div class="error">'.$has_error.'</div>':""; ?>
				<br>
				<?php
				if (isset($avatar_folder)) {
					?>
					<div class="row-wrapper avatar">
						<div class="avatar_wrapper">
							<div class="the_avatar">
								<img src="<?= $avatar_folder . (isset($init["avatar"]) ? $init["avatar"] : set_value('avatar')); ?>">
							</div>
						</div>
						<link rel="stylesheet" href="<?= base_url()?>avup/aui/css/aui-all.css" media="all">
						<!--[if lt IE 9]><link rel="stylesheet" href="aui/css/aui-ie.css" media="all"><![endif]-->
						<!--[if IE 9]><link rel="stylesheet" href="aui/css/aui-ie9.css" media="all"><![endif]-->
						<link rel="stylesheet" type="text/css" href="<?= base_url() . "avup/" ?>css/image-explorer.css">
						<link rel="stylesheet" type="text/css" href="<?= base_url() . "avup/" ?>css/image-upload-and-crop.css">
						<link rel="stylesheet" type="text/css" href="<?= base_url() . "avup/" ?>lib/fd-slider/fd-slider.css">

						<div id="avup">
							<button id="crop-button"><?= $this->lang->line("fe_avup_crop") ?></button>
						</div>
						<div id="output" class="aui-avatar aui-avatar-xxxxlarge aui-avatar-project">
							<div class="aui-avatar-inner">
								<img src="" alt="">
							</div>
						</div>
						<script src="<?= base_url() . "avup/" ?>aui/js/aui-all.js"></script>
						<!--[if lt IE 9]><script src="aui/js/aui-ie.js"></script><![endif]-->
						<script>window.$ = jQuery; //reset from AUI's noConflict jQuery</script>
						<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js"></script>
						<script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.0.rc.2/handlebars.min.js"></script>
						<script src="<?= base_url() . "avup/" ?>lib/fd-slider/fd-slider.js"></script>
						<script src="<?= base_url() . "avup/" ?>templates/image-upload-and-crop.js"></script>
						<script src="<?= base_url() . "avup/" ?>templates/aui-message.js"></script>
						<script src="<?= base_url() . "avup/" ?>js/canvas-cropper.js"></script>
						<script src="<?= base_url() . "avup/" ?>js/client-file-handler.js"></script>
						<script src="<?= base_url() . "avup/" ?>js/client-file-reader.js"></script>
						<script src="<?= base_url() . "avup/" ?>js/drag-drop-file-target.js"></script>
						<script src="<?= base_url() . "avup/" ?>js/upload-interceptor.js"></script>
						<script src="<?= base_url() . "avup/" ?>js/image-explorer.js"></script>
						<script src="<?= base_url() . "avup/" ?>js/image-upload-and-crop.js"></script>
						<script src="<?= base_url() . "avup/" ?>js/text-util.js"></script>
						<script type="text/javascript" src="<?= base_url() ?>/assets/js/jquery.exif.js"></script>
						<script>
							var upload_texts = [];
							upload_texts["image_select"] = "<?= $this->lang->line("fe_avup_image_select") ?>";
							upload_texts["drag_and_drop"] = "<?= $this->lang->line("fe_avup_drag_and_drop") ?>";
							upload_texts["err_image_size"] = "<?= $this->lang->line("fe_avup_err_image_size") ?>";
							upload_texts["err_max_size"] = "<?= $this->lang->line("fe_avup_err_max_size") ?>";
							upload_texts["err_file"] = "<?= $this->lang->line("fe_avup_err_file") ?>";
							upload_texts["err_is"] = "<?= $this->lang->line("fe_avup_err_is") ?>";
							upload_texts["err_larger_than"] = "<?= $this->lang->line("fe_avup_err_larger_than") ?>";
							upload_texts["err_error_upload"] = "<?= $this->lang->line("fe_avup_err_error_upload") ?>";
							upload_texts["err_valid_type"] = "<?= $this->lang->line("fe_avup_err_valid_type") ?>";
							$(function () {
								var $avup = $("#avup"),
										$cropButton = $('#crop-button');
								$output = $("#output");
								$avup.prepend(Handlebars.templates['image-upload-and-crop']({}));
								var imageUploadAndCrop = new ImageUploadAndCrop($avup.find('.image-upload-and-crop-container'),
                                                                {
                                                                    cropButton: $cropButton,
                                                                    selectImageBtnText: upload_texts["image_select"],
                                                                    err_image_size: upload_texts["err_image_size"],
                                                                    err_max_size: upload_texts["err_max_size"],
                                                                    err_file: upload_texts["err_file"],
                                                                    err_is: upload_texts["err_is"],
                                                                    err_larger_than: upload_texts["err_larger_than"],
                                                                    err_error_upload: upload_texts["err_error_upload"],
                                                                    err_valid_type: upload_texts["err_valid_type"],
                                                                    maxImageDimension: 4500,
                                                                    onCrop: function (croppedDataURI) {
//												console.log(imageUploadAndCrop);
                                                                            //$output.find("img").attr('src', croppedDataURI);
                                                                            $(".the_avatar img").attr('src', croppedDataURI);
                                                                            $(".avatar_value").val(croppedDataURI);
                                                                            scrol_to(".the_avatar", -130);
                                                                            //$output.show();
                                                                    },
                                                                    onImageUpload: function () {
                                                                        $cropButton.show();
                                                                        
                                                                        if (!$("#profile-image-crop-explain-text").length) {
                                                                            $(".image-explorer-container").after('<div id="profile-image-crop-explain-text"><i class="fa fa-exclamation-circle"></i> <?=$this->lang->line("fe_avup_crop_info");?></a></div>');
                                                                        }
                                                                    },
                                                                    onImageUploadError: function () {
                                                                            $cropButton.hide();
                                                                    }
                                                                }
								);

								var exifCallback = function (exifObject) {
									if (exifObject.Orientation!==undefined) {
										$("#avatar_orientation").val(exifObject.Orientation);
									}
								}

								$('#image-upload-and-crop-upload-field').change(function () {
									$(this).fileExif(exifCallback);
								});

							});
						</script>
						<div class="clear"></div>
					</div>
					<?php
				}
				?>

				<?= $register_type == "register" ? form_open("users/registration") : form_open("users/edit"); ?>
				<input type="hidden" name="avatar_orientation" id="avatar_orientation" value="1">
				<input type="hidden" name="old_avatar" value="<?= (isset($init["avatar"]) ? $init["avatar"] : set_value('avatar')); ?>">
				<?php if (!isset($is_brand) || $register_type == "register") { ?>
					<div class="radio_wrapper">
						<ul class="input_radio_choices">
							<li>
								<input type="radio" name="sex" value="f" id="type_female"<?= $sex_array["f"] ?><?= ( (isset($init) && $init["sex"] == "f" ) || set_value('sex') == "f") ? ' checked="checked"' : ""; ?>>
								<label for="type_female"></label>
								<label for="type_female"><?= $this->lang->line('fe_sex_f') ?></label>
							</li>
							<li>
								<input type="radio" name="sex" value="m" id="type_male"<?= $sex_array["m"] ?><?= ( (isset($init) && $init["sex"] == "m" ) || set_value('sex') == "m") ? ' checked="checked"' : ""; ?>>
								<label for="type_male"></label>
								<label for="type_male"><?= $this->lang->line('fe_sex_m') ?></label>
							</li>
							<?php if ($register_type == "register") { ?>
								<li>
									<input type="radio" name="sex" value="b" id="type_brand"<?= $sex_array["b"] ?><?= ( (isset($init) && $init["sex"] == "b" ) || set_value('sex') == "b") ? ' checked="checked"' : ""; ?>>
									<label for="type_brand"></label>
									<label for="type_brand"><?= $this->lang->line('fe_brand') ?></label>
								<li>
									<?php
								}
								?>
						</ul>
					</div>
					<?php
				}
				?>
				<div class="clear"></div>
				<?php echo form_error('sex', '<div class="error">', '</div>'); ?>
				<?php if (isset($avatar_folder)) { ?>
					<input type="hidden" class="avatar_value" name = "avatar" value = "">
				<?php } ?>
				<div class="row-wrapper">
					<label for="user_name"><?= $this->lang->line('fe_register_name') ?>:<span>*</span></label>
					<div class="input-wrapper">
						<input type="text" id="user_name" name="user_name" maxlength="25" value="<?= isset($init["user_name"]) ? $init["user_name"] : set_value('user_name'); ?>" />
					</div>
				</div>
				<?php echo form_error('user_name', '<div class="error">', '</div>'); ?>

				<div class="row-wrapper">
					<label for="email_address"><?= $this->lang->line('fe_email') ?>:<span>*</span></label>
					<div class="input-wrapper">
						<input type="text" id="email_address" name="email_address" maxlength="40" value="<?= isset($init["email_address"]) ? $init["email_address"] : set_value('email_address'); ?>" <?= $register_type == "edit" ? "disabled" : ""; ?>/>
					</div>
				</div>
				<?php echo form_error('email_address', '<div class="error">', '</div>'); ?>


				<div class="row-wrapper">
					<label for="password"><?= $this->lang->line('fe_password') ?>:<?= $register_type == "register" ? '<span>*</span>' : "" ?></label>
					<div class="input-wrapper">
						<input type="password" id="password" name="password" maxlength="32" value="<?php echo set_value('password'); ?>" />
					</div>
				</div>
				<?php echo form_error('password', '<div class="error">', '</div>'); ?>


				<div class="row-wrapper">
					<label for="con_password"><?= $this->lang->line('fe_password_repeat') ?>:<?= $register_type == "register" ? '<span>*</span>' : "" ?></label>
					<div class="input-wrapper">
						<input type="password" id="con_password" name="con_password" maxlength="32" value="<?php echo set_value('con_password'); ?>" />
					</div>
				</div>
				<?php echo form_error('con_password', '<div class="error">', '</div>'); ?>

				<?php if (!isset($is_brand) || $register_type == "register") { ?>
					<div class="bidth-date-wrapper"<?php if ($show_brand) { ?> style="display: none;"<?php } ?>>
						<div class="row-wrapper">
							<label><?= $this->lang->line('fe_register_birthday') ?>:<span>*</span></label>
							<div class="clear"></div>
							<?= $birth_date_day ?>
							<?= $birth_date_month ?>
							<?= $birth_date_year ?>
							<input type="hidden" id="birthdate" name="birthdate" value="<?= isset($init["birthdate"]) ? $init["birthdate"] : set_value('birthdate'); ?>"/>
						</div>
					</div>
					<?php echo form_error('birthdate', '<div class="error">', '</div>'); ?>
					<?php
				}
				?>
				<?php if (isset($is_brand) || $register_type == "register") { ?>
					<div class="brand-wrapper"<?php if ($show_brand) { ?> style="display: block;"<?php } ?>>
						<?php if (!isset($is_brand)) { ?>
							<div class="row-wrapper_disclaimer">
								<div class="disclaimer"><?= $this->lang->line("fe_brand_disclaimer") ?></div>
							</div>
							<?php
						}
						?>
						<div class="row-wrapper">
							<label for="brand_name"><?= $this->lang->line('fe_register_brandname') ?>:<span>*</span></label>
							<div class="input-wrapper">
								<input type="text" id="brand_name" name="brand_name" maxlength="20" value="<?= isset($init["brand_name"]) ? $init["brand_name"] : set_value('brand_name'); ?>" />
							</div>
						</div>
						<?php echo form_error('brand_name', '<div class="error">', '</div>'); ?>

						<div class="row-wrapper">
							<label for="website"><?= $this->lang->line('fe_register_website') ?>:<span>*</span></label>
							<div class="input-wrapper">
								<input type="text" id="website" name="website" maxlength="40" value="<?= isset($init["website"]) ? $init["website"] : set_value('website'); ?>" />
							</div>
						</div>
						<?php echo form_error('website', '<div class="error">', '</div>'); ?>

						<div class="row-wrapper">
							<label for="phone"><?= $this->lang->line('fe_register_phone') ?>:<span>*</span></label>
							<div class="input-wrapper">
								<input type="tel" id="phone" name="phone" maxlength="20" value="<?= isset($init["phone"]) ? $init["phone"] : set_value('phone'); ?>" />
							</div>
						</div>
						<?php echo form_error('phone', '<div class="error">', '</div>'); ?>
					</div>
					<?php
				}
				?>

				<div class="row-wrapper">
					<label for="about"><?= $this->lang->line('fe_register_description') ?>:</label>
					<div class="description">
						<textarea id="about" name="about" maxlength="500"><?= isset($init["about"]) ? $init["about"] : set_value('about'); ?></textarea>
					</div>
				</div>
				<?php echo form_error('about', '<div class="error">', '</div>'); ?>
				<?php if (!isset($is_brand) || $register_type == "register") { ?>
					<script>
						$(".input_radio_choices input").change(function () {
							if ($(this).attr("value") == "b") {
								$(".brand-wrapper").show();
								$(".bidth-date-wrapper").hide();
							} else {
								$(".brand-wrapper").hide();
								$(".bidth-date-wrapper").show();
							}
						});
					</script>
					<?php
				}
				?>


				<div class="row-wrapper center">
					<input type="submit" name="submit" id="submit-register" value="<?= $submit_button ?>" />
				</div>
                                        
                                <div class="row-wrapper_disclaimer center login">
                                    <?=$this->lang->line("fe_register_already");?> <a href="<?=base_url("login");?>"><?=$this->lang->line("fe_word_login");?></a>
                                </div>
				<?php echo form_close(); ?>
			</div><!--<div class="reg_form">-->
		</div><!--<div id="content">-->

	</div><!-- end .single-content -->
	<div class="clear"></div>
        <a name="profile-save"></a>
</div>