<!--<div class="wrap-fullwidth">
        <div class="single-content full">
<?php if (isset($error)): ?>
                                            <div style="color: red">
    <?php echo $error; ?>
                                            </div>
<?php endif; ?>

<?php echo form_open(); ?>

                <label for="username">Username: </label>
                <input type="text" name="username" id="username" />

                <label for="password">Password: </label>
                <input type="password" name="password" id="password" />
                <label for="remember">Remember me: </label>
                <input type="checkbox" name="remember" value="1" id="remember" />

                <input type="submit" value="login" />
                </form>
        </div> end .single-content 
        <div class="clear"></div>
</div>-->

<div class="clear_header"></div>
<div class="wrap-fullwidth the_content">
    <div class="single-content full">
        <div id="content">
            <div class="register-container ml">
                <h1><?= $this->lang->line('fe_reset_enter_new_pass'); ?></h1>
                <div class="row-wrapper center">
                    <i><?= $this->lang->line('fe_reset_pass_enter_new'); ?></i>
                </div>
                <br />
                <div class="status"></div>

                <?php
                $attributes = array('id' => 'frm_resetpass');
                //echo form_open('email/send', $attributes);
                echo form_open("", $attributes);
                ?>
                <input type="hidden" name="resethash" value="<?=$resethash;?>" />
                
                <div class="row-wrapper">
                    <label for="password"><?= $this->lang->line('fe_password') ?>:<span>*</span></label>
                    <div class="input-wrapper">
                        <input type="password" id="password" name="password" maxlength="32" value="<?php echo set_value('password'); ?>" />
                    </div>
                </div>
                <div class="row-wrapper">
                    <label for="con_password"><?= $this->lang->line('fe_password_repeat') ?>:<span>*</span></label>
                    <div class="input-wrapper">
                        <input type="password" id="con_password" name="con_password" maxlength="32" value="<?php echo set_value('con_password'); ?>" />
                    </div>
                </div>
                
                <div class="row-wrapper center">
                    <button type="submit" id="submit-register"><?= $this->lang->line('fe_reset_btn_change_pass'); ?></button>
                </div>

                <div class="row-wrapper_disclaimer center">
                    <div>
                        <?= $this->lang->line("fe_register_notyet"); ?>
                    </div>
                    <div>
                        <a href="<?= base_url("signup"); ?>"><b><?= strip_tags($this->lang->line("fe_register_push_1")); ?></b></a>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <script>
            jQuery(document).ready(function () {
                jQuery("#frm_resetpass").submit(function (e) {
                    
                    $("#submit-register").attr("disabled", "disabled").data("value", $("#submit-register").html()).html('<i class="fa fa-spin fa-spinner"></i> <?=$this->lang->line("fe_please_wait");?>...');
                    
                    e.preventDefault();
                    var url = jQuery(this).attr('action');
                    var method = jQuery(this).attr('method');
                    var data = jQuery(this).serialize();
                    
                    jQuery.ajax({
                        url: url,
                        type: method,
                        data: data
                    }).done(function (data) {
                        
                        jQuery(".status").html('').removeClass("error success");
                        
                        if (data !== '') {
                            var data_obj = jQuery.parseJSON(data);
                            
                            if (data_obj.error === true) {
                                if (typeof(data_obj.redirect) != "undefined") {
//                                    window.location = data_obj.redirect;
                                }
                                else if (typeof(data_obj.text) != "undefined") {
                                    jQuery(".status").addClass("error").html(data_obj.text);
                                }
                            }
                            else {
                                window.location = data_obj.redirect;
                            }
                        } else {
                            jQuery(".status").addClass(".error").html('<?=$this->lang->line("fe_contacts_sent_error");?>');
                        }
                        
                        $("#submit-register").html( $("#submit-register").data("value") ).removeAttr("disabled");
                    });
                });

                jQuery("div").each(function (index) {
                    var cl = jQuery(this).attr('class');
                    if (cl == '')
                    {
                        jQuery(this).hide();
                    }
                });

            });
        </script>


    </div>
    <div class="clear"></div>
</div>