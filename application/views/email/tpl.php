<!DOCTYPE html>
<html>
    <head>
        <title>Fave.bg</title>
        <style>
            * {font-family: 'Helvetica Neue', Arial, sans-serif; color: #000000; box-sizing: border-box; -webkit-text-size-adjust: none;}
            body { margin: 0; padding: 10px; background-color: #dfdfdf; width: 100%; min-width: 100%; height: 100%; }
            .content { with: 100%; max-width: 550px; margin: 0 auto; background-color: #FFFFFF; border-radius: 10px; overflow: hidden; font-size: 14px;}
            .header {padding: 15px 20px; margin-bottom: 20px;}
            .header img { height: auto; line-height: 100%; outline: none; text-decoration: none; display: block; border: 0; max-height: 40px; }
            .content-inner { padding: 0 20px 15px 20px;}
            .content-inner .sign-off { margin-top: 30px; }
            .footer { padding: 15px 20px; position: relative;}
            .footer a { color: #6c6c67; font-weight: normal; text-decoration: none;}
            .footer a, .footer .left { font-size: 12px; }
            .footer .right { height: 24px; margin-left: -92px; margin-top: -12px; position: absolute; right: 10px; top: 50%; width: 136px; }
            @media only screen and (max-width: 400px) {.footer .right{display: block;float: none;margin: 15px 0 0;position: relative;text-align: right;width: 100%;}}
            .footer .right a { display: inline-block; margin-left: 5px; background-image: url('<?=base_url()."assets/images/fave_email_social_media.png";?>'); background-repeat: no-repeat; width: 24px; height: 24px;}
            .footer .right a:first-child { margin-left: 0; }
            .footer .right a.fa-facebook { background-position: 0 0;}
            .footer .right a.fa-twitter { background-position: -24px 0;}
            .footer .right a.fa-linkedin { background-position: -48px 0;}
            .footer .right a.fa-pinterest { background-position: -72px 0;}
            .btn {background-color: #fff;border: 1px solid #e23129;border-radius: 3px;color: #e23129 !important;display: inline-block;margin: 20px 0;padding: 8px 16px;text-align: center;text-decoration: none;}
        </style>
    </head>

    <body>
        <div class="content">
            <div class="header">
                <img src="<?= base_url(); ?>assets/images/logo.png" alt="Fave.bg" />
            </div>
            <div class="content-inner">
                
                <?=$content;?>
                
                <p class="sign-off">
                    <?= $this->lang->line("email_signoff"); ?><br />
                    <?= $this->lang->line("email_faveteam"); ?>
                </p>
            </div>
            <div class="footer">
                <div class="left">
                    <a href="<?= base_url("terms"); ?>"><?= $this->lang->line("fe_usage_terms"); ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?= base_url("contacts"); ?>"><?= $this->lang->line("fe_contacts"); ?></a><br />
                    &copy; 2014-<?= date("Y"); ?> Fave.bg</td>
                </div>

                <div class="right">
                    <a href="<?=$this->config->item("my_socialmedia_facebook");?>" class="fa fa-facebook"></a>
                    <a href="<?=$this->config->item("my_socialmedia_twitter");?>" class="fa fa-twitter"></a>
                    <a href="<?=$this->config->item("my_socialmedia_linkedin");?>" class="fa fa-linkedin"></a>
                    <a href="<?=$this->config->item("my_socialmedia_pinterest");?>" class="fa fa-pinterest"></a>
                </div>
            </div>
        </div>
    </body>

</html>