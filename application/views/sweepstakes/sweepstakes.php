<!doctype html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
        <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui' >
        <link rel="shortcut icon" href="https://fave.bg/assets/images/favicon16.ico?v=2" type="image/x-icon">
        <title>Спечелете ваучер за 50лв. от Fave.bg и H&M</title>
        <meta name="description" content="Онлайн фешън платформа за споделяне, търсене и намиране на любими дрехи или модни аксесоари.">
        <meta property="fb:app_id" content="1151140538265910" >
        <meta property="og:site_name" content="Fave.bg" />
        <meta property="og:title" content="Спечелете ваучер за 50лв. от Fave.bg и H&M" />
        <meta property="og:description" content="Запишете се за участие, харесайте и споделете страницата и имате възможност да спечелите ваучер на стойност 50лв. от H&M." />
        <meta property="og:image" content="https://fave.bg/assets/sweepstakes/teens-college-fashion-promo.jpg" />
        <meta property="og:locale" content="bg_BG" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://fave.bg/sweepstakes" />
        <meta property="og:image:width" content="590" />
        <meta property="og:image:height" content="295" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel='stylesheet' href='assets/fonts/css/font-awesome.css' type='text/css' media='all' >
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <style>
            * {
                box-sizing: border-box;
            }
            body {
                background: rgba(0, 0, 0, 0) url("assets/sweepstakes/fashion_background.jpg") repeat fixed 0 0;
                font-family: verdana;
                color: #454545;
                font-size: 16px;
            }
            .container {
                width: 600px;
                max-width: 100%;
                margin: 0 auto 25px auto;
            }
            .header > div {
                height: 160px;
            }
            .fave-red {
                color: #E23129;
            }
            .fave-blue {
                color: #2969B0;
            }
            .fave-green {
                color: #41A85F;
            }
            .fave-orange {
                color: #F37934;
            }
            .txt-lg {
                font-size: 18px;
            }
            .logo {
                max-width: 100%;
                position: relative;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
            }
            .logo.hm {
                max-width: 120px;
            }
            .inner {
                background-color: rgba(245,245,245,0.85);
                padding: 30px 50px 50px 50px;
            }
            .inner h1.title {
                color: #E23129;
                text-align: center;
                font-size: 36px;
            }
            .signup_form .inputs {
                background: #f5f5f5 none repeat scroll 0 0;
                border: 2px solid rgba(0,0,0,0.35); /*38c3dc*/
                color: #444;
                vertical-align: baseline;
            }
            .md-fields .form-control, .md-fields .button {
                font-size: 16px;
                height: 48px;
            }
            .input-group .form-control:first-child {
                border-bottom-left-radius: 3px;
                border-top-left-radius: 3px;
                font-size: 1.75rem;
                padding-bottom: 0.75rem;
                border-right: none;
            }
            .input-group-addon, .input-group-btn {
                vertical-align: middle;
                white-space: nowrap;
                width: 1%;
            }
            .button, .button:active, .button.active {
                background: rgba(0,0,0,0.35) none repeat scroll 0 0;
                color: #ffffff;
                cursor: pointer;
                letter-spacing: 1px;
                margin: 0;
                transition: all 0.25s ease-in-out 0s;
            }
            .social-media-container {
                margin-top: 10px;
            }
            .social-media-container a {
                text-decoration: none;
                margin: 0 5px;
            }
            .social-media-container a.facebook {
                color: #3b5998;
            }
            .social-media-container a.linkedin {
                color: #007bb6;
            }
            .social-media-container a.twitter {
                color: #00aced;
            }
            .social-media-container a.pinterest {
                color: #cb2027;
            }
            .social-media-container a.instagram {
                color: #517fa4;
            }
            @media only screen and (max-width: 1199px){
                .logo {
                    max-width: 70%;
                }
                .logo.fave {
                    max-width: 405px;
                }
            }
            @media only screen and (max-width: 480px){
                .inner {
                    padding: 25px 25px 25px
                }
                .logo.fave {
                    max-width: 90%;
                }
                .header > div {
                    height: 120px;
                }
                h3 {
                    font-size: 18px;
                    font-weight: bold;
                }
                .input-group .form-control:first-child {
                    font-size: 14px;
                    border-bottom-right-radius: 3px;
                    border-top-right-radius: 3px;
                    border: 2px solid rgba(0,0,0,0.35);
                }
                .input-group-btn:last-child > .btn {
                    border-bottom-left-radius: 3px;
                    border-top-left-radius: 3px;
                }
                .input-group-btn:last-child,
                .input-group-btn:last-child > .btn {
                    width: 100%;
                }
                .md-fields .form-control, .md-fields .button {
                    font-size: 14px;
                }
                .input-group-addon, .input-group-btn, .input-group .form-control {
                    display: block;
                    margin-bottom: 10px;
                    clear: both;
                    }
                .input-group {
                    display: block;
                }
            }
        </style>
    </head>
    <body>
        <?= $this->config->item("my_analytics") ?>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/bg_BG/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <div class="container">
            <div class="row header">
                <div class="col-lg-8">
                    <img src="assets/sweepstakes/fave_new_platform.png" class="logo fave"/>
                </div>
                <div class="col-lg-4">
                    <img src="assets/sweepstakes/1280px-HM-Logo.svg.png" class="logo hm" />
                </div>
            </div>
            <div class="inner">
                <div class="row">
                    <div class="col-lg-12">
                        <div style="background-image: url('assets/sweepstakes/teens-college-fashion-590x295.jpg'); background-repeat: no-repeat; background-size: cover; background-position: top center; height: 150px;"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="fave-red title">СПЕЧЕЛЕТЕ ВАУЧЕР<br> 50лв от <img src="assets/sweepstakes/1280px-HM-Logo.svg.png" style="max-width: 55px;" /></h1>
                        <div style="margin: 15px 0;">
                            <strong><span class="fave-red txt-lg">Регистрирайте</span> се в новата социална платформа за мода <a href="https://fave.bg" target="_blank">fave.bg</a>, <span class="fave-red txt-lg">харесайте</span> Фейсбук страницата ни и 
                                <span class="fave-red txt-lg">споделете</span> със своите приятели за нас и ще имате шанс да спечелите ваучер за H&M на стойност 50лв.
                            </strong>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">1. Запишете се за участие</h3>
                        <div class="fave-red txt-lg">
                            Играта на Fave.bg приключи. Благодарим на всички, които взеха участие. Очаквайте тегленето на наградите съвсем скоро.
                        </div>
                        <?php /* ?><form method="post" id="signup_form" data-remote="true" class="signup_form" accept-charset="UTF-8" data-page-id="119718" target="_parent">
                            <div class="input-group md-fields">
                                <input type="email" data-label-text="Email" placeholder="Въведете имейл адрес" name="email" id="email" class="inputs required form-control ">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn submit button big" id="btn-submit" style="min-width: 125px;">Запиши ме</button>
                                </span>
                            </div>
                        </form>
                        <div class="signup-confirm" style="display: none">
                            <p><i class="fa fa-check fave-green"></i> Благодарим. Успешно се записахте в играта за ваучер на стойност 50лв от H&M.</p>
                            <p>Моля харесайте и споделете страницата за да участвате в жребия за наградата.</p>
                        </div><?php */ ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">2. Харесайте страницата</h3>
                        <div class="fb-like" data-href="https://facebook.com/fave.bg" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">3. Споделете страницата</h3>
                        <div class="fb-share-button" data-href="https://facebook.com/fave.bg" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffacebook.com%2Ffave.bg%2F&amp;src=sdkpreparse">Share</a></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" style="margin-top: 50px; font-size: 0.8em; font-style: italic;">
                        <p><strong>Допълнителни условия</strong><br>
                            Един участник може да се запише и участва в играта само един път , играта ще продължи до 16.10.2016. 
                            <a href="https://fave.bg" target="_blank">fave.bg</a> си запазва правото да промени правилата или прекрати играта по всяко време.
                        </p>                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12" style="margin-top: 25px;">
                        Намерете ни в социалните мрежи:
                    </div>
                    <div class="col-lg-12 social-media-container tmb">
                        <a target="_blank" class="facebook" href="https://facebook.com/fave.bg"><i class="fa fa-facebook fa-2x"></i></a>
                        <a target="_blank" class="linkedin" href="https://www.linkedin.com/in/fave-bg-927294125"><i class="fa fa-linkedin fa-2x"></i></a>
                        <a target="_blank" class="twitter" href="https://twitter.com/BgFave"><i class="fa fa-twitter fa-2x"></i></a>
                        <a target="_blank" class="pinterest" href="https://www.pinterest.com/favebg/"><i class="fa fa-pinterest fa-2x"></i></a>
                        <a target="_blank" class="instagram" href="https://www.instagram.com/fave.bg/"><i class="fa fa-instagram fa-2x"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                $("#signup_form").on("submit", function() {
                    var email = $("#email").val();
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (!regex.test(email)) {
                        alert("Моля въведете Вашият имейл адрес");
                        return false;
                    }
                    
                    $("#btn-submit").data("html", $("#btn-submit").html() ).html('<i class="fa fa-spin fa-spinner"></i>');
                    
                    $.ajax({
                        type: "POST",
                        url: '/sweepstakes/enter',
                        dataType: "json",
                        data: {email:email},
                        async: false,
                        success: function (data) {
                            if (data.error === true) {
                                alert(data.text);
                                return;
                            }
                            
                            $("#signup_form").hide(250, function() {
                                $(this).remove();
                                $(".signup-confirm").show(250);
                            });
                        }, // End success function
                        error: function (jqXHR, textStatus) {
                            alert("Възникна грешка (" + textStatus + ")\n\nМоля опитайте отново.");
                        },
                        complete: function() {
                            $("#btn-submit").html($("#btn-submit").data("html"));
                        }
                    });
                    
                    return false;
                });
            });
        </script>
    </body>
</html>