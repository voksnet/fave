<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="home-fullwidth footer">
	<div class="wrap-content">
		<div class="clearfix">
			<div class="tag_cloud"><?= $this->cache_model->get("cache.footer.tags.html"); ?></div>
	<!--<?=$this->tags_model->make_most_cloud();?> -->
		</div>
	</div>
	<div class="wrap-content">
		<div class="clearfix">
			<div>
				<div class="fb-page" data-href="https://www.facebook.com/fave.bg" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
					<blockquote cite="https://www.facebook.com/fave.bg" class="fb-xfbml-parse-ignore">
						<a href="https://www.facebook.com/fave.bg">Fave.bg</a>
					</blockquote>
				</div>
			</div>
			<div>
				<h2><?= $this->lang->line("fe_word_latest_blogs"); ?></h2>
				<?= $this->cache_model->get("cache.footer.blog.html"); ?>
			</div>
			<div>
				<h2><?= $this->lang->line("fe_word_useful"); ?></h2>
				<div class="social">
					<a href="<?= $this->config->item("my_socialmedia_facebook"); ?>"><i class="fa fa-facebook"></i></a>
					<a href="<?= $this->config->item("my_socialmedia_twitter"); ?>"><i class="fa fa-twitter"></i></a>
					<a href="<?= $this->config->item("my_socialmedia_linkedin"); ?>"><i class="fa fa-linkedin"></i></a>
					<a href="<?= $this->config->item("my_socialmedia_instagram"); ?>"><i class="fa fa-instagram"></i></a>
					<a href="<?= $this->config->item("my_socialmedia_pinterest"); ?>"><i class="fa fa-pinterest"></i></a>
				</div>
				<div class="nav">
					<a href="<?= base_url() . "faq"; ?>"><?= $this->lang->line("fe_site_menu")["faq"]["title"]; ?></a>
					<a href="<?= base_url() . "about"; ?>"><?= $this->lang->line("fe_site_menu")["about_us"]["title"]; ?></a>
					<a href="<?= base_url() . "how"; ?>"><?= $this->lang->line("fe_site_menu")["how_it_works"]["title"]; ?></a>
					<a href="<?= base_url() . "contacts"; ?>"><?= $this->lang->line("fe_site_menu")["contact_us"]["title"]; ?></a>
					<a href="<?= base_url() . "terms"; ?>"><?= $this->lang->line("fe_site_menu")["terms_of_use"]["title"]; ?></a>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Footer Theme output -->
<link rel='stylesheet' href='<?= base_url() ?>assets/css/some.css' type='text/css' media='all' />
<script type='text/javascript'>
	/* <![CDATA[ */
//			var _wpcf7 = {
//				"loaderUrl": "<?= base_url() ?>assets/images/ajax-loader.gif",
//				"sending": "Sending ...",
//				"cached": "1"
//			};
	/* ]]> */
</script>
<script type='text/javascript' src='<?= base_url() ?>assets/js/jquery-plugins.js'></script>
</body>
</html>