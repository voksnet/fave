<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="masonry_wrapper the_content">
	<script>
		var masonries = {
			blank: {
//				explore: "<?= isset($explore) ? $explore : ""; ?>",
//				search: "<?= isset($search) ? $search : ""; ?>",
//				exclude: <?= isset($exclude) ? $exclude : 0; ?>,
//				category: "<?= isset($category) ? $category : ""; ?>",
//				profile: '<?= isset($selector) ? $selector : ""; ?>',
				ui: '<?= $unique_id ?>',
//				likes: 0,
//				my_feed: 0,
//				my_likes: 0,
				masonry_selector: "",
				msnry: false,
				masonry_parent_selector: ".home-fullwidth.mason.template",
				append_to: ".main_mason_appendor"
			}
		};
		var scroll_top = 0;
		var is_modal = false;
		var special_reset = false;
		var add_data = {};
//		var from_button = 0;
		var my_feed = <?= isset($my_feed) ? $my_feed : 0; ?>;
		var my_likes = <?= isset($my_likes) ? $my_likes : 0; ?>;
		var popstate = 0;
		var profile = '<?= isset($selector) ? $selector : ""; ?>';
		var go_to_uploaded = false;
		var just_loaded = true;

		function resetMasonry(key) {
			$(masonries[key].append_to).html("");
			delete masonries[key];
		}

		function clone_ms_object(ms) {
			var new_object = {};
			for (var key in ms) {
				if (typeof (ms[key]) !== "object") {
					new_object[key] = ms[key];
				}
			}
			return new_object;
		}


		function showMasonry(key) {
			if (special_reset && key == "main") {
				special_reset = false;
				resetMasonry("main");
				scrol_to("#main_top", -130);
				var add_data = {
					my_feed: my_feed,
					my_likes: my_likes,
					profile: profile,
				}
				masonryInit("main", add_data);
			} else {
				var layout_timeout;
				$(masonries[key].masonry_selector + " .loading_more").show();
				var masonryGrid = function (callback) {
					var data_to_post = clone_ms_object(masonries[key]);
//					data_to_post.button = from_button;
					$.ajax({
						url: '<?= base_url() ?>main/get_masonry',
						dataType: 'json',
						data: data_to_post,
						success: callback
					});
				};
				masonryGrid(function (data) {
					$(masonries[key].masonry_selector + " .show_more").hide();
					if (data["none"] === true) {
						if (data["reset"]) {
							location.reload();
						} else if (controler == "") {
							$(".error_no_result").html(data["error_message"]);
						}
						$(".home-fullwidth.mason.view").remove();


						$(masonries[key].masonry_selector + " .loading_more").hide();
					} else if (data["ok"] === true) {
						$(".error_no_result").html("");
						for (var i = 0; i < data["posts"].length; i++) {
							var item = $(data["posts"][i]);
							item.find(".post_author_delete .fa-times").attr("data-mason", key);
							$(masonries[key].masonry_selector + " .infinite-articles").append(item).masonry('appended', item);
						}
						//MYTODO fix this make like php: here goes limbo process
//						$(masonries[key].masonry_selector + " .infinite-articles").animate({
//							"opacity": "1"
//						}, 100);
						$(masonries[key].masonry_selector + " .infinite-articles").css({"opacity": "1"});
						$(masonries[key].masonry_selector + " .infinite-articles .open-view").imagesLoaded().progress(
								function (instance, image) {
									clearTimeout(layout_timeout);
									layout_timeout = setTimeout(function () {
										masonries[key].msnry.masonry("layout");
										if ((!data["final"] && (masonries[key].exclude === undefined || masonries[key].exclude === "" || masonries[key].exclude == 0))) { //&& $(masonries[key].masonry_selector + " .show_more").is(":visible") == false
											$(masonries[key].masonry_selector + " .show_more").show("300");
										}
										//FIXME: if problem with loadin more button
										$(masonries[key].masonry_selector + " .loading_more").hide("300");
									}, 300);
								});


						if (go_to_uploaded) {
							$(".infinite-articles .open-view").imagesLoaded(function () {
								$('html, body').animate({
									scrollTop: $(".top_uploads").offset().top - 50
								}, 100);
							});
						}
						masonries[key].msnry.masonry("layout");
						masonries[key].msnry.on('layoutComplete', function (event, laidOutItems) {
							masonries[key].msnry.off('layoutComplete');
							masonries[key].msnry.masonry("layout");
						});

						if (data["ok"] == undefined || data["final"]) {
							$(masonries[key].masonry_selector + " .show_more").hide();
							$(masonries[key].masonry_selector + " .loading_more").hide();
						}
						if (data["reset"]) {
							special_reset = true;
						}
						if (data["new_ui"]) {
							masonries[key].ui = data["new_ui"];
						}
					}
				});
			}
		}
		;

		function masonryInit(key, add_data) {
			masonries[key] = $.extend(true, {}, masonries.blank);
			if (add_data !== undefined) {
				for (var element in add_data) {
					masonries[key][element] = add_data[element];
				}
			}

			if (!masonries[key].msnry) {
				masonries[key].masonry_selector = ".home-fullwidth.mason." + key;
				var this_masonry = masonries[key].masonry_selector;
				if (!just_loaded) {
					$(masonries[key].append_to).append($("<div>", {"class": "home-fullwidth mason " + key}));
					$(masonries[key].masonry_selector).html($(masonries[key].masonry_parent_selector).html());
					$(masonries[key].masonry_selector).show();
				}

				masonries[key].msnry = $(this_masonry + " .infinite-articles");

				if (just_loaded) {
					$(masonries[key].masonry_selector + " .infinite-articles").css({"opacity": "1"});

					$(masonries[key].append_to).append($("<div>", {"class": "limbo"}));
					var init = false;
					var all_posts = $(this_masonry + " .infinite-articles .post");
					$(masonries[key].append_to + " .limbo").html(all_posts);
					all_posts.find(".post").remove();
					masonries[key].msnry.masonry({
						itemSelector: ".infinite-articles .ex34"
					});
					var limbo = masonries[key].append_to + " .limbo";
					$(limbo).find(".open-view,.sponsored").imagesLoaded().progress(
							function (instance, image) {
								var post_item = image.img.id.replace("postitm-", "");
								var this_post = $(limbo).find(".post_" + post_item);
								$(masonries[key].masonry_selector + " .infinite-articles").append(this_post).masonry('appended', this_post);
								this_post.show();
								masonries[key].msnry.masonry("layout");
								if ($(limbo + " li").length === 0) {
									$(".loading_more").hide();
									$(".showit").show();
									$(".showit").removeClass("showit");
								}
								$(".home-fullwidth.mason." + key).find(".post_author_delete .fa-times").attr("data-mason", key);
							}
					);
					//MYTODO:!!! for old ioses init

				} else {
					masonries[key].msnry.masonry({
						itemSelector: ".ex34"
					});
				}
				masonries[key].msnry.imagesLoaded(function () {
					if (!just_loaded) {
						//MYTODO: here is a good place for ios tablet masonry re init or trace showMasonry(key);
						showMasonry(key);
						setTimeout(
								function () {
									$(masonries[key].masonry_selector + " .loading_more").hide();
								}, 2000
								);
					}
				});
			} else {
				showMasonry(key);
			}

			$(this_masonry + " #show_more_button").on("click", function (e) {
				if (just_loaded) {
					just_loaded = false;
				}
//				from_button = 1;
				showMasonry(key);
				$(this_masonry + " .show_more").hide();
//				from_button = 0;
				return false;
			});
		}

		function show_modal_post(id) {
			is_modal = true;
			$(".home-fullwidth.mason.view").remove();
			if (controler != "view") {
				if (scroll_top == 0) {
					scroll_top = $(window).scrollTop();
				}
				reset_post();
				get_post(id, "view");
				$(masonries.main.masonry_selector).hide();
			} else {
				get_post(id, "view");
				//$(window).scrollTop(0);
			}
			//MYTODO: IOS  BUG FIX - gap problem on chrome when pulling down

			$("header.sticky").css("top", "0px");
			if (controler == "profile") {
				$(".wrap-fullwidth.profile").hide();
			} else if (sub_controler != "tags") { //if (controler != "")
				//MYTODO: fix this for modal view in tags !!!!!!!!!!!!!!!!!!!!!!!!!!!!
				$(".clear_header").hide();
				//$("#featured-slider").hide();
				$('#featured-slider').css({top: -1, opacity: 0, height: "1px"});
				$(".blog-home-container").hide();
			}
		}

		window.onpopstate = function (event) {
//			popstate++;
			if (event.state && event.state.myTag || is_modal) {
				is_modal = false;
				if (controler == "view") {
					$(".post_appendor").hide();
					$(masonries.view.append_to).hide();
					location.reload();
				} else if (!hash_process) {
					if (event.state && event.state.view) {
						post_id = event.state.view;
						show_modal_post(post_id);
					} else {
						reset_post();
						$(masonries.main.masonry_selector).show();
						masonries.main.msnry.masonry("layout");
						$(window).scrollTop(scroll_top);
						scroll_top = 0;
					}
				}
			}
//			else if (popstate > 1) {
//					console.log("reload");
////					location.reload();
//			}
		}
		$(function () {
			if (controler != "view" && history.state == null && !just_loaded) { //     // && controler != "profile"
				history.pushState({myTag: true}, "", "<?= base_url(uri_string()) ?>");
			} else {
				if (history.state == null) {
					history.replaceState({myTag: true}, "", "<?= base_url(uri_string()) ?>");
				}
			}

			$(window).on("orientationchange", function () {
				//MYTODO: !!!this is for older ioses must find what version and implement for post remove, add key. add this in get_post also
				var scroll_top_orientation = $(window).scrollTop();
				masonries["main"].msnry.masonry("destroy");
				masonries["main"].msnry.masonry({
					itemSelector: ".infinite-articles .ex34"
				});
				$(window).scrollTop(scroll_top_orientation);
			})

			if (controler != "view") {
				$("body").on("click", ".open-view:not(.external)", function (e) {
					if (just_loaded) {
						just_loaded = false;
					}
					post_id = $(e.currentTarget).data("open_view");
					var slug = "";
					slug = $(e.currentTarget).data("slug");
					if (slug != "") {
						slug = "/" + slug;
					}
					//modal
					history.pushState({view: post_id, myTag: true}, "", "<?= base_url() . $this->config->item("my_urlparams_view") ?>/" + post_id + slug);
					show_modal_post(post_id);
					return false;
				});
			}

			if (controler === "" || controler === "profile") {
				var add_data = {
					my_feed: my_feed,
					my_likes: my_likes,
					profile: profile,
				}
				$(".infinite-articles li").hide();
				$(".infinite-articles li.sizer").show();
				masonryInit("main", add_data);
				//$(masonries.main.masonry_selector).on("click", ".open-view", function (e) {
			}
		});

	</script>
	<?php
	$header_title = "";
	$tag_content = "";
	if ((isset($explore) && $explore != "" && !isset($exclude)) || ((isset($search) && $search != ""))) {
		$this->load->helper('word');
		?>
		<script>
			sub_controler = "tags";
		</script>
		<?php
		if (isset($search)) {
			$search_term = $search;
			$header_title = $search_term;
		} else {
			$search_term = $explore;
			$header_title = "#" . $search_term;
		}
		if (isset($tags)) {
			$tag_image = "";
			$tag_description = "";
			if (isset($tags["image"])) {
				$tag_image = '<img src="' . $tags["image"] . '" alt="' . $search_term . '">';
			}
			if (isset($tags["description"])) {
				$tag_description = '<div class="tag_description">' . $tag_image . $tags["description"] . '</div>';
			}
			$tag_content = '<div class="tag_header">' . $tag_description . '</div>';
		}
		if ($tag_content != "") {
			$data["tag_info"] = $tag_content;
		}
	} else if (isset($exclude) && $exclude != "") {
		
		$this->load->helper('word');
		$header_title = capitalize($this->lang->line("fe_word_similar_posts"));
	} else if (isset($page_title) && !isset($no_header_title) ) { //&& !isset($home) &&
		$header_title = $page_title;
	}
	
//if (isset($page_title)) {
//			$header_title = $page_title;
//		} 
	$show_more_show = "";
	if (isset($data) && isset($page_type) && (isset($data["final"]) && !$data["final"])) {
		$show_more_show = ' showit';
	}

	if ((!isset($exclude) || $exclude == "") || isset($selector) || (isset($data["final"]) )) {
		$loading_show = '<div class="loading_more"></div><div class="show_more' . $show_more_show . '"><a href="#" id="show_more_button">' . $this->lang->line("fe_word_show_more") . '</a></div>';
	} else {
		$loading_show = "";
	}

	function masonry_content($template = true, $header_title, $loading_show, $data = "", $page_type = "") {
		$main_display = "";
		if ($template) {
			$template_class = " template";
		} else {
			$template_class = " " . $page_type;
			$main_display = ' style="display: block"';
		}
		$error_message = "";
		if (is_array($data) && isset($data["error_message"])) {
			$error_message = $data["error_message"];
		}
		$tag_content = "";
		if (isset($data["tag_info"])) {
			$tag_content = $data["tag_info"];
		}
		$masonry = '<div class="home-fullwidth mason' . $template_class . '"' . $main_display . '>
	<div class="wrap-content">
		<h2 class="search_header">' . $header_title . '</h2>
		' . $tag_content . '
		<div class="error_no_result">' . $error_message . '</div>';
		if ($error_message == "") {
			$masonry.='<div class="related_wrapper">
				<ul class="infinite-articles grid_list" data-masonry-options=\'{ "columnWidth": 0 }\'>
				<li class="sizer"></li>';
			if (isset($data["posts"])) {
				foreach ($data["posts"] as $post_item) {
					$masonry.=$post_item;
				}
			}
			$masonry.='			</ul>
			' . $loading_show . '
		</div>';
		}
		$masonry.='</div>
	<div class="clear"></div>
</div>';
		echo $masonry;
	}

	masonry_content(true, $header_title, $loading_show);
	?>
	<a href="#" name="main_top" id="main_top"></a>

	<?php
	if (!isset($page_type) || (isset($page_type) && $page_type == "main")) {
		?>
		<div class="main_mason_appendor">
			<?php
			if (isset($data) && isset($page_type) && $page_type == "main") {
				masonry_content(false, $header_title, $loading_show, $data, "main");
			}
			?>
		</div>
	<?php } ?>
	<?php
	if ($page_type != "view") {
		?>
		<span class="post_appendor"></span>
		<?php
	}
	?>
	<span class="post_mason_appendor">
		<?php
		if (isset($data) && isset($page_type) && $page_type == "view") {
			masonry_content(false, $header_title, $loading_show, $data, "view");
		}
		?>
	</span>
</div>