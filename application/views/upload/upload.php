<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$sponsored = false;
$is_brand = false;
if (($this->session->userdata("sex") == "b" && stristr($this->session->userdata("user_level"), ",sponsored,")) || $this->session->userdata("user_type") == "admin") {
	$sponsored = true;
} else if ($this->session->userdata("sex") == "b") {
	$is_brand = true;
}
?>
<!-- Bootstrap styles -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/upload.css">
<!-- blueimp Gallery styles -->
<!--<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">-->
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fileupload/jquery.fileupload.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/tags/tags.css">
<!--<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fileupload/jquery.fileupload-ui.css">-->
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?= base_url(); ?>assets/css/fileupload/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?= base_url(); ?>assets/css/fileupload/jquery.fileupload-ui-noscript.css"></noscript>

<!-- Begin Home Full width -->
<div class="clear_header"></div>
<div class="wrap-fullwidth the_content upload">
	<div class="top_div">
	</div>
	<div class="single-content">
		<div class="upload_request_holder">

			<div class="request_header">
				<h1><?= $this->lang->line('fe_requestupload_title'); ?></h1>
			</div>
			<form id="fileupload" class="request-form fileupload-processing" action="<?= $this->config->item("my_urlparams_upload") ?>/do_upload" method="POST" enctype="multipart/form-data">
				<div class="upload_form">
					<div id="server_error"></div>
					<div class="after_upload"></div>
					<div class="fileupload-buttonbar">
						<!-- The fileinput-button span is used to style the file input field as button -->
						<span class="btn fileinput-button">
							<i class="fa fa-plus"></i>
							<span><?= $this->lang->line('fe_word_add-image'); ?></span>
							<input type="file" name="userfile">
						</span>
					</div>

					<div class="fileupload-webimage">
						<div class="row-wrapper">
							<label id="web_image_label" for="web_image"><?= $this->lang->line('fe_word_or') ?></label>
							<div class="input-wrapper">
								<span class="deleteicon">
									<input type="text" id="web_image" name="web_image" maxlength="500" value="" placeholder="<?= $this->lang->line('fe_paste_image_url') ?>" />
									<span class="web_image_reset fa fa-close"></span>
								</span>
							</div>
							<div class="web_image_error form_error"></div>
							<div class="web_image_preview"></div>
						</div>
					</div>
					<div class="upload_image_selector">
						<div class="tip_images">
							<div class="tip_images_nav">
								<div class="arrows left_sequence fa fa-caret-square-o-left"> </div>
								<div class="total_sequences"> <span class="current">0</span> / <span class="total">0</span> </div>
								<div class="arrows right_sequence fa fa-caret-square-o-right"> </div>
							</div>
							<div class="tip_images_img"></div>
							<div class="tip_images_store_img"></div>
							<input type="hidden" name="tip_image_url" class="tip_image_url">
						</div>
						<a href="#" class="chouse"><?= $this->lang->line("fe_word_chouse") ?></a>
					</div>
					<!-- The table listing the files available for upload/download -->
					<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
					<script id="template-upload" type="text/x-tmpl">
						{% for (var i=0, file; file=o.files[i]; i++) { %}
						<tr class="template-upload fade">
						<td class="canvas_preview">
						<div>
						<span class="preview"></span>
						</div>
						<div>
						<button type="reset" class="btn cancel">
						<i class="fa fa-close"></i>
						<span><?= $this->lang->line("fe_word_cancel") ?></span>
						</button>
						</div>
						<div>
						<strong class="error text-danger"></strong>
						</div>
						</td>
						</tr>
						{% } %}
					</script>

					<script id="template-download" type="text/x-tmpl">
					</script>
					<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
					<script src="<?= base_url(); ?>assets/js/fileupload/vendor/jquery.ui.widget.js"></script>
					<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
					<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
					<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
					<script src="<?= base_url(); ?>assets/js/fileupload/jquery.iframe-transport.js"></script>
					<script src="<?= base_url(); ?>assets/js/fileupload/jquery.fileupload.js"></script>
					<script src="<?= base_url(); ?>assets/js/fileupload/jquery.fileupload-process.js"></script>
					<script src="<?= base_url(); ?>assets/js/fileupload/jquery.fileupload-image.js"></script>
					<script src="<?= base_url(); ?>assets/js/fileupload/jquery.fileupload-ui.js"></script>
					<script src="<?= base_url(); ?>assets/js/tags/tagit.js" type="text/javascript" charset="utf-8"></script>
					<script>
						var maxFileUpload = <?= $this->config->item("my_max_filesize_upload") ?>,
								commentLenght = <?= $this->config->item("my_comment_max_length") ?>,
								cs = <?= $this->config->item("my_censorship"); ?>,
								my_imageupload_max_w = "<?= $this->config->item("my_imageupload_max_w") ?>",
								my_imageupload_max_h = "<?= $this->config->item("my_imageupload_max_h") ?>",
								my_imageupload_min_w = "<?= $this->config->item("my_imageupload_min_w") ?>",
								my_imageupload_min_h = "<?= $this->config->item("my_imageupload_min_h") ?>",
								my_max_tags = "<?= $this->config->item("my_max_tags") ?>",
								my_min_tag_length = "<?= $this->config->item("my_min_tag_length") ?>",
								my_max_tag_length = "<?= $this->config->item("my_max_tag_length") ?>",
								successPage = "<?= $success_page ?>",
								can_sponsor = <?=$sponsored?"true":"false"?>,
								is_brand = <?=$is_brand?"true":"false"?>;


						var langLine = {
							fe_imgupload_invalid_url: "<?= $this->lang->line("fe_imgupload_invalid_url") ?>",
							fe_imgupload_invalid_url_site: "<?= $this->lang->line("fe_imgupload_invalid_url_site") ?>",
							fe_imageupload_small_image: "<?= $this->lang->line("fe_imageupload_small_image") ?>",
							fe_imageupload_big_file: "<?= $this->lang->line("fe_imageupload_big_file") ?>",
							maxNumberOfFiles: "<?= $this->lang->line("fe_imageupload_max_files") ?>",
							acceptFileTypes: "<?= $this->lang->line("fe_imageupload_file_types") ?>",
							maxFileSize: "<?= $this->lang->line("fe_imageupload_big_file_js") ?>",
							minFileSize: "<?= $this->lang->line("fe_imageupload_small_file") ?>",
							smallSizes: "<?= $this->lang->line("fe_imageupload_small_image") ?>",
							fe_imageupload_tag_link: "<?= $this->lang->line("fe_imageupload_tag_link") ?>",
							fe_imageupload_tag_censor: "<?= $this->lang->line("fe_imageupload_tag_censor") ?>",
							fe_imageupload_comment_link: "<?= $this->lang->line("fe_imageupload_comment_link") ?>",
							fe_imageupload_comment_censor: "<?= $this->lang->line("fe_imageupload_comment_censor") ?>",
							fe_imageupload_tag_none: "<?= $this->lang->line("fe_imageupload_tag_none") ?>",
							fe_imageupload_cat_none: "<?= $this->lang->line("fe_imageupload_cat_none") ?>",
							fe_imageupload_gender_none: "<?= $this->lang->line("fe_imageupload_gender_none") ?>",
							fe_upload_short_tag: "<?= sprintf($this->lang->line("fe_upload_short_tag"), $this->config->item("my_min_tag_length"), $this->config->item("my_max_tag_length")) ?>",
							fe_upload_long_tag: "<?= sprintf($this->lang->line("fe_upload_long_tag"), $this->config->item("my_min_tag_length"), $this->config->item("my_max_tag_length")) ?>",
							fe_upload_too_many_tags: "<?= sprintf($this->lang->line("fe_upload_too_many_tags"), $this->config->item("my_max_tags")) ?>",
							fe_upload_error_url: "<?= $this->lang->line("fe_upload_error_url") ?>",
							fe_upload_error_currency: "<?= $this->lang->line("fe_upload_error_currency") ?>",
							fe_upload_error_no_price: "<?= $this->lang->line("fe_upload_error_no_price") ?>",
							fe_upload_error_price: "<?= $this->lang->line("fe_upload_error_price") ?>",
							fe_upload_error_must_price: "<?= $this->lang->line("fe_upload_error_must_price") ?>",
							fe_upload_error_price_url: "<?= $this->lang->line("fe_upload_error_price_url") ?>",
							fe_upload_error_url_must: "<?= $this->lang->line("fe_upload_error_url_must") ?>",
							fe_upload_error_url_must_sponsored: "<?= $this->lang->line("fe_upload_error_url_must_sponsored") ?>",
						}
					</script>
					<script src="<?= base_url(); ?>assets/js/fileupload/jquery.fileupload-validate.js"></script>
					<script src="<?= base_url(); ?>assets/js/helpers.js"></script>
					<script src="<?= base_url(); ?>assets/js/fileupload/main.js"></script>
					<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
					<!--[if (gte IE 8)&(lt IE 10)]>
					<script src="<?= base_url(); ?>assets/js/fileupload/cors/jquery.xdr-transport.js"></script>
					<![endif]-->
				</div>
				<div class="upload_form_next">
					<div class="content_row">
						<label><?= $this->lang->line('fe_requestupload_menorowomen'); ?></label>
						<div class="radio_wrapper">
							<ul class="input_radio_choices">
								<li>
									<input name="sex" value="f" id="type_female" checked type="radio">
									<label for="type_female"></label>
									<label for="type_female"><?= $this->lang->line('fe_sex_f') ?></label>
								</li>
								<li>
									<input name="sex" value="m" id="type_male" type="radio">
									<label for="type_male"></label>
									<label for="type_male"><?= $this->lang->line('fe_sex_m') ?></label>
								</li>
								<li>
									<input name="sex" value="u" id="type_unisex" type="radio">
									<label for="type_unisex"></label>
									<label for="type_unisex"><?= $this->lang->line('fe_sex_u') ?></label>
								</li>
							</ul>
						</div>
						<div class="gender_error form_error"></div>
						<div class="clear"></div>
					</div>

					<div class="content_row">
						<div>
							<?php if ($this->session->userdata("sex") != "b") { ?>
<!--								<span class=""><?= $this->lang->line("fe_imageupload_cat_title") ?></span>-->
							<?php } ?>
							<div>
								<select name="category">
									<option value="-1"><?= $this->lang->line("fe_uploadpost_selectitem") ?></option>
									<?php
									foreach ($categories as $cat_row) {
										?>
										<optgroup label="<?= $cat_row["group_name"] ?>">
											<?php
											foreach ($cat_row["data"] as $data_row) {
												?>
												<option value="<?= $data_row["cat_id"] ?>"><?= $data_row["cat_name"] ?></option>
												<?php
											}
											?>
										</optgroup>
										<?php
									}
									?>
								</select>
							</div>
							<div class="cats_error form_error"></div>
						</div>
						<div class="clear"></div>
					</div>

					
					<div class="content_row">
						<?php if ($sponsored) { ?>
						<label><?= $this->lang->line("fe_add_post_url_sponsored") ?></label>
						<?php } else if ($is_brand) { ?>
						<label><?= $this->lang->line("fe_add_post_url_brand") ?></label>
						<?php } else { ?>
						<label><?= $this->lang->line("fe_add_post_url") ?></label>
						<?php } ?>
						<div class="url input_wrapper">
							<input class="url-selector" name="url" maxlength="600">
							<div class="url_error form_error"></div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="link_attr_wrapper">
					<?php if ($sponsored) { ?>
					<div class="content_row">
						<label class="checkbox_holder"><?= $this->lang->line("fe_add_is_sponsored") ?></label>
							<input class="sponsored_selector" type="checkbox" name="sponsored" value="1">
						<div class="clear"></div>
					</div>
					<?php } ?>
					<div class="content_row">
						<label><?= $this->lang->line('fe_tip_price') ?>:<span>*</span></label>
						<div class="clear"></div>
						<div class="left">
							<select name="post_currency" class="post_currency">
								<option value=""><?= $this->lang->line('fe_tip_currency') ?></option>
								<?php
								foreach ($this->lang->line("fe_currency") as $c_k => $c_v) {
									?>
									<option value="<?= $c_k ?>"><?= $c_v ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<div class="left">
							<input type="number" class="post_price" name="post_price" min="<?= $this->config->item("my_min_tip_price") ?>" max="<?= $this->config->item("my_max_tip_price") ?>" value="" />
						</div>
						<div class="clear"></div>
						<div class="currency_error form_error"></div>
						<div class="price_error form_error"></div>
					</div>
					</div>


					<div class="content_row">
						<label><?= $this->lang->line("fe_add_comment") ?></label>
						<div class="textarea input_wrapper">
							<textarea class="comment-selector" name="comment" maxlength="<?= $this->config->item("my_comment_max_length") ?>"></textarea>
							<div class="comments_error form_error"></div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="content_row">
						<label><?= $this->lang->line("fe_add_tags") ?></label>
						<div class="input_wrapper">
							<ul id="tag-input"></ul>
							<div class="tags_error form_error"></div>
							<input type="hidden" class="tags-selector" name="tags" value="">
						</div>
						<div class="clear"></div>
					</div>
					<div class="content_row submit_upload">
						<div class="fileupload-progress">
							<!-- The global progress bar -->
							<div class="progress progress-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
								<div class="progress-bar progress-bar-success" style="width:0%;"></div>
							</div>
							<!-- The extended global progress state -->
						</div>
						<div id="ajax-loading-image"><img src="<?= base_url() ?>assets/images/ajax-loader.gif"></div>
						<span class="btn start post_upload_btn" >
							<i class="fa fa-upload"></i>
							<span><?= $this->lang->line('fe_word_start-upload') ?></span>
						</span>
						<div class="clear"></div>
					</div>
				</div>
			</form>
		</div>
		<div class="clear"></div>
	</div><!-- end .single-content -->
	<!-- Begin Sidebar (right) -->
	<aside class="sidebar-buy">
	</aside><!-- end #sidebar  (right) -->
	<div class="clear"></div>
</div><!-- end .wrap-fullwidth  -->