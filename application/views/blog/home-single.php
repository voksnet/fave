<li>
    <div class="post-likes">
        <a target="_blank" class="facebook_share" href="https://www.facebook.com/sharer/sharer.php?u=<?= $this->config->item("cache_base_url") . "/blog/" . $blog->slug; ?>"><i class="fa fa-facebook-square"></i></a>
        <a title="<?= $this->lang->line("fe_word_like_this"); ?>" class="zilla-likes active-<?= $blog->id; ?>" id="blog_<?= $blog->id; ?>" href="#">
            <span class="zilla-likes-count"><?= $blog->likes; ?></span>
        </a>
    </div>
    <a href="<?= $this->config->item("cache_base_url") . "/blog/" . $blog->slug; ?>" title="<?= $blog->title . ": " . $blog->subtitle; ?>">
        <img src="<?= $this->config->item("cache_base_url") . "/cache/blog/" . $blog->slug . ".jpg"; ?>" alt="">
        <h3 class="ellipsis"><?= $blog->title; ?></h3>
        <span class="subtitle ellipsis"><?= $blog->subtitle; ?></span>
    </a>
</li>