<?php
//set_time_limit(4200);
/*
 * File: SimpleImage.php
 * Author: Simon Jarvis
 * Copyright: 2006 Simon Jarvis
 * Date: 08/11/06
 * Link: http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php
 * 
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 2 
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details: 
 * http://www.gnu.org/licenses/gpl.html
 *
 */

class SimpleImage {

	var $image;

	function SimpleImage($image) {
		//$this->image = imagecreatefromjpeg($filename);
		$this->image = $image;
		//$original_width=$this->getWidth();
	}

	function save($filename, $compression = 93, $permissions = null) {
		imagejpeg($this->image, $filename, $compression);
	}

	function output() {
		imagejpeg($this->image, '', $compression);
	}

	function getWidth() {
		return imagesx($this->image);
	}

	function getHeight() {
		return imagesy($this->image);
	}

	function resizeToHeight($height) {
		$ratio = $height / $this->getHeight();
		$width = $this->getWidth() * $ratio;
		$this->resize($width, $height);
	}

	function resizeToWidth($width) {
		$ratio = $width / $this->getWidth();
		$height = $this->getheight() * $ratio;
		$this->resize($width, $height);
	}

	function scale($scale) {
		$width = $this->getWidth() * $scale / 100;
		$height = $this->getheight() * $scale / 100;
		$this->resize($width, $height);
	}

	function resize($width, $height) {
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
	}

	function crop($width, $height) {
		//centering
		$x = 0;
		$y = 0;
		if ($this->getWidth() > $width) {
			$x = ($this->getWidth() - $width) / 2;
		}
		if ($this->getHeight() > $height) {
			$y = (($this->getHeight() - $height) / 2);
		}

		//echo ($x." - ".$y." - ".$this->getHeight()."<br />"."\n");

		$new_image = imagecreatetruecolor($width, $height);
		// Resize and crop
		imagecopyresampled(
				$new_image, $this->image, 0, 0, $x, $y, $width, $height, $width, $height
		);
		//imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
	}

}

?>