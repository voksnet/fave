<?php
//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Tags_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get($name) {
		return $this->db->get_where("tags", array("tag" => $name));
	}
	
	public function get_most() {
		$this->db->from('tags');
		$this->db->order_by("views", "desc");
		$this->db->order_by("views", "desc");
		$this->db->limit(10);
		$query = $this->db->get();
		$result = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result[] = $row->tag;
			}
		}
		return $result;
	}
	
	public function make_most_cloud() {
		$tags = $this->get_most();
		$result = "<h2>".$this->lang->line("fe_word_most_tags")."</h2>";
		$result .= "<ul>";
		foreach ($tags as $tag) {
			$tag_link = str_ireplace(" ", "%20", $tag);
			$result.='<li><h3><a href="'.$this->config->item("base_url")."/sh/".$tag_link.'">#'.$tag.'</a></h3></li>';
		}
		$result.="</ul>";
		return $result;
	}
	
}
?>