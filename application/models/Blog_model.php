<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Blog_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	private function increase_views($post) {
		// update blog views
		$views_go = ($this->session->userdata('blogviews') == null);

		if (!$views_go) {
			$views_go = !isset($this->session->userdata('blogviews')[$post->id]);
		}
		if ($views_go) {
			$views_go = $this->session->userdata("id") == null;
			if (!$views_go) {   // dont increase views of own publications
				$views_go = ($post->usersFK != $this->session->userdata("id"));
			}
		}

		$increase = 0;

		if ($views_go) {
			$this->db->where(array('id' => $post->id));
			$this->db->set('views', 'views+1', FALSE);
			$this->db->update('blog');
			$increase = 1;

			//update owner post views
			$this->db->where(array('id' => $post->usersFK));
			$this->db->set('post_views', 'post_views+1', FALSE);
			$this->db->update('users');

			$viewd_ids = array();
			if (is_array($this->session->userdata('blogviews'))) {
				$viewd_ids = $this->session->userdata('blogviews');
			}
			$viewd_ids[$post->id] = true;
			$this->session->set_userdata("blogviews", $viewd_ids);
		}

		return $increase;
	}

	public function get_posts($skip_blogFK = false, $limit, $offset = 0) {

		$this->db->limit($limit, $offset);
		$this->db->order_by("submitted", "desc");
		$data = array("del" => "no");
		if ($skip_blogFK) {
			$data["id !="] = $skip_blogFK;
		}
		$res = $this->db->get_where("blog", $data);
		$blogs = array();
		foreach ($res->result() as $b) {
			$blogs[] = $this->get_post($b->slug, false);
		}

		return $blogs;
	}

	public function get_post($slug, $bUpdateViews = true) {

		$this->load->helper("date_helper");

		$this->db->select("B.*, U.username, U.alias, U.selector AS user_selector, U.avatar, U.brand_name, U.website");
		$this->db->from('blog AS B');
		$this->db->join('users AS U', 'B.usersFK = U.id');
		$this->db->where(array("B.slug" => $slug, "B.del" => "no"));
		$post = $this->db->get();
//        $post = $this->db->get_where("blog", array("slug" => $slug, "del" => "no"));

		if (!$post->num_rows()) {
			return false;
		}

		$post = $post->row();

		$row = new stdClass();

		$date = dateToPeriods($post->submitted);
		$date_string = "";
		switch ($date["type"]) {
			case "minutes":
				$date_string = "fe_timebefore_minute";
				break;
			case "hours":
				$date_string = "fe_timebefore_hour";
				break;
			case "days":
				$date_string = "fe_timebefore_day";
				break;
		}
		if ($date["value"] > 1) {
			$date_string.="s";
		}

		if ($bUpdateViews) {
			$post->views += $this->increase_views($post);
		}

		$post->date_word = sprintf($this->lang->line("fe_timebefore_before"), $date["value"], $this->lang->line($date_string));
		$post->views_word = sprintf($this->lang->line($post->views == 1 ? "fe_word_view" : "fe_word_views"), $post->views);
		$post->post_avatar = base_url() . $this->config->item("my_avatars_folder") . $post->avatar;
		if ($post->alias == "") {
			$post->profile_url = base_url() . $this->config->item("my_urlparams_user") . "/" . $post->user_selector;
		} else {
			$post->profile_url = base_url() . $post->alias;
		}
		$post->post_username = $post->username;

		return $post;
	}

	public function add_like($post_id) {
		if ($this->session->userdata('logged_in')) {
			if (!$this->is_liked($post_id)) {


				$result = $this->get_likes($post_id);
				$total_likes = $result["likes"];
				$sql_data = array("usersFK" => $this->session->userdata('id'), "post_type" => "blog", "postsFK" => $post_id, "users_selectorFK" => $this->session->userdata('selector'));
				$this->db->insert('likes', $sql_data);

				//increase likes in blog post
				$this->db->where('id', $post_id);
				$this->db->set('likes', 'likes+1', FALSE);
				$this->db->update('blog');

				$total_likes+=1;

				//$this->users_model->update_user_rating($result["user_id"]);
				return $total_likes;
			}
		}
	}

	public function remove_like($post_id) {
		if ($this->session->userdata('logged_in')) {
			if ($this->is_liked($post_id)) {

				$result = $this->get_likes($post_id);
				$total_likes = $result["likes"];

				$this->db->where('postsFK=' . $post_id . " AND usersFK=" . $this->session->userdata('id') . " AND post_type='blog'");
				$this->db->delete('likes');

				//decrease likes in blog post
				$this->db->where('id', $post_id);
				$this->db->set('likes', 'likes-1', FALSE);
				$this->db->update('blog');

				$total_likes-=1;
				//$this->users_model->update_user_rating($result["user_id"]);
				return $total_likes;
			}
		}
	}

	public function is_liked($post_id) {
		if ($this->session->userdata('logged_in')) {
			$select_string = 'id';
			$this->db->select($select_string);
			$this->db->from('likes');
			$where_string = "postsFK = " . $post_id . " AND usersFK=" . $this->session->userdata('id') . " AND post_type = 'blog'";
			$this->db->where($where_string);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function toggle_like($post_id) {
		$result = array();
		$result["id"] = $post_id;
		if ($this->is_liked($post_id)) {
			$result["type"] = "remove";
			$result["value"] = $this->remove_like($post_id);
		} else {
			$result["type"] = "add";
			$result["value"] = $this->add_like($post_id);
		}
		return $result;
	}

	public function get_likes($post_id) {
		$this->db->select('likes');
		$this->db->from('blog');
		$this->db->where("id = " . $post_id . " AND del = 'no'");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result["likes"] = $row->likes;
			}
		} else {
			$result["likes"] = 0;
		}
		return $result;
	}

	function check_liked_posts($blog_content) {
		if ($this->session->userdata('logged_in')) {
			preg_match_all('/id="blog_(\d)+"/', $blog_content, $matches);
			foreach ($matches[1] as $blogFK) {
				if ($this->is_liked($blogFK)) {
					$blog_content = str_replace("active-" . $blogFK, "active", $blog_content);
				}
			}
		}
		return $blog_content;
	}

}

?>