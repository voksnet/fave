<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Menu_seo_model extends CI_Model {

	private $controler;
	private $header_menu = array();
//	private $footer_menu = array();
	private $seo = array();
	private $html_menu = array();
	public $menu_seo;

	public function __construct() {
		parent::__construct();
//		$this->load->helper(array('date'));
		//die($this->router->class);
		$this->controler = $this->router->class;
		$this->get_menu();
		$this->make_menu();
		$this->menu_seo["menu"] = $this->html_menu;
	}

	private function get_menu() {
		$this->load->model('post_model');
		$this->load->model('cache_model');
		$mobile_cats = unserialize($this->cache_model->get("cache.cat_menu_mobile.txt"));
		$this->header_menu = array(
			"home" => array(
				"title" => $this->lang->line("fe_site_menu")["home"]["title"],
				"url" => base_url(),
			),
			"recent" => array(
				"title" => $this->lang->line("fe_site_menu")["recent"]["title"],
				"url" => base_url("recent"),
			),
			"my_feed" => array(
				"title" => $this->lang->line("fe_site_menu")["my_feed"]["title"],
				"url" => base_url() . "me",
			),
			"fave" => array(
				"title" => $this->lang->line("fe_site_menu")["fave"]["title"],
				"url" => base_url() . "likes",
			),
			"blog" => array(
				"title" => $this->lang->line("fe_site_menu")["blog"]["title"],
				"url" => base_url() . "blog",
			),
			"fashion" => array(
				"title" => $this->lang->line("fe_site_menu")["fashion"]["title"],
				"url" => "#",
				"wide_content" => $this->cache_model->get("cache.cat_menu.html"),
				"sub" => $mobile_cats
			),
			"faq" => array(
				"title" => $this->lang->line("fe_site_menu")["faq"]["title"],
				"url" => base_url() . "faq",
			),
			"about_us" => array(
				"title" => $this->lang->line("fe_site_menu")["about_us"]["title"],
				"url" => base_url() . "about",
				"sub" => array(
					array(
						"title" => $this->lang->line("fe_site_menu")["how_it_works"]["title"],
						"url" => base_url() . "how",
					),
					array(
						"title" => $this->lang->line("fe_site_menu")["contact_us"]["title"],
						"url" => base_url() . "contacts",
					),
					array(
						"title" => $this->lang->line("fe_site_menu")["terms_of_use"]["title"],
						"url" => base_url() . "terms",
					),
				)
			),
		);
//		$this->footer_menu = array(
//			"about_us" => array(
//				"title" => $this->lang->line("fe_site_menu")["about_us"]["title"],
//				"url" => base_url() . "aboutus",
//			),
//			"how_it_works" => array(
//				"title" => $this->lang->line("fe_site_menu")["how_it_works"]["title"],
//				"url" => base_url() . "how",
//			),
//			"contact_us" => array(
//				"title" => $this->lang->line("fe_site_menu")["contact_us"]["title"],
//				"url" => base_url() . "contactus",
//			),
//		);
	}

	public function make_menu($type = "header") {


//				<ul>
//							<li class="current-menu-item">
//								<a href="#">Home</a>
//							</li>
//							<li class="menu-item-has-children">
//								<a href="#">Resume</a>
//								<ul class="sub-menu">
//									<li><a href="#">John smith</a></li>
//									<li><a href="#">Diana Richards</a></li>
//								</ul>
//							</li>
//							<li class="menu-item-has-children">
//								<a href="#">Features</a>
//								<ul class="sub-menu">
//									<li><a href="#">Contact Form 7</a></li>
//									<li><a href="#">Paginated Page</a></li>
//									<li><a href="#">Paginated Article</a></li>
//									<li><a href="#">Full Width</a></li>
//									<li><a href="#">Shortcodes</a></li>
//									<li><a href="#">Typography</a></li>
//									<li><a href="#">FAQ</a></li>
//									<li><a href="#">Sample Page</a></li>
//								</ul>
//							</li>
//							<li>
//								<a href="#">Contact</a>
//							</li>
//						</ul>
		$this->html_menu["header"] = '<ul class="responsive-menu">';
//		$this->html_menu["footer"] = "<ul>";
		foreach ($this->header_menu as $m_item) {

			if (!isset($m_item["sub"])) {
				$this->html_menu["header"].='<li><a href="' . $m_item["url"] . '" class="first_level">' . $m_item["title"] . '</a></li>';
			} else {
				$this->html_menu["header"].='<li class="menu-item-has-children"><a href="' . $m_item["url"] . '" class="first_level">' . $m_item["title"] . '</a>';
				if (isset($m_item["sub"])) {
					$wide_class = "";
					if (isset($m_item["wide_content"])) {
						$wide_class = " wide";
					}
					$this->html_menu["header"].='<ul class="sub-menu' . $wide_class . '">';
					foreach ($m_item["sub"] as $sub_item) {
						if (isset($sub_item["url"])) {
							$this->html_menu["header"].='<li class="header_m_item"><a href="' . $sub_item["url"] . '" class="second_level">' . $sub_item["title"] . '</a></li>';
						} else {
							$this->html_menu["header"].='<li class="header_m_item no_url">' . $sub_item["title"] . '</li>';
						}
					}
					if (isset($m_item["wide_content"])) {
						//wide_content
						$this->html_menu["header"].='<li class="cats"><div class="wide_content">';
						$this->html_menu["header"].= $m_item["wide_content"];
						$this->html_menu["header"].='</div></li>';
					}
					$this->html_menu["header"].='</ul>';
				}
			}
		}
		$this->html_menu["header"].='</ul>';
//		foreach ($this->footer_menu as $m_item) {
//			$this->html_menu["footer"].='<li><a href="' . $m_item["url"] . '">' . $m_item["title"] . '</a></li>';
//			$this->html_menu["footer"].='</ul></li>';
//		}
//		$this->html_menu["footer"].='</ul>';
	}

}

?>
