<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_model extends CI_Model {

	public $category = array();
	private $type_masonry = "";
	private $no_session = false;
	private $fake_session = array();

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('date'));
	}

	function addPost($insert_data) {
		if (count($insert_data) > 0) {
			if (stristr($insert_data["tags"], ",")) {
				$tags_array = explode(",", $insert_data["tags"]);
				foreach ($tags_array as $tag_key => $tag_string) {
					$tag_string = htmlspecialchars_decode($tag_string);
					$tag_string = mb_convert_case(trim($tag_string), MB_CASE_LOWER, "UTF-8");
					$tag_string = preg_replace('/[^\w_ &;-]+/u', '', $tag_string);
					$tags[] = $tag_string;
				}
				$insert_data["tags"] = implode(",", $tags);
			} else {
				$insert_data["tags"] = mb_convert_case(trim($insert_data["tags"]), MB_CASE_LOWER, "UTF-8");
				$tags[] = $insert_data["tags"];
			}
			$insert_data["tags"] = "," . $insert_data["tags"] . ",";

			$cats = $this->get_all_cats(true, false, true);
			$category = $insert_data["categoriesFK"];
			$insert_data["cat_strings"] = "," . $cats[$insert_data["categoriesFK"]] . ",";
			$insert_data["categoriesFK"] = "," . $insert_data["categoriesFK"] . ",";
//			$cats_in_post = explode(",", $insert_data["category"]);
//			foreach ($cats_in_post as $c_v) {
//				if (isset($cats[$c_v])) {
//					$insert_data["cat_strings"] .= $cats[$c_v] . ",";
//				}
//			}
			$image_data = getimagesize(FCPATH . $this->config->item("my_upload_big_images") . substr($insert_data["image"], 0, 2) . "/" . $insert_data["image"]);
			$insert_data["image_width"] = $image_data[0];
			$insert_data["image_height"] = $image_data[1];

			if ($insert_data["comment"] != "") {
				while (stristr($insert_data["comment"], "\r\n")) {
					$insert_data["comment"] = str_ireplace("\r\n", "\n", $insert_data["comment"]);
				}
				while (stristr($insert_data["comment"], "  ")) {
					$insert_data["comment"] = str_ireplace("  ", " ", $insert_data["comment"]);
				}
				while (stristr($insert_data["comment"], "\n\n\n")) {
					$insert_data["comment"] = str_ireplace("\n\n\n", "\n\n", $insert_data["comment"]);
				}
			}

			$this->db->insert('posts', $insert_data);
			$post_id = $this->db->insert_id();
			$this->load->model('events_model');
			$event_array = array();
			$event_array["from_users_selectorFK"] = $insert_data["users_selectorFK"];
//				$event_array["to_users_selectorFK"] = $selector;
			$event_array["item1"] = $post_id;
			$event_array["event_type"] = "post_follow_user";
			$event_array["user_type"] = "user";
			$this->events_model->add_event($event_array);

			$wants_data["usersFK"] = $insert_data["usersFK"];
			$wants_data["users_selectorFK"] = $insert_data["users_selectorFK"];
			$wants_data["categoriesFK"] = trim($insert_data["categoriesFK"], ",");
			$wants_data["categories_nameFK"] = trim($insert_data["cat_strings"], ",");
			$wants_data["postsFK"] = $post_id;
			$this->addWants($wants_data);

			$this->addTags($tags);
			$user_id = "";
			$isCLI = ( php_sapi_name() == 'cli' );
			if ($isCLI) {
				$user_id = $insert_data["usersFK"];
			}
			$this->increase_users_posts($category, $user_id);
			return $post_id;
		}
	}

	public function toggleWants($data) {
		if ($this->session->userdata('logged_in') == null) {
			return false;
		} else {
			if ($this->is_wanted($data["post_id"], $data["cat_id"], $this->session->userdata('selector'))) {
				$this->db->query("UPDATE wants SET del = '1' WHERE users_selectorFK = '" . $this->session->userdata('selector') . "' AND categoriesFK = " . $data["cat_id"] . " AND postsFK=" . $data["post_id"]);
				return "remove";
			} else {
				$insert_data["usersFK"] = $this->session->userdata('id');
				$insert_data["users_selectorFK"] = $this->session->userdata('selector');
				$insert_data["postsFK"] = $data["post_id"];
				$insert_data["categoriesFK"] = $data["cat_id"];
				$insert_data["categories_nameFK"] = $data["cat_name"];
//				$this->db->insert('wants', $insert_data);
				$this->addWants($insert_data);
				return "add";
			}
//			
		}
	}

	private function is_wanted($post_id, $cat_id, $selector) {
		$this->db->select("id");
		$this->db->from('wants');
		$where_string = " del='0' AND postsFK = " . $post_id . " AND users_selectorFK='" . $selector . "' AND categoriesFK = " . $cat_id;
		$this->db->where($where_string);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function addWants($data) {
		$this->load->model('events_model');
		$this->db->insert('wants', $data);

		$event_array = array();
		$event_array["from_users_selectorFK"] = $data["users_selectorFK"];
		$event_array["event_type"] = "wants";
		$event_array["item1"] = $data["postsFK"];
		$event_array["item2"] = $data["categoriesFK"];
		$event_array["user_type"] = "user";
		$this->events_model->add_event($event_array);
	}

	private function addTags($tags_array) {
		if (count($tags_array) > 0) {
			$insert_string = "INSERT INTO `tags` (`tag`,`posts`) VALUES ";
			foreach ($tags_array as $tag) {
				$tag = mb_convert_case($tag, MB_CASE_LOWER, "UTF-8");
				$insert_string.='("' . $tag . '", 1), ';
			}
			$insert_string = trim(trim($insert_string), ",") . " ON DUPLICATE KEY UPDATE posts=posts+1;";
			$this->db->query($insert_string);
			$this->post_model->update_tag_rating($tag);
		}
	}

	public function check_post_id($id) {
		$this->db->select("id");
		$this->db->where(array("id" => $id, "del" => '0', "post_type" => "post"));
		$query = $this->db->get("posts");
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function get_post($id, $where = "", $short = false, $ajax = false) {
//		$this->check_count();
		$select_string = 'a.id, a.date, a.image, a.image_width,  a.image_height, a.tips, a.likes, a.views, a.comment, a.usersFK, a.post_type, a.tags, a.categoriesFK AS category ,b.username, b.alias, b.selector AS user_selector, b.avatar, b.brand_name, b.website';
		$this->db->select($select_string);
		$this->db->from('posts AS a');
		$this->db->join('users AS b', 'a.usersFK = b.id');
		$this->db->where(array("a.id" => $id, "a.del" => '0'));
		//if ($active) {
		//	$this->db->where(array("categories.state" => "1", "cat_groups.state" => "1",));
		//}
		//$this->db->order_by('a.date DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = array();
			$this->load->model('tips_model');
			foreach ($query->result() as $row) {
				$posts_ids[] = $row->id;
				$prim_image = $row->image;
				$row->image = base_url() . $this->config->item("my_upload_big_images") . substr($row->image, 0, 2) . "/" . $row->image;
				$date = dateToPeriods($row->date);
				$date_string = "";
				switch ($date["type"]) {
					case "minutes":
						$date_string = "fe_timebefore_minute";
						break;
					case "hours":
						$date_string = "fe_timebefore_hour";
						break;
					case "days":
						$date_string = "fe_timebefore_day";
						break;
				}
				if ($date["value"] > 1) {
					$date_string.="s";
				}
				$row->comment = htmlspecialchars($row->comment);
				$row->date_word = sprintf($this->lang->line("fe_timebefore_before"), $date["value"], $this->lang->line($date_string));
				$row->views_word = sprintf($this->lang->line("fe_word_views"), $row->views);
				$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
				$row->can_delete = false;
				$row->can_delete = $this->users_model->access("posts", "delete", array("user_selector" => $row->user_selector, "date" => $row->date));
//				if ($row->avatar != "") {
//					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//				} else {
//					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . "user-female.jpg";
//				}
				if ($row->alias == "") {
					$row->profile_url = base_url() . $this->config->item("my_urlparams_user") . "/" . $row->user_selector;
				} else {
					$row->profile_url = base_url() . $row->alias;
				}
				$row->liked = false;

				$result = (array) $row;
				$result["post_categories"] = $this->get_several_cats($row->category, $id);
			}

			if ($result["image_width"] == 0) {
				$image_data = getimagesize(FCPATH . $this->config->item("my_upload_big_images") . substr($prim_image, 0, 2) . "/" . $prim_image);
				$result["image_width"] = $image_data[0];
				$result["image_height"] = $image_data[1];
				$this->db->query("UPDATE posts SET image_width = " . $result["image_width"] . ", image_height = " . $result["image_height"] . " WHERE id = " . $result["id"]);
			}

			if ($this->session->userdata('logged_in')) {
				$liked = $this->liked_posts($this->session->userdata('id'), array($id));
				if (is_array($liked)) {
					foreach ($liked as $l_k => $l_v) {
						$result["liked"] = true;
					}
				}
			}

			$result['tags'] = explode(",", trim($result['tags'], ","));
			if (!$short) {
				//get tips
				$result["tips"] = $result["post_categories"];
				$tips = $this->tips_model->get_tips($id, "", $ajax);
				foreach ($result["tips"] as $t_k => $t_v) {

					if (!isset($tips[$t_v["id"]])) {
						$result["tips"][$t_k]['count'] = 0;
					} else {
						$result["tips"][$t_k]['count'] = count($tips[$t_v["id"]]);
						$result["tips"][$t_k]["tips"] = $tips[$t_v["id"]];
					}
				}
			}

			//increase post views
			//TSM
			$views_go = ($this->session->userdata('views') == null);
			if (!$views_go) {
				$views_go = !isset($this->session->userdata('views')[$id]);
			}

			if ($views_go) {
				$views_go = $this->session->userdata("id") == null;
				if (!$views_go) {
					$views_go = ($result["usersFK"] != $this->session->userdata("id"));
				}
			}

			if ($views_go) {
				$this->db->where(array('id' => $id));
				$this->db->set('views', 'views+1', FALSE);
				$this->db->update('posts');
				$this->update_post_rating($id);

				//update owner post views
				$this->db->where(array('id' => $result["usersFK"]));
				$this->db->set('post_views', 'post_views+1', FALSE);
				$this->db->update('users');

				//increase cat views
				//$this->update_cat_views($result["category"] );
				$viewd_ids = array();
				if (is_array($this->session->userdata('views'))) {
					$viewd_ids = $this->session->userdata('views');
				}
				$viewd_ids[$id] = true;
				$this->session->set_userdata("views", $viewd_ids);
			}
			return $result;
		}
		return false;
	}

	private function get_several_cats($ids, $post_id) {
		$ids = trim($ids, ",");
		$ids_array = explode(",", $ids);
		if ($this->session->userdata('logged_in')) {
			$where = " a.id = " . implode(" OR a.id = ", $ids_array);
			$order_by = " a.id = " . implode(" DESC, a.id = ", $ids_array) . " DESC";
			$query = $this->db->query("SELECT a.id, a.name, a.param, IF(ISNULL(b.usersFK),'no','yes') AS wants  FROM categories AS a LEFT JOIN (SELECT usersFK, categoriesFK FROM wants WHERE users_selectorFK='" . $this->session->userdata('selector') . "' AND postsFK = " . $post_id . " AND del = '0' ) AS b ON a.id = b.categoriesFK WHERE " . $where . " ORDER BY " . $order_by);
		} else {
			$where = " `id`=" . implode(" OR id=", $ids_array);
			$order_by = " id =" . implode(" DESC, id=", $ids_array) . " DESC";
			$query = $this->db->query("SELECT id, name, param FROM categories WHERE " . $where . " ORDER BY " . $order_by);
		}
//		$this->db->order_by($order_by);
//		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$row->big_name = mb_convert_case($row->name, MB_CASE_UPPER, "UTF-8");
				if ($this->session->userdata('logged_in') == null) {
					$row->wants = "no";
				}
				$cats[] = (array) $row;
			}
		}
		return $cats;
	}

	private function unset_session_matrix($ui) {
		$session_data = $this->session->userdata('matrix');
		unset($session_data[$ui]);
		$this->session->set_userdata('matrix', $session_data);
		unset($session_data);
	}

	private function make_matrix($ui, $params = array(), $select = "", $where = "", $order = "", $page = 0, $limit = 0) {
		if ($this->no_session && $limit == 0) {
			$limit = $this->config->item("my_masonry_per_page_related");
		} else if ($limit == 0) {
			$limit = $this->config->item("my_masonry_large_per_page");
		}
		//wants and likes
		if ($select == "" && $where == "" && $order == "" && isset($this->session->userdata('matrix')[$ui])) {
			$select = $this->session->userdata('matrix')[$ui]["query"]["select"];
			$where = $this->session->userdata('matrix')[$ui]["query"]["where"];
			$order = $this->session->userdata('matrix')[$ui]["query"]["order"];
			$page = $this->session->userdata('matrix')[$ui]["big_page"];
		}
//		if ($where != "") {
//			$this->db->where($where . " AND a.del = '0'");
//		} else {
//			$this->db->where("a.del = '0'");
//		}

		$page_offset = ($page * $limit);
		$this->db->select($select);
		$this->db->from('posts AS a');
		$this->db->where($where);
		$this->db->join('users AS b', 'a.usersFK = b.id');
		$this->db->limit($limit + 1, $page_offset);
		$this->db->order_by($order);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[$row->id] = $row->id;
			}
			if (count($data) > $this->config->item("my_masonry_large_per_page")) {
				$more_big_page = true;
				while (count($data) > $this->config->item("my_masonry_large_per_page")) {
					array_pop($data);
				}
			} else {
				$more_big_page = false;
			}
			if ($this->session->userdata('matrix') != null) {
				$newdata = $this->session->userdata('matrix');
			}
			$newdata[$ui] = array(
				'small_page' => 0,
				'big_page' => $page,
				'more_big_page' => $more_big_page,
				'updated' => time(),
				'ui' => $ui,
				'type' => $this->type_masonry,
				'data_count' => count($data),
				'query' => array(
					"select" => $select,
					"where" => $where,
					"order" => $order
				),
				"matrix_params" => $params,
				'data' => $data
			);
			//$this->session->userdata('matrix')[$ui]
			$tips_matrix = array();
			$temp_tips_matrix = $this->make_tips_matrix($newdata[$ui]["data"]);
			if (count($temp_tips_matrix) > 0) {
				$newdata[$ui]["data_tips"] = $temp_tips_matrix;
			}
			if (!$this->no_session) {
				$sponsored_limit = 10;
			} else {
				$sponsored_limit = 800;
			}
			if ($newdata[$ui]["type"] != "own_sponsored") {
				//get_sponsored_matrix
				$temp_sponsored_matrix = $this->make_sponsored_matrix($sponsored_limit, $params);
				if (count($temp_sponsored_matrix) > 0) {
					$newdata[$ui]["data_sponsored"] = $temp_sponsored_matrix;
				}
			}
			if (!$this->no_session) {
				$this->session->set_userdata(array("matrix" => $newdata));
			} else {
				$this->fake_session = $newdata;
			}
		} else {
			return false;
		}
	}

	private function make_tips_matrix($data) {
		$where = " WHERE ( postsFK = " . implode(" OR postsFK=", $data) . " ) AND del = '0' ";
		$query_string = "
SELECT a.* FROM
	(
	SELECT id, postsFK, priority, thanks, 
		@currcount := IF( @currvalue = postsFK, @currcount + 1, 1) AS row,
		@currvalue := postsFK AS whatever
	FROM tips
	" . $where . "
	ORDER BY postsFK, priority DESC ,thanks DESC
	) AS a
WHERE a.row <=3";
		$result = array();
		$this->db->trans_start();
		$this->db->query("SET @currcount = NULL, @currvalue = NULL;");
		$query = $this->db->query($query_string);
		$this->db->trans_complete();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result[$row->postsFK][] = $row->id;
			}
		}
		return $result;
	}

	private function clean_session_matrix($ui = "") {
		$masonry_reset = false;
		if ($this->session->userdata('matrix') != null) {
			$current_session = $this->session->userdata('matrix');
			if ($ui != "" && isset($this->session->userdata('matrix')[$ui])) {
				$session_time = (time() - $this->session->userdata('matrix')[$ui]["updated"]);
				if ($session_time >= ($this->config->item("my_masonry_session_time") * 60 )) {
					unset($current_session[$ui]);
					$masonry_reset = true;
				}
			}

			if (count($current_session) > 0) {
				// wee keep only n sessions per session :-)
				foreach ($current_session as $key => $value) {
					$session_time = (time() - $this->session->userdata('matrix')[$key]["updated"]);
					if ($session_time >= ($this->config->item("my_masonry_session_time") * 60 )) {
						unset($current_session[$key]);
					} else {
						$total_sessions[$session_time] = $key;
					}
				}
				if (isset($total_sessions) > 0) {
					ksort($total_sessions);
					$total_sessions = array_reverse($total_sessions, true);
					if (count($total_sessions) > $this->config->item("my_max_matrix_sessions")) {
						$total_s = count($total_sessions);
						foreach ($total_sessions as $t_key => $t_val) {
							unset($current_session[$t_val]);
							unset($total_sessions[$t_key]);
							$total_s -= 1;
							if ($total_s == $this->config->item("my_max_matrix_sessions") - 1) {
								break;
							}
						}
					}
				}
			}
			$this->session->set_userdata('matrix', $current_session);
		}
		return $masonry_reset;
	}

	public function make_wants_matrix($selector, $ui) {
		$this->load->model('users_model');
		$limit = $this->config->item("my_masonry_large_per_page");
		if (isset($this->session->userdata('matrix')[$ui]) && count($this->session->userdata('matrix')[$ui]["data"]) == 0) {
			$page = $this->session->userdata('matrix')[$ui]["big_page"];
		} else {
			$page = 0;
		}

		$page_offset = ($page * $limit);
		$wants_profile = $this->users_model->get_short_user($selector);
		$wants_array = array();
		$data = array();
		$this->db->select("postsFK, categoriesFK, categories_nameFK");
		$this->db->from('wants');
		$this->db->where('users_selectorFK = "' . $selector . '" AND del = "0"');
		$this->db->order_by('date DESC');
		$this->db->limit($limit + 1, $page_offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
//				$wants_array[$row->postsFK] = $row->postsFK;
				$wants_array[$row->postsFK]["wants_cats"][] = $row->categoriesFK;
				$wants_array[$row->postsFK]["wants_cats_name"][] = $row->categories_nameFK;
				if (!isset($data[$row->postsFK])) {
					$data[$row->postsFK] = $row->postsFK;
				}
			}
//			die(print_r($wants_array));
			if (count($data) > $this->config->item("my_masonry_large_per_page")) {
				$more_big_page = true;
				while (count($data) > $this->config->item("my_masonry_large_per_page")) {
					array_pop($data);
				}
			} else {
				$more_big_page = false;
			}
			if ($this->session->userdata('matrix') != null) {
				$newdata = $this->session->userdata('matrix');
			}
			$newdata[$ui] = array(
				'small_page' => 0,
				'big_page' => $page,
				'more_big_page' => $more_big_page,
				'updated' => time(),
				'ui' => $ui,
				'type' => $this->type_masonry,
				'data_count' => count($data),
				'data' => $data,
				'wants_data' => $wants_array,
				'wants_profile' => $wants_profile
			);
			$tips_matrix = array();
			$temp_tips_matrix = $this->make_tips_matrix($newdata[$ui]["data"]);
			if (count($temp_tips_matrix) > 0) {
				$newdata[$ui]["data_tips"] = $temp_tips_matrix;
			}
			$this->session->set_userdata(array("matrix" => $newdata));
		} else {
			return false;
		}
	}

	public function make_liked_matrix($selector, $ui) {
		$limit = $this->config->item("my_masonry_large_per_page");
		if (isset($this->session->userdata('matrix')[$ui]) && count($this->session->userdata('matrix')[$ui]["data"]) == 0) {
			$page = $this->session->userdata('matrix')[$ui]["big_page"];
		} else {
			$page = 0;
		}

		$page_offset = ($page * $limit);

		$this->db->select("a.postsFK");
		$this->db->from('likes AS a');
		$this->db->where('a.users_selectorFK = "' . $selector . '" AND b.del = "0"');
		$this->db->join('posts AS b', 'a.postsFK = b.id');
		$this->db->order_by('a.date_liked DESC');
		$this->db->limit($limit + 1, $page_offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[$row->postsFK] = $row->postsFK;
			}
			if (count($data) > $this->config->item("my_masonry_large_per_page")) {
				$more_big_page = true;
				while (count($data) > $this->config->item("my_masonry_large_per_page")) {
					array_pop($data);
				}
			} else {
				$more_big_page = false;
			}
			if ($this->session->userdata('matrix') != null) {
				$newdata = $this->session->userdata('matrix');
			}
			$newdata[$ui] = array(
				'small_page' => 0,
				'big_page' => $page,
				'more_big_page' => $more_big_page,
				'updated' => time(),
				'ui' => $ui,
				'type' => $this->type_masonry,
				'data_count' => count($data),
				'data' => $data
			);
			$tips_matrix = array();
			$temp_tips_matrix = $this->make_tips_matrix($newdata[$ui]["data"]);
			if (count($temp_tips_matrix) > 0) {
				$newdata[$ui]["data_tips"] = $temp_tips_matrix;
			}
			$this->session->set_userdata(array("matrix" => $newdata));
		} else {
			return false;
		}
	}

	private function make_search_matrix($ui, $params = array(), $query_sql = "", $order = "", $page = 0, $limit = 0) {
		if ($limit == 0) {
			$limit = $this->config->item("my_masonry_large_per_page");
		}
		if ($query_sql == "" && isset($this->session->userdata('matrix')[$ui])) {
			$query_sql = $this->session->userdata('matrix')[$ui]["query"]["query"];
			$order = $this->session->userdata('matrix')[$ui]["query"]["order"];
		}
		$page_offset = ($page * $limit);
		//, ( ROUND( (UNIX_TIMESTAMP(a.date))/' . $this->config->item("my_periods_for_posts") . ') ) AS epoch,
		$this->db->limit($limit + 1, $page_offset);
		$limit_query = " LIMIT " . $page_offset . ", " . $limit;
		$query = $this->db->query($query_sql . " ORDER BY " . $order . $limit_query);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[$row->id] = $row->id;
			}
			if (count($data) > $this->config->item("my_masonry_large_per_page")) {
				$more_big_page = true;
				while (count($data) > $this->config->item("my_masonry_large_per_page")) {
					array_pop($data);
				}
			} else {
				$more_big_page = false;
			}
			if ($this->session->userdata('matrix') != null) {
				$newdata = $this->session->userdata('matrix');
			}
			$newdata[$ui] = array(
				'small_page' => 0,
				'big_page' => $page,
				'more_big_page' => $more_big_page,
				'updated' => time(),
				'matrix_params' => $params,
				'ui' => $ui,
				'type' => $this->type_masonry,
				'data_count' => count($data),
				'query' => array(
					"query" => $query_sql,
					"order" => $order
				),
				'data' => $data
			);
			$tips_matrix = array();
			$temp_tips_matrix = $this->make_tips_matrix($newdata[$ui]["data"]);
			if (count($temp_tips_matrix) > 0) {
				$newdata[$ui]["data_tips"] = $temp_tips_matrix;
			}
			if (!$this->no_session) {
				$sponsored_limit = 10;
			} else {
				$sponsored_limit = 800;
			}
			$temp_sponsored_matrix = $this->make_sponsored_matrix($sponsored_limit, $params);
			if (count($temp_sponsored_matrix) > 0) {
				$newdata[$ui]["data_sponsored"] = $temp_sponsored_matrix;
			}
			if (!$this->no_session) {
				$this->session->set_userdata(array("matrix" => $newdata));
			} else {
				$this->fake_session = $newdata;
			}
		} else {
			return false;
		}
	}

	private function get_short_tips($post_array) {
		$result = array();
		$where = " a.del ='0' AND ( a.id = " . implode(" OR a.id=", $post_array) . " )";
		$this->db->select("a.*, b.username, b.alias, b.selector, b.avatar, c.name, c.param");
		$this->db->where($where);
		$this->db->from('tips AS a ');
		$this->db->join('users AS b', 'a.users_selectorFK = b.selector', "left");
		$this->db->join('categories AS c', 'a.categoriesFK = c.id');
		$query = $this->db->get();
		$result_array = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				if ($row->username) {
					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//					if ($row->avatar != "") {
//						$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//					} else {
//						$row->avatar = base_url() . $this->config->item("my_avatars_folder") . "user-female.jpg";
//					}
				}
				$row->currency_sign = $this->lang->line("fe_currency")[$row->currency];
				$row->new_link = $row->link;
				$row->cat_big_name = mb_convert_case($row->name, MB_CASE_UPPER, "UTF-8");
				if ($row->image) {
					$row->image = $row->image = base_url() . $this->config->item("my_upload_tips_folder") . substr($row->image, 0, 2) . "/" . $row->image;
				}
				$result_array[$row->id]["id"] = $row->id;
				$result_array[$row->id]["provider_domain"] = $row->provider_domain;
				$result_array[$row->id]["user"] = $row->username;
				$result_array[$row->id]["selector"] = $row->selector;
				$result_array[$row->id]["alias"] = $row->alias;
				if ($row->alias == "") {
					$result_array[$row->id]["profile_url"] = base_url() . $this->config->item("my_urlparams_user") . "/" . $row->selector;
				} else if ($row->selector != "") {
					$result_array[$row->id]["profile_url"] = base_url() . $row->alias;
				} else {
					$result_array[$row->id]["profile_url"] = "";
				}
				$result_array[$row->id]["brand"] = $row->brand;
				$result_array[$row->id]["price"] = $row->price;
				$result_array[$row->id]["currency_sign"] = $row->currency_sign;
				$result_array[$row->id]["new_link"] = $row->new_link;
				$result_array[$row->id]["image"] = $row->image;
				$result_array[$row->id]["avatar"] = $row->avatar;
				$result_array[$row->id]["cat_name"] = $row->name;
				$result_array[$row->id]["param"] = $row->param;
				$result_array[$row->id]["cat_big_name"] = $row->cat_big_name;
			}
		}
		return $result_array;
	}

	public function get_msnry($input_data = "") {
		if (IS_AJAX) {
			$data = $this->get_posts();
		} else {
			$sponsored = "post";
			if (isset($input_data["own_sponsored"]) && $input_data["own_sponsored"]) {
				$sponsored = "sponsored";
			}
			$data = $this->get_posts($input_data, $sponsored);
		}
		if (isset($input_data["featured"])) {
			return $data;
		}
		if (!$data) {
			$result["none"] = true;
			$result["error_message"] = $this->lang->line("fe_word_error_no_result");
		} else {
			if (is_array($data) && isset($data["posts"]) && count($data["posts"]) > 0) {
				$result = $data["post_info"];
				$result["ok"] = true;
				$result["first_image"] = base_url() . $this->config->item("my_upload_big_images") . current($data["posts"])["image"];
				$result["first_short_description"] = "";
				if (current($data["posts"])["comment"]) {
					if (mb_strlen(current($data["posts"])["comment"], "utf8") > 156) {
						$result["first_short_description"] = mb_substr(strip_tags(current($data["posts"])["comment"]), 0, 156, "utf8") . " ... ";
					} else {
						$result["first_short_description"] = strip_tags(current($data["posts"])["comment"]);
					}
					$result["first_short_description"] = str_ireplace("\n", " ", $result["first_short_description"]);
					$result["first_short_description"] = str_ireplace("\r", " ", $result["first_short_description"]);
				}
				$result["posts"] = $this->build_masonry($data["posts"]);
			} else if (!$data["reset"]) {
				$result["none"] = true;
				$result["error_message"] = $this->lang->line("fe_word_error_no_result");
			} else {
				$result = $data;
			}
		}
		if (IS_AJAX) {
			echo json_encode($result);
		} else {
			return $result;
		}
	}

	private function build_masonry($source_data) {
//		die(print_r($source_data));
		$ajax_result = array();
		foreach ($source_data as $data) {
//			if (trim($profile) != "") {
//				$data_open_view = ' data-open_view="' . $data["id"] . '" class="open-view" ';
//			} else {
//				$data_open_view = " ";
//			}
			$post_class = "";
			$open_view = "open-view";
			$link_target = "";
			$full_avatar = "";
			$facebook_share = "";
			$slug = "";
			if ($data["post_type"] == "post") {
				$facebook_share = base_url() . $this->config->item("my_urlparams_view") . "/" . $data["id"];
				$link = base_url() . $this->config->item("my_urlparams_view") . '/' . $data["id"];
				//makes slug from tags
				//die(print_r($data));
				$slug = str_ireplace($data["cat_strings"], ",", $data["tags"]);
				$slug = $data["cat_strings"] . trim(trim($slug, ","));
				//psl - post slug
				$slug = "psl/" . str_ireplace(array(",", " "), array("_", "-"), trim(trim($slug, ",")));
				$link.="/" . $slug;
			} else if ($data["post_type"] == "sponsored") {
				$full_avatar = " full_avatar";
				$link = $data["url"];
				$facebook_share = $link;
				$open_view .= " external";
				$link_target = ' target="_blank" ';
				$post_class = " sponsored";
			}
			$data_open_view = ' data-open_view="' . $data["id"] . '" data-slug="' . $slug . '" class="' . $open_view . '" ';
			$liked_status = ($data['liked'] ? " active" : "");
			$short_tips = "";
			if (isset($data["short_tips"]) && count($data["short_tips"]) > 0) {
				foreach ($data["short_tips"] as $s_tip) {
					$short_tips .= $this->make_short_tip($s_tip);
				}
			} else if ($data["post_type"] == "sponsored") {
				$short_tips .= $this->make_short_tip($data, true);
			}
			$short_comment = "";
			if ($data['comment'] != "") {
				if (mb_strlen($data['comment'], "utf8") > 156) {
					$short_comment = mb_substr(strip_tags($data['comment']), 0, 156, "utf8") . " ... ";
				} else {
					$short_comment = strip_tags($data['comment']);
				}
				$short_comment = str_ireplace("\n", " ", $short_comment);
				$short_comment = str_ireplace("\r", " ", $short_comment);
			}
			$go_delete = "";
			if ($data["can_delete"]) {
				$go_delete = " ok";
			}
//			die(print_r($data));
			if (isset($data['date_word']) && !isset($data["wants_data"])) {
				$second_row_text = '<br />
			<div class="at-time">' . $data['date_word'] . '</div>';
			} else if ($data["post_type"] == "sponsored") {
				$second_row_text = '<div class="at-time">' . $this->lang->line('fe_word_sponsored') . '</div>';
			} else if (isset($data["wants_data"])) {
				$second_row_text = '<br />
			<div class="at-time">' . $this->lang->line("fe_word_wants_this") . '</div>';
				;
			} else {
				$second_row_text = "";
			}
			$mason = "main";
//			$ajax_result[] = '<li class="post ex34 post_'.$data["id"].' sponsored" style="position: absolute; left: 0px; top: 0px; display: list-item;"><img name="'.$data["id"].'" src="'.base_url().'assets/images/pixel.png">sponsored content ' . $data["id"] . '</li>';
			//WANTS
			$wants = "";
			if (isset($data["wants_data"])) {
				$post_class.=" wants";
				$wants = implode(", ", $data["wants_data"]);
			}
			if (isset($data["wants_profile"])) {
				$data["avatar"] = $data["wants_profile"]["avatar"];
				$data["profile_url"] = $data["wants_profile"]["profile_url"];
				$data["username"] = $data["wants_profile"]["username"];
			}
			$ajax_result[] = '<li class = "post ex34 post_' . $data["id"] . $post_class . '">
	<div class = "author-box">
		<div class="post_author_delete' . $go_delete . '"><span class="fa fa-times" data-post_del = "' . $data["id"] . '"  data-mason="' . $mason . '"></span></div>
		<div class = "at-img' . $full_avatar . '">
			<a href = "' . $data["profile_url"] . '"><img  src = "' . $data["avatar"] . '" class = "avatar" height = "34" width = "34"  alt="' . $data['username'] . '"/></a>
		</div>
		<div class = "at-links">
			<a href = "' . $data["profile_url"] . '" title = "' . $this->lang->line("fe_word_post_by") . ' ' . $data['username'] . '" rel = "author">' . $data['username'] . '</a>
			' . $second_row_text . '
		</div>
		<div class="clear"></div>
	</div>
	<div class="wants-box">' . $wants . '</div>
	<div class="post-likes">
		<a href="https://www.facebook.com/sharer/sharer.php?u=' . $facebook_share . '" class="facebook_share"  target="_blank"><i class="fa fa-facebook-square"></i></a>
		<a href="#" id="zilla_' . $data["id"] . '" class="zilla-likes' . $liked_status . '" title="' . $this->lang->line("fe_word_like_this") . '">
			<span class="zilla-likes-count">' . $data['likes'] . '</span>
		</a>
	</div>
	<a href="' . $link . '"' . $data_open_view . 'title="' . $short_comment . '"' . $link_target . '>
		<img src="' . base_url() . $this->config->item("my_upload_small_images") . $data['image'] . '" id="postitm-' . $data['id'] . '" class="attachment-thumbnail-blog-masonry wp-post-image" alt="' . $short_comment . '">
	</a>
	<div class="clear"></div>
	<div class="post_tips_holder">
	' . $short_tips . '
	</div>
</li>';
		}
		return $ajax_result;
	}

	public function make_short_tip($s_tip, $sponsored = false) {
		$temp_provider = "";
		$alt_provider = "";
		$alt_price = "";
		$img = "";
		$img_class = "";
		$link = "";
		$price = "";
		$currency = "";
		$category = "";
		$post_class = "";
		$short_tips = "";
		$tip_alt = "";
		if (!$sponsored) {
			if ($s_tip["brand"] != "") {
				$temp_provider = $s_tip["brand"];
			} else {
				$temp_provider = $s_tip["provider_domain"];
			}
			$alt_provider = " : " . $temp_provider;
			$temp_provider = '<div class="a_post_tip_provider"><a href="' . $s_tip["new_link"] . '" target="_blank">' . $temp_provider . '</a></div>';
			if ($s_tip["image"] != "") {
				$img = $s_tip["image"];
			} else {
				$img = $s_tip["image"];
			}
			$link = $s_tip["new_link"];
			$price = $s_tip["price"];
			$currency = $s_tip["currency_sign"];
			$category = $s_tip["cat_big_name"];
		} else {
			$img = base_url() . $this->config->item("my_upload_small_images") . $s_tip["image"];
//			$img_class = " no_image";
			$link = $s_tip["url"];
			if ($s_tip["post_price"] != 0) {
				$price = $s_tip["post_price"];
				$currency = $s_tip["post_currency"];
				$category = $s_tip["cat_big_name"];
			}
		}
		if ($price != "") {
			$alt_price = " : " . $price . " " . $currency;
		}
		$tip_alt = $category . $alt_provider . $alt_price;
		if (empty($img)) {
			$img = base_url("assets/images/fave_tips_placeholder.png");
		}
		if ($category != "") {
			$short_tips = '
		<div class="a_post_tip">
			<div class="a_post_tip_image initial' . $img_class . '">
				<a href="' . $link . '" target="_blank"><img src="' . $img . '" alt="' . $tip_alt . '"></a>
			</div>
			<div class="a_post_tip_image_data">
				<div class="a_post_tip_cat"><a href="' . $link . '" target="_blank">' . $category . '</a></div>
				<div class="a_post_tip_price">' . $price . " " . $currency . '</div>
				' . $temp_provider . '
			</div>
			<div class="a_post_tip_buy_button"><a href="' . $link . '" target="_blank">' . $this->lang->line("fe_tip_word_buy") . '</a></div>
			<div class="clear"></div>
		</div>
';
		}
		return $short_tips;
	}

	private function make_sponsored_matrix($limit = 800, $params = array(), $own = false) {
		$result = array();
		$this->db->select("id");
		$this->db->from('posts');
		$where = "";
		$order_by = "";
		if (isset($params["search"]) && $params["search"] != "") {
			$where .= " AND (cat_strings LIKE '%" . $params["search"] . "%' OR cat_strings != '' OR tags LIKE '%" . $params["search"] . "%' OR tags != '' OR comment LIKE '%" . $params["search"] . "%' OR comment = '' OR comment != '') ";
			$order_by = "case 
    when cat_strings LIKE '%" . $params["search"] . "%' then 1 
	when tags LIKE '%" . $params["search"] . "%' then 2 
	when comment LIKE '%" . $params["search"] . "%' then 3 
    else 4
end";
		}
		if (isset($params["tag"]) && $params["tag"] != "") {
			$where .= " AND (cat_strings LIKE '%" . $params["tag"] . "%' OR cat_strings != '' OR tags LIKE '%" . $params["tag"] . "%' OR tags != '' ) ";
			$order_by = "case 
    when cat_strings LIKE '%" . $params["tag"] . "%' then 1 
    else 2
end";
		}
		if (isset($params["cat"]) && $params["cat"] != "") {
			$where .= " AND (cat_strings LIKE '" . $params["cat"] . "' OR cat_strings != '' )";
			$order_by = "case 
    when cat_strings LIKE '" . $params["cat"] . "' then 1 
    else 2
end";
		}
		if ($order_by == '') {
			$order_by = " rand() ";
		}
		$this->db->where(' del = "0" AND active = "1" AND post_type = "sponsored" ' . $where);
		$this->db->limit($limit);
		$this->db->order_by($order_by);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result[$row->id] = $row->id;
			}
		}
		return $result;
	}

	private function get_sponsored_matrix($ui, $current_count) {
		$final = false;
		$go = false;
		$limit = 1;
		foreach ($this->config->item("my_sponsored_positions") as $p_step) {
			if ($p_step <= $current_count) {
				$limit++;
			}
		}
		if ($limit != 0) {
			$go = true;
		}
		if ($go) {
			if ($this->no_session) {
				$session_data = $this->fake_session;
			} else if (isset($this->session->userdata('matrix')[$ui])) {
				$session_data = $this->session->userdata('matrix');
			}
			if (isset($session_data) && count($session_data) > 0) {
				$select_data = array_slice($session_data[$ui]["data_sponsored"], 0, $limit, true);
				if (count($session_data[$ui]["data_sponsored"]) - count($select_data) <= 0) {
					$final = true;
				}
				foreach ($select_data as $val) {
					unset($session_data[$ui]["data_sponsored"][$val]);
				}
				////		SELECT a.*,b.name FROM (    SELECT id, TRIM(BOTH ',' FROM categoriesFK) AS cat FROM posts     ) AS a LEFT JOIN categories AS b ON a.cat = b.id
				//PENDING: process url to trace google analytics
				$select_string = 'a.id, a.date, ( ROUND( (UNIX_TIMESTAMP(a.date))/' . $this->config->item("my_periods_for_posts") . ') ) AS epoch, a.image, a.tips, a.likes, a.views, a.rating, a.comment, a.usersFK, a.post_type, a.post_price, a.post_currency, a.url , b.username, b.alias, b.selector AS user_selector, a.cat_strings, b.avatar, b.brand_name, b.website';
				$this->db->select($select_string);
				$this->db->from('posts AS a');
				$this->db->join('users AS b', 'a.usersFK = b.id');
				$where = ' a.id IN ("' . implode('","', $select_data) . '")';
				$order = ' FIELD (a.id,"' . implode('","', $select_data) . '")';
				$this->db->where($where);
				$this->db->order_by($order);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					$posts_array = array();
					$posts_ids = array();
					foreach ($query->result() as $row) {
						$posts_ids[] = $row->id;
						$row->image = substr($row->image, 0, 2) . "/" . $row->image;
						$row->comment = htmlspecialchars($row->comment);
						$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
						$row->cat_big_name = trim(mb_convert_case($row->cat_strings, MB_CASE_UPPER, "UTF-8"), ",");
						$row->can_delete = false;
						$row->can_delete = $this->users_model->access("posts", "delete", array("user_selector" => $row->user_selector, "date" => $row->date));
						if ($row->alias == "") {
							$row->profile_url = base_url() . $this->config->item("my_urlparams_user") . "/" . $row->user_selector;
						} else {
							$row->profile_url = base_url() . $row->alias;
						}
						$row->liked = false;
						if ($row->post_currency != "") {
							$row->post_currency = $this->lang->line("fe_currency")[$row->post_currency];
						} else {
							$row->post_price = "";
						}
						$posts_array[$row->id] = (array) $row;
					}
					if ($this->session->userdata('logged_in')) {
						$liked = $this->liked_posts($this->session->userdata('id'), $posts_ids);
						if (is_array($liked)) {
							foreach ($liked as $l_k => $l_v) {
								$posts_array[$l_k]["liked"] = true;
							}
						}
					}
					if (!$this->no_session) {
						if ($final) {
							$sponsored_limit = 800;
							$temp_sponsored_matrix = $this->make_sponsored_matrix($sponsored_limit, $session_data[$ui]["matrix_params"]);
							if (count($temp_sponsored_matrix) > 0) {
								$session_data[$ui]["data_sponsored"] = $temp_sponsored_matrix;
							}
						}
						$this->session->set_userdata('matrix', $session_data);
					}
					return $posts_array;
				}
			}
		}
		return false;
	}

	private function get_matrix($ui, $sponsored = true) {
		if ($this->no_session) {
			$session_data = $this->fake_session;
		} else if (isset($this->session->userdata('matrix')[$ui])) {
			$session_data = $this->session->userdata('matrix');
		}
		if (isset($session_data) && count($session_data) > 0) {
			$final = false;
			$last_in_large_page = false;
			//my_masonry_per_page_related
			//
			if ($this->no_session) {
				$limit = $this->config->item("my_masonry_per_page_related");
			} else {
				$limit = $this->config->item("my_masonry_per_page");
			}
			$select_data = array_slice($session_data[$ui]["data"], 0, $limit, true);

			foreach ($select_data as $val) {
				unset($session_data[$ui]["data"][$val]);
			}
			$session_data[$ui]['data_count'] = count($session_data[$ui]["data"]);
			if (count($session_data[$ui]["data"]) == 0 && !$session_data[$ui]["more_big_page"]) {
				$final = true;
			}

			if (count($session_data[$ui]["data"]) == 0 && $session_data[$ui]["more_big_page"]) {
				$last_in_large_page = true;
			}
			//PENDING: process url to trace google analytics
			$select_string = 'a.id, a.date, ( ROUND( (UNIX_TIMESTAMP(a.date))/' . $this->config->item("my_periods_for_posts") . ') ) AS epoch,a.image, a.tips, a.likes, a.views, a.rating, a.comment, a.usersFK, a.cat_strings, a.post_type, a.post_price, a.post_currency, a.url, a.tags ,b.username, b.alias, b.selector AS user_selector, b.avatar, b.brand_name, b.website';

			$this->db->select($select_string);
			$this->db->from('posts AS a');
			$this->db->join('users AS b', 'a.usersFK = b.id');

			$where = ' a.id IN ("' . implode('","', $select_data) . '")';
			$order = ' FIELD (a.id,"' . implode('","', $select_data) . '")';
			$this->db->where($where);
			$this->db->order_by($order);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$posts_array = array();
				$posts_array["post_info"] = array();
				$posts_ids = array();

				foreach ($query->result() as $row) {
					$posts_ids[] = $row->id;
					$row->image = substr($row->image, 0, 2) . "/" . $row->image;
					$date = dateToPeriods($row->date);
					$date_string = "";
					switch ($date["type"]) {
						case "minutes":
							$date_string = "fe_timebefore_minute";
							break;
						case "hours":
							$date_string = "fe_timebefore_hour";
							break;
						case "days":
							$date_string = "fe_timebefore_day";
							break;
					}
					if ($date["value"] > 1) {
						$date_string.="s";
					}
					$row->comment = htmlspecialchars($row->comment);
					$row->date_word = sprintf($this->lang->line("fe_timebefore_before"), $date["value"], $this->lang->line($date_string));
					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
					$row->can_delete = false;
					$row->can_delete = $this->users_model->access("posts", "delete", array("user_selector" => $row->user_selector, "date" => $row->date));
					$row->cat_big_name = trim(mb_convert_case($row->cat_strings, MB_CASE_UPPER, "UTF-8"), ",");
//					$row->tags = trim(trim($row->tags, ","));
//can delete
//					if ($row->avatar != "") {
//						$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//					} else {
//						$row->avatar = base_url() . $this->config->item("my_avatars_folder") . "user-female.jpg";
//					}
					if ($row->alias == "") {
						$row->profile_url = base_url() . $this->config->item("my_urlparams_user") . "/" . $row->user_selector;
					} else {
						$row->profile_url = base_url() . $row->alias;
					}
					$row->liked = false;
					$posts_array["posts"][$row->id] = (array) $row;
					if (isset($session_data[$ui]["data_tips"]) && count($session_data[$ui]["data_tips"]) > 0 && isset($session_data[$ui]["data_tips"][$row->id])) {
						$posts_array["posts"][$row->id]["short_tips"] = $this->get_short_tips($session_data[$ui]["data_tips"][$row->id]);
						unset($session_data[$ui]["data_tips"][$row->id]);
					}

					//WANTS
					if (isset($session_data[$ui]["wants_data"][$row->id])) {
						$posts_array["posts"][$row->id]["wants_data"] = $session_data[$ui]["wants_data"][$row->id]["wants_cats_name"];
						unset($session_data[$ui]["wants_data"][$row->id]);
					}
					if (isset($session_data[$ui]["wants_profile"])) {
						$posts_array["posts"][$row->id]["wants_profile"] = $session_data[$ui]["wants_profile"];
					}
				}
				if ($this->session->userdata('logged_in')) {
					$liked = $this->liked_posts($this->session->userdata('id'), $posts_ids);
					if (is_array($liked)) {
						foreach ($liked as $l_k => $l_v) {
							$posts_array["posts"][$l_k]["liked"] = true;
						}
					}
				}
				if (!$this->no_session) {
					$posts_array["post_info"]["final"] = false;
					$posts_array["post_info"]["special_more"] = false;
					$posts_array["post_info"]["reset"] = false;

					if ($final) {
						$posts_array["post_info"]["final"] = true;
					} if ($last_in_large_page) {
						$session_data[$ui]["big_page"] ++;
						$session_data[$ui]["small_page"] = 0;
						$posts_array["post_info"]["special_more"] = true;
						$posts_array["post_info"]["reset"] = true;
					}
					if (count($session_data[$ui]["data"]) > 0) {
						$session_data[$ui]["small_page"] ++;
					}
					if (!$final) {
						$session_data[$ui]["updated"] = time();
					} else {
						$session_data[$ui]["updated"] = 0;
					}
				} else {
					$posts_array["post_info"]["fk"] = true;
					unset($session_data[$ui]);
				}

				if (isset($session_data[$ui]["data_tips"]) && count($session_data[$ui]["data_tips"]) == 0) {
					unset($session_data[$ui]["data_tips"]);
				}

				if (isset($session_data[$ui]["wants_data"]) && count($session_data[$ui]["wants_data"]) == 0) {
					unset($session_data[$ui]["wants_data"]);
				}

				$this->session->set_userdata('matrix', $session_data);

				if ($sponsored) {
					$sponsored_array = $this->get_sponsored_matrix($ui, count($posts_array["posts"]));
					if (is_array($sponsored_array) && count($sponsored_array) > 0) {
						$sponsored_positions = $this->config->item("my_sponsored_positions");
						$sponsored_count = 0;
						foreach ($sponsored_positions as $sp_position) {
							if (count($sponsored_array) > 0 && ($sponsored_count == 0 || ($sp_position <= count($posts_array["posts"]) ) )) {

								$sp_position = $sp_position - 1;
								$spp_key = key($sponsored_array);
								$insert_array[$spp_key] = $sponsored_array[$spp_key];
								unset($sponsored_array[$spp_key]);
								$posts_array["posts"] = array_slice($posts_array["posts"], 0, $sp_position, true) +
										$insert_array +
										array_slice($posts_array["posts"], $sp_position, count($posts_array["posts"]) - $sp_position, true);
								$sponsored_count++;
							}
						}
					}
				}
				if ($this->no_session) {
					$session_data = array();
					$this->no_session = false;
				}
				return $posts_array;
			}
		} else {
			return false;
		}
	}

	public function get_posts($input_data = "", $post_type = "post") {
		$sponsored = true;
		$matrix_params = array();
		$where_type = "post_type = '" . $post_type . "' AND ";

		if ($input_data == "") {
			$input_data = $this->input->get();
		};
		$this->type_masonry = "";
		if (isset($input_data["ui"])) {
			$ui = $input_data["ui"];
		} else {
			$ui = "";
			$this->no_session = true;
		}
//PENDING: !!!!!!! OFFERS ORDER 
//a.id, ( ROUND( (UNIX_TIMESTAMP(a.date))/604800) ) AS epoch, a.rating
// a.id, epoch, rating, a.date
//TSM order of the matrix
		if (!isset($input_data["recent"])) {
			$order = "epoch DESC, a.rating DESC, a.id DESC";
		} else {
			$order = "a.id DESC";
		}
		$exclude = isset($input_data["exclude"]) ? $input_data["exclude"] : "";
		$my_feed = isset($input_data["my_feed"]) ? (bool) $input_data["my_feed"] : false;
		$my_likes = isset($input_data["my_likes"]) ? (bool) $input_data["my_likes"] : false;
		$catalog = isset($input_data["catalog"]) ? (bool) $input_data["catalog"] : false;
		$page = 0;
		$limit = isset($input_data["limit"]) ? $input_data["limit"] : 0;
		$featured = isset($input_data["featured"]) ? true : false;
		$featured_static = isset($input_data["featured_static"]) ? true : false;
		$own_sponsored = isset($input_data["own_sponsored"]) ? true : false;
//		die(print_r($input_data));
		if ($own_sponsored) {
			if (!$this->session->userdata("logged_in") || $input_data["profile"] != $this->session->userdata("selector")) {
				$own_sponsored = false;
			} else {
				$order = ' a.active DESC, a.date DESC ';
				$this->type_masonry = "own_sponsored";
				$sponsored = false;
			}
		}

		$masonry_reset = false;
		$temp_session = array();
		$likes = isset($input_data["likes"]) ? (bool) $input_data["likes"] : false;
		$button = isset($input_data["button"]) ? (bool) $input_data["button"] : false;
		if ($button && (!isset($this->session->userdata('matrix')[$ui]) || count($this->session->userdata('matrix')[$ui]["data"]) == 0 )) {
			return array("none" => true, "reset" => true);
		}
		$do_search = false;
		if (isset($input_data["search"]) && trim($input_data["search"]) != "") {
			$do_search = true;
			$serach_term = trim($input_data["search"]);
			$serach_term = $this->db->escape_str($serach_term);
			if ($serach_term == "" || mb_strlen($serach_term, "utf8") < 3) {
				return false;
			}
		}

		$show_cat = false;
		if (isset($input_data["cat"]) && trim($input_data["cat"]) != "") {
			$show_cat = true;
			$serach_cat = trim($input_data["cat"]);
			$serach_cat = $this->db->escape_str($serach_cat);
			if ($serach_cat == "" || mb_strlen($serach_cat, "utf8") < 3) {
				return false;
			}
		}

		$profile = isset($input_data["profile"]) ? $input_data["profile"] : "";
		$explore = isset($input_data["explore"]) ? $input_data["explore"] : "";

		if (($exclude == "" || $exclude == 0) && (!$featured && !$featured_static)) {
			//don't clean session if it is view
			$masonry_reset = $this->clean_session_matrix($ui);
		} else {
			$this->no_session = true;
			$ui = uniqid();
			$profile = "";
			$likes = false;
		}
		if ($this->type_masonry == "") {
			if ($catalog) {
				$this->type_masonry = "catalog";
			} else if ($profile != "" && $likes) {
				$this->type_masonry = "likes";
			} else if ($profile != "") {
				$this->type_masonry = "wants";
				$sponsored = false;
			}
		}
		$where = "";
		if ($profile != "" && $likes == 1) {
			$sponsored = false;
		}
		if (!$this->no_session && $this->session->userdata('matrix') != null && isset($this->session->userdata('matrix')[$ui]) && count($this->session->userdata('matrix')[$ui]["data"]) != 0) {
			$posts_array = $this->get_matrix($ui, $sponsored);
		} else {
			$tag = "";
			$cats_array = [];
			if ($do_search) {
//SEARCH
				//make search cat string. first get cats and split by ids and search like '%,12,%'
//				SELECT id, rating, epoch FROM (
//    SELECT id, rating, MATCH (comment) AGAINST ('тениска') AS relevance, (ROUND( (UNIX_TIMESTAMP(date))/604800) ) AS epoch 
//    FROM posts
//    WHERE post_type = 'post' AND del = '0' HAVING relevance > 0.2 ORDER BY epoch, rating DESC
//) AS a 
//    
//UNION SELECT id, rating, ( ROUND( (UNIX_TIMESTAMP(date))/604800) ) AS epoch 
//FROM `posts` AS b 
//WHERE b.tags LIKE '%тениска%' AND b.post_type = 'post' AND b.del = '0'
//    
//UNION SELECT id, rating, ( ROUND( (UNIX_TIMESTAMP(date))/604800) ) AS epoch
//FROM `posts` AS d 
//WHERE ( d.categoriesFK LIKE '%,18,%' ) AND d.post_type = 'post' AND d.del = '0'
//    
//UNION SELECT id, rating, ( ROUND( (UNIX_TIMESTAMP(date))/604800) ) AS epoch
//FROM `posts` AS c 
//WHERE c.cat_strings LIKE '%тениска%' AND c.post_type = 'post' AND c.del = '0'
//   
//ORDER BY epoch DESC, rating DESC LIMIT 0, 800

				$where_cats = "";
				$matrix_params["search"] = $serach_term;
				$query_cat = $this->db->query("SELECT id FROM categories WHERE state='1' AND name LIKE '%" . $serach_term . "%' ");
				if ($query_cat->num_rows() > 0) {

					foreach ($query_cat->result() as $row) {
						$cats_ids[] = $row->id;
					}
					$where_cats = "UNION \nSELECT id, rating, ( ROUND( (UNIX_TIMESTAMP(date))/" . $this->config->item("my_periods_for_posts") . ") ) AS epoch FROM  `posts` AS d WHERE ( d.categoriesFK LIKE '%,";
					$where_cats.=implode(",%' OR d.categoriesFK LIKE '%,", $cats_ids) . ",%' ) AND d." . $where_type . "d.del = '0' ";
//					die($where_cats);
				}
				$query_sql = "SELECT id, rating, epoch  FROM (SELECT id, rating, MATCH (comment) AGAINST ('" . $serach_term . "') AS relevance, ( ROUND( (UNIX_TIMESTAMP(date))/" . $this->config->item("my_periods_for_posts") . ") ) AS epoch FROM `posts`  WHERE " . $where_type . "del = '0' HAVING relevance > 0.2 ORDER BY `epoch` DESC, `rating` DESC) AS a
UNION
SELECT id, rating, ( ROUND( (UNIX_TIMESTAMP(date))/" . $this->config->item("my_periods_for_posts") . ") ) AS epoch FROM `posts` AS b WHERE b.tags LIKE '%" . $serach_term . "%' AND b." . $where_type . "b.del = '0' 
"
						. $where_cats .
						"
UNION
SELECT id, rating, ( ROUND( (UNIX_TIMESTAMP(date))/" . $this->config->item("my_periods_for_posts") . ") ) AS epoch FROM `posts` AS c WHERE c.cat_strings LIKE '%" . $serach_term . "%' AND c." . $where_type . "c.del = '0'";
				//TSM SEARCH order of the matrix
				//, ( ROUND( (UNIX_TIMESTAMP(a.date))/" . $this->config->item("my_periods_for_posts") . ") ) AS epoch,
				$order = "epoch DESC, rating DESC";
				$this->make_search_matrix($ui, $matrix_params, $query_sql, $order);
			} if ($show_cat) {
				//show cat
				$where_cats = "";
				$matrix_params["cat"] = $serach_cat;
				$query_cat = $this->db->query("SELECT id, name FROM categories WHERE state='1' AND name LIKE '" . $serach_cat . "' ");
				if ($query_cat->num_rows() > 0) {
					foreach ($query_cat->result() as $row) {
						$cats_names[] = $row->name;
					}
					$query_sql = "SELECT id, rating, ( ROUND( (UNIX_TIMESTAMP(date))/" . $this->config->item("my_periods_for_posts") . ") ) AS epoch FROM  `posts` WHERE cat_strings LIKE '%,";
					$query_sql.=implode(",%' OR cat_strings LIKE '%,", $cats_names) . ",%' AND  del = '0' ";
				} else {
					return false;
				}
				$order = "epoch DESC, rating DESC";
				$this->make_search_matrix($ui, $matrix_params, $query_sql, $order);
			} else if ($explore != "") {
//TAGS
				$tag = urldecode(trim($explore));
				$tag = mb_convert_case($tag, MB_CASE_LOWER, "UTF-8");
				$matrix_params["tag"] = $tag;
				if (isset($input_data["category"])) {
					$category = trim(trim($input_data["category"]), ",");
				}
				$tag_search = false;
				if (!stristr($tag, ",")) { //$exclude != "" && 
					$tag_search = true;
				}

				if (strlen($tag) >= 3) {
					$tags_array = explode(",", trim($tag, ","));
				}

				if (isset($tags_array) || isset($category)) {
					$cats = $this->get_all_cats();
					//sanitize tags
					if (isset($tags_array)) {
						foreach ($tags_array as $tag_key => $tag_val) {
							$tags_array[$tag_key] = $this->db->escape_str($tags_array[$tag_key]);
							//tags to cats
							foreach ($cats as $cat_group) {
								foreach ($cat_group["data"] as $cat) {
									if (in_array($cat["cat_name"], $tags_array)) {
										if (!in_array($cat["cat_id"], $cats_array)) {
											$cats_array[] = $cat["cat_id"];
										}
									}
								}
							}
						}
					}
					if (isset($category)) {
						$input_cats = explode(",", $category);
						foreach ($cats as $cat_group) {
							foreach ($cat_group["data"] as $cat) {
								if (in_array($cat["cat_id"], $input_cats)) {
									if (!in_array($cat["cat_id"], $cats_array)) {
										$cats_array[] = $cat["cat_id"];
									}
								}
							}
						}
					}
					$order = ' epoch DESC,a.rating DESC';
				}

				if (isset($tags_array)) {
					$where = " a.tags LIKE '%," . $tags_array[0] . ",%'";
					if (count($tags_array) > 1) {
						unset($tags_array[0]);
						sort($tags_array);
						foreach ($tags_array as $tag_val) {
							$where .= " OR a.tags LIKE '%," . $tag_val . ",%'";
						}
					}
				}
				if (count($cats_array) > 0) {
					$where.= " OR categoriesFK LIKE '%," . implode(",%' OR categoriesFK  LIKE '%,", $cats_array) . ",%' ";
				}
				if ($where != "") {
					$where = " ( " . $where . " ) ";
				}
				if ($exclude != "" && $exclude != 0) {
					if ($where != "") {
						$where.=" AND 	";
					}
					$where.=" a.id!=" . $exclude;
				}
				if ($tag_search) {
					$views_go = ($this->session->userdata('tags_views') == null);
					if (!$views_go) {
						$views_go = !isset($this->session->userdata('tags_views')[$tag]);
					}

					if ($views_go) {
						$this->post_model->update_tag_views($tag);
						$this->post_model->update_tag_rating($tag);
						$this->post_model->update_cat_views($tag);
						$this->post_model->update_cat_rating($tag);

						$viewd_ids = array();
						if (is_array($this->session->userdata('tags_views'))) {
							$viewd_ids = $this->session->userdata('tags_views');
						}
						$viewd_ids[$tag] = true;
						$this->session->set_userdata("tags_views", $viewd_ids);
					}
				}
			} else if ($this->type_masonry == "likes" || ($my_likes && $this->session->userdata("logged_in"))) {
				if ($my_likes) {
//LIKES in main and profile
					$profile = $this->session->userdata("selector");
					$sponsored = false;
				}
				$this->make_liked_matrix($profile, $ui);
			} else if ($this->type_masonry == "wants") {
//WANTS profile
				$order = "a.id DESC";
				$sponsored = false;
//				$where.=" b.selector='" . $profile . "'";
				$this->make_wants_matrix($profile, $ui);
			} else if ($this->type_masonry == "catalog") {
				$order = "a.id DESC";
				$sponsored = false;
				$where.=" b.selector='" . $profile . "'";
			} else if ($this->type_masonry == "own_sponsored") {
//OWN SPONSORED profile 
				$where .= " a.users_selectorFK='" . $this->session->userdata("selector") . "' ";
			} else {
				if ($my_feed) {
//MY FEED
					$follow_selectors = array();
					$query = $this->db->query("SELECT follows_users_selectorFK FROM follow WHERE users_selectorFK ='" . $this->session->userdata("selector") . "'");
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $row) {
							$follow_selectors[] = $row->follows_users_selectorFK;
						}
					}
					if (is_array($follow_selectors) && count($follow_selectors) > 0) {
						$where = " ( b.selector = '";
						$where.=implode("' OR b.selector = '", $follow_selectors) . "' ) ";
						unset($follow_selectors);
						$order = ' a.id DESC';
					} else {
						return false;
					}
				} else {
//DASHBOARD
					$where = "";
					if ($featured || $featured_static) {
//FEATURED
						$sponsored = false;
						//MYTODO: set featured criteria here
						$order = ' rand() ';
						$where.=" a.tips!=0 ";
						$page = 0;
						$limit = ($limit == 0) ? $this->config->item("my_featured_items") : $limit;
					} else if ($exclude != "" && $exclude != 0) {
						$where.=" a.id!=" . $exclude;
						$order = ' epoch DESC,a.rating DESC';
					}
				}
			}
			if ($this->type_masonry != "likes" && !$do_search && !$show_cat && !$my_likes && $this->type_masonry != "wants") { //  && $this->type_masonry != "catalog"
				$select_string = 'a.id, ( ROUND( (UNIX_TIMESTAMP(a.date))/' . $this->config->item("my_periods_for_posts") . ') ) AS epoch, a.rating';
				if (!$this->no_session && $this->session->userdata('matrix') != null && isset($this->session->userdata('matrix')[$ui]) && count($this->session->userdata('matrix')[$ui]["data"]) == 0) {
					$this->make_matrix($ui, $matrix_params);
				} else {
					if ($where != "") {
						$where = $where . " AND a." . $where_type . " a.del = '0'";
					} else {
						$where = "a." . $where_type . "a.del = '0'";
					}

					$this->make_matrix($ui, $matrix_params, $select_string, $where, $order, $page, $limit);
				}
			}
			$posts_array = $this->get_matrix($ui, $sponsored);
		}
		if (!$posts_array) {
			return false;
		} else {
			$posts_array["post_info"]["type"] = $this->type_masonry;
			if ($masonry_reset) {
				$posts_array["post_info"]["reset"] = true;
			}
			if ($this->type_masonry != "") {
				$posts_array["post_info"]["new_ui"] = $ui;
			}
			return $posts_array;
		}
	}

	public function get_all_cats($active = true, $description = false, $simple = false, $no_zero = false) {
		$select_string = 'cat_groups.id AS group_id, categories.id AS cat_id, cat_groups.name AS group_name, categories.name AS cat_name, cat_groups.param AS group_param, categories.param AS cat_param';
		if ($description) {
			$select_string.=", cat_groups.description AS group_description, categories.description AS cat_description";
		}
		$this->db->select($select_string);
		$this->db->from('cat_groups');
		$this->db->join('categories', 'categories.cat_groupsFK = cat_groups.id');

		$where_string = "";
		if ($active) {
			$where_string .= " categories.state = '1' AND cat_groups.state = '1' ";
		}
		if ($no_zero) {
			if ($where_string != "") {
				$where_string .= " AND ";
			}
			$where_string .= " categories.posts > 0 ";
		}
		if ($where_string != "") {
			$this->db->where($where_string);
		}
		$this->db->order_by('cat_groupsFK ASC, categories.name ASC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$cats_array = array();
			foreach ($query->result() as $rows) {
				if (!$simple) {
					if (!isset($cats_array[$rows->group_id])) {
						$cats_array[$rows->group_id] = [];
						$cats_array[$rows->group_id]["group_id"] = $rows->group_id;
						$cats_array[$rows->group_id]["group_name"] = $rows->group_name;
						$cats_array[$rows->group_id]["group_param"] = $rows->group_param;
						if ($description) {
							$cats_array[$rows->group_id]["group_description"] = $rows->group_description;
						}
						$cats_array[$rows->group_id]["data"] = array();
					}
					$temp_cat_array = array();
					$temp_cat_array["cat_id"] = $rows->cat_id;
					$temp_cat_array["cat_name"] = $rows->cat_name;
					$temp_cat_array["cat_param"] = $rows->cat_param;
					if ($description) {
						$temp_cat_array["cat_description"] = $rows->cat_desccription;
					}
					$cats_array[$rows->group_id]["data"][] = $temp_cat_array;
				} else {
					$cats_array[$rows->cat_id] = $rows->cat_name;
				}
			}
			return $cats_array;
		}
		return false;
	}

	public function update_tag_views($tag) {
		$tag = mb_convert_case(trim($tag), MB_CASE_LOWER, "UTF-8");
		$this->db->where("`tag`='" . $tag . "' ");
		$this->db->set('views', 'views+1', FALSE);
		$this->db->update('tags');
	}

	public function increase_cat_posts($cat_id, $sign = "+") {
		$this->db->query("UPDATE categories SET posts = posts" . $sign . "1 WHERE id = " . $cat_id);
		$cat_group = $this->get_cat_group_by_cat("", $cat_id);
//		$this->db->query("UPDATE cat_groups SET posts = posts" . $sign . "1 WHERE id = " . $cat_group['id']);
//		$this->update_cat_rating("", $cat_id);
	}

	public function increase_users_posts($cat_id, $user_id = "") {
		if ($user_id == "") {
			$this->db->where(array("id" => $this->session->userdata("id")));
		} else {
			$this->db->where(array("id" => $user_id));
		}
		$this->db->set('posts', 'posts+1', FALSE);
		$this->db->update('users');
		$this->increase_cat_posts($cat_id);
	}

	public function decrease_users_posts($selector) {
		$this->db->where(array("selector" => $selector));
		$this->db->set('posts', 'posts-1', FALSE);
		$this->db->update('users');
	}

	public function decrease_tags_posts($tag) {
		$this->db->where(array("tag" => $tag));
		$this->db->set('posts', 'posts-1', FALSE);
		$this->db->update('tags');
		$this->update_tag_rating($tag);
	}

	public function get_cat($cat_name = "", $cat_id = "") {
		$where = "";
		if ($cat_name != "") {
			$where = " `name`= '" . mb_convert_case(trim($cat_name), MB_CASE_LOWER, "UTF-8") . "' ";
		} else if ($cat_id != "") {
			$where = "`id`=" . $cat_id;
		}
		$this->db->from('categories');
		$this->db->where($where);
		$query_result = $this->db->get();
		if ($query_result->num_rows() > 0) {
			foreach ($query_result->result() as $row) {
				$row->big_name = mb_convert_case($row->name, MB_CASE_UPPER, "UTF-8");
				$result = (array) $row;
				return $result;
			}
		} else {
			return false;
		}
	}

	public function get_cat_names() {
		$this->db->select("id, name");
		$this->db->from('categories');
		$query_result = $this->db->get();
		if ($query_result->num_rows() > 0) {
			foreach ($query_result->result() as $row) {
				$row->big_name = mb_convert_case($row->name, MB_CASE_UPPER, "UTF-8");
				$result[$row->name] = $row->id;
			}
			return $result;
		}
	}

	public function get_cat_group($group_name = "", $cat_id = "") {
		$where = array();
		if ($group_name != "") {
			$where["name"] = mb_convert_case(trim($group_name), MB_CASE_LOWER, "UTF-8");
		} else if ($cat_id != "") {
			$where["id"] = $cat_id;
		}
		$this->db->where($where);
		$this->db->from('cat_groups');
		$query_result = $this->db->get();
		if ($query_result->num_rows() > 0) {
			foreach ($query_result->result() as $row) {
				$result = (array) $row;
				return $result;
			}
		} else {
			return false;
		}
	}

	public function get_cat_group_by_cat($cat_name = "", $cat_id = "") {
		$cat = $this->get_cat($cat_name, $cat_id);
		if (is_array($cat)) {
			$group = $this->get_cat_group("", $cat["cat_groupsFK"]);
			if (is_array($group)) {
				return $group;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function update_cat_views($category) {
		$category = mb_convert_case(trim($category), MB_CASE_LOWER, "UTF-8");
		$cat = $this->get_cat($category);
		$update_groups = array();
		if (is_array($cat)) {
			if (!in_array($cat['cat_groupsFK'], $update_groups)) {
//				increase vies for group only once
				$this->db->where(array("id" => $cat['cat_groupsFK']));
				$this->db->set('views', 'views+1', FALSE);
				$this->db->update('cat_groups');
				$update_groups[] = $cat['cat_groupsFK'];
			}
			$this->db->where(array("id" => $cat["id"]));
			$this->db->set('views', 'views+1', FALSE);
			$this->db->update('categories');
		}
	}

	public function update_post_user_views($post_id, $user_id) {
		$this->db->where(array("id" => $post_id));
		$this->db->set('views', 'views+1', FALSE);
		$this->db->update('posts');

		$this->db->where(array("id" => $user_id));
		$this->db->set('post_views', 'views+1', FALSE);
		$this->db->update('users');

		//update rating
		//$this->users_model->update_user_rating($user_id);
	}

//PENDING: !!!!! POST RATING
	private function update_post_rating($post_id) {
		$query = "UPDATE `posts` SET `rating` = ( (`tips`*4)+`views`+(`likes`*2) ) WHERE `id` = " . $post_id;
		$query_result = $this->db->query($query);
	}

	public function update_post_cats($post_id, $cat, $cat_name) {
		$query = "UPDATE `posts` SET categoriesFK = CONCAT(categoriesFK, '" . $cat . ",'), cat_strings = CONCAT(cat_strings, '" . $cat_name . ",') WHERE `id`=" . $post_id;
		$this->db->query($query);
		$this->increase_cat_posts($cat);
	}

	public function update_cat_rating($category = "", $cat_id = "") {
		$category = mb_convert_case(trim($category), MB_CASE_LOWER, "UTF-8");

		if ($category != "") {
			$where = "`name` = '" . $category . "' ";
		} else if ($cat_id != "") {
			$where = " `id` = " . $cat_id;
		}
		$query = "SELECT IFNULL((`views`/`posts`),0) as `calc_rating`,`id` FROM `categories` WHERE " . $where;

		$query_result = $this->db->query($query);
		if ($query_result->num_rows() > 0) {
			foreach ($query_result->result() as $row) {

				$this->db->where(array("id" => $row->id));
				$this->db->set('rating', $row->calc_rating, FALSE);
				$this->db->update('categories');

				$cat_group = $this->get_cat_group_by_cat("", $row->id);
				if (is_array($cat_group)) {
					if ($cat_group['posts'] != 0) {
						$this->db->where(array("id" => $cat_group['id']));
						$this->db->set('rating', $cat_group['views'] / $cat_group['posts'], FALSE);
						$this->db->update('cat_groups');
					}
				}
			}
		}
	}

	public function update_tag_rating($tag) {
		$tag = mb_convert_case(trim($tag), MB_CASE_LOWER, "UTF-8");
		//$query = "SELECT IFNULL((`views`/`posts`),0) as `calc_rating`,`id` FROM `tags` WHERE `tag` = '" . $tag . "'";

		$query = "UPDATE `tags` SET `rating` = IFNULL((`views`/`posts`),0) WHERE `tag` = '" . $tag . "'";

		$query_result = $this->db->query($query);
//		if ($query_result->num_rows() > 0) {
//			foreach ($query_result->result() as $row) {
//				$this->db->where(array("id" => $row->id));
//				$this->db->set('rating', $row->calc_rating, FALSE);
//				$this->db->update('tags');
//			}
//		}
	}

	public function suggestTags($term, $limit = 10) {
		$term = trim($term);
		if ($term != "") {
			$this->db->select("tag");
			$this->db->order_by('rating DESC');

			$result_prim = [];
			//prio results that begins with term
			$query = $this->db->get_where("tags", "`tag` like ('" . $term . "%')", 0, $limit);
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $rows) {
					$result_prim[] = $rows->tag;
				}
			}

			$result_sec = [];
			$query = $this->db->get_where("tags", "`tag` like ('%" . $term . "%')", 0, $limit);
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $rows) {
					$result_sec[] = $rows->tag;
				}
			}

			$result = array_merge($result_prim, $result_sec);
			$result = array_unique($result);
			$result = array_slice($result, 0, $limit);
			return $result;
		} else {
			return false;
		}
	}

	public function liked_posts($user_id, $posts_ids) {
		$select_string = 'postsFK';
		$this->db->select($select_string);
		$this->db->from('likes');
		$where_string = "usersFK = " . $user_id . " AND (";
		if (count($posts_ids) > 0) {
			$where_string .= "postsFK=" . implode(" OR postsFK=", $posts_ids);
		}
		$where_string .=")";
		$this->db->where($where_string);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = array();
			foreach ($query->result() as $row) {
				$result[$row->postsFK] = true;
			}
			return $result;
		}
		return false;
	}

	public function add_like($post_id) {
		if ($this->session->userdata('logged_in')) {
			if (!$this->is_liked($post_id)) {
				$this->load->model('events_model');
				$result = $this->get_likes($post_id);
				$total_likes = $result["likes"];
				$sql_data = array("usersFK" => $this->session->userdata('id'), "post_type" => "post", "postsFK" => $post_id, "users_selectorFK" => $this->session->userdata('selector'));
				$this->db->insert('likes', $sql_data);

				//increase likes in posts
				$this->db->where('id', $post_id);
				$this->db->set('likes', 'likes+1', FALSE);
				$this->db->update('posts');

				//increase likes for author
				$this->db->where('id', $result["user_id"]);
				$this->db->set('post_likes', 'post_likes+1', FALSE);
				$this->db->update('users');
				$total_likes+=1;
				$event_array = array();

				$this->update_post_rating($post_id);

				//ADD LIKE EVENT
				if ($result["user_selector"] != $this->session->userdata('selector')) {
					$event_array["to_users_selectorFK"] = $result["user_selector"];
					$event_array["from_users_selectorFK"] = $this->session->userdata('selector');
					$event_array["event_type"] = "like";
					$event_array["item1"] = $post_id;
					$event_array["user_type"] = "user";
					$this->events_model->add_event($event_array);
				}
				//$this->users_model->update_user_rating($result["user_id"]);
				return $total_likes;
			}
		}
	}

	public function remove_like($post_id) {
		if ($this->session->userdata('logged_in')) {
			if ($this->is_liked($post_id)) {

				$result = $this->get_likes($post_id);
				$total_likes = $result["likes"];

				$this->db->where('postsFK=' . $post_id . " AND usersFK=" . $this->session->userdata('id') . " AND post_type='post'");
				$this->db->delete('likes');

				//decrease likes in posts
				$this->db->where('id', $post_id);
				$this->db->set('likes', 'likes-1', FALSE);
				$this->db->update('posts');

				$this->update_post_rating($post_id);

				//decrease likes for author
				$this->db->where('id', $result["user_id"]);
				$this->db->set('post_likes', 'post_likes-1', FALSE);
				$this->db->update('users');

				$total_likes-=1;
				//$this->users_model->update_user_rating($result["user_id"]);
				return $total_likes;
			}
		}
	}

	private function is_liked($post_id) {
		if ($this->session->userdata('logged_in')) {
			$select_string = 'id';
			$this->db->select($select_string);
			$this->db->from('likes');
			$where_string = "postsFK = " . $post_id . " AND usersFK=" . $this->session->userdata('id') . " AND post_type = 'post'";
			$this->db->where($where_string);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function toggle_like($post_id) {
		$result = array();
		$result["id"] = $post_id;
		if ($this->is_liked($post_id)) {
			$result["type"] = "remove";
			$result["value"] = $this->remove_like($post_id);
		} else {
			$result["type"] = "add";
			$result["value"] = $this->add_like($post_id);
		}
		return $result;
	}

	public function get_likes($post_id) {
		$select_string = 'likes, usersFK, users_selectorFK';
		$this->db->select($select_string);
		$this->db->from('posts');
		$where_string = "id = " . $post_id . " AND del = '0'";
		$this->db->where($where_string);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result["likes"] = $row->likes;
				$result["user_id"] = $row->usersFK;
				$result["user_selector"] = $row->users_selectorFK;
			}
		} else {
			$result["likes"] = 0;
		}
		return $result;
	}

	public function count_post_tips($post_id) {
		$this->db->select("COUNT(tips) AS total");
		$this->db->from("tips");
		$this->db->where(array("id" => $post_id));
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$row->total;
			}
		} else {
			return 0;
		}
	}

	public function del_post($id) {
		$result = array();
		if ($id != "") {
			if (!$this->session->userdata("logged_in")) {
				$result["login"] = true;
			} else {
				$post = $this->get_post($id);
				if ($post["can_delete"]) {
					$this->load->model('tips_model');
					foreach ($post["tips"] as $p_t) {
						if (isset($p_t['tips'])) {
							foreach ($p_t['tips'] as $tip) {
								$this->tips_model->del_tip($tip["id"]);
							}
						}
					}
					foreach ($post["post_categories"] as $post_cat) {
						$this->increase_cat_posts($post_cat["id"], "-");
					}
					$this->decrease_users_posts($post["user_selector"]);
					foreach ($post["tags"] as $tag) {
						$this->decrease_tags_posts($tag);
					}
					$this->db->query("UPDATE posts SET del = '1' WHERE id=" . $post["id"]);
					$this->db->query("UPDATE wants SET del = '1' WHERE postsFK=" . $post["id"]);
					$this->db->query("UPDATE events SET del = '1' WHERE item1=" . $post["id"]);
					$result["done"] = true;
				}
			}
		}
		return $result;
	}

}

?>
