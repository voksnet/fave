<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Scrape_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function add_scrape($data) {
		$data = json_decode($data, true);
		//DATA
		//url !
		//sex ! - f,m,u
		//domain
		//brand
		//url !\
		//image_url !
		//usersFK || users_selectorFK
		//post_currency !
		//post_price !
		//category !
		//tags !
		//comment
		
		foreach ($data as $provider) {
			if (
					isset($provider["url"]) && (isset($provider["users_selectorFK"]) || isset($provider["usersFK"])) &&
					isset($provider["image_url"]) && isset($provider["category"]) &&
					isset($provider["tags"]) && isset($provider["sex"])
			) {
				$result = array();
				if (isset($provider["domain"])) {
					$domain = parse_url($provider["url"]);
					$result["domain"] = $domain["host"];
				} else {
					$result["domain"] = "";
				}

				if (isset($provider["brand"])) {
					$result["brand"] = $provider["brand"];
				} else {
					$result["domain"] = "";
				}
				$result["url"] = $provider["url"];
				$result["sex"] = $provider["sex"];
				$result["image_url"] = $provider["image_url"];
				if (isset($provider["tip_image_url"])) {
					$result["tip_image_url"] = $provider["tip_image_url"];
				} else {
					$result["tip_image_url"] = "";
				}
				if (isset($provider["usersFK"])) {
					$result["usersFK"] = $provider["usersFK"];
				} else {
					$result["usersFK"] = "";
				}
				if (isset($provider["users_selectorFK"])) {
					$result["users_selectorFK"] = $provider["users_selectorFK"];
				} else {
					$result["users_selectorFK"] = "";
				}
				$result["post_currency"] = $provider["post_currency"];
				$result["post_price"] = $provider["post_price"];
				$result["tags"] = trim($provider["tags"], ",");
				$result["category"] = $provider["category"];
				if (trim($result["category"])=="") {
					die(print_r($result));
				}
				if (isset($provider["comment"])) {
					$result["comment"] = $provider["comment"];
				} else {
					$result["comment"] = "";
				}
				$insert_query = $this->db->insert_string("scraper", $result);
				$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
				$this->db->query($insert_query);
			}
		}
	}

	public function add_posts($limit = 20, $brand= "") {
		$this->load->model("upload_model");
		$this->load->model("post_model");
		$this->load->model("tips_model");
		$this->db->from("scraper");
		$where_brand = "";
		if ($brand != "") {
			$where_brand = " AND brand = '".$brand."'";
		}
		$this->db->where('posted = "0" AND error="0"'.$where_brand);
		$this->db->limit($limit);
		$this->db->order_by("RAND()");//RAND()
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$post_data[] = (array) $row;
			}
			$all_cats = $this->post_model->get_cat_names();
//			die(print_r($post_data));
			foreach ($post_data as $data) {
                echo "scrape id: ".$data["id"]."\n";
				if ($data["users_selectorFK"] != "" || $data["usersFK"] != "") {
					$up_result = $this->upload_model->upload_web_image($data["image_url"]);
					if (isset($up_result["new_filename"])) {
						$files = $up_result["files"];
						$new_filename = $up_result["new_filename"];
						if (!isset($up_result["unknown_error"]) && (!isset($files["files"][0]['error']) || is_null($files["files"][0]['error']))) {
							$insert_data = [];
							$insert_data["sex"] = $data['sex'];
							$insert_data["comment"] = trim($data["comment"]);
							$insert_data["tags"] = trim($data["tags"]);

							$insert_data["categoriesFK"] = $all_cats[mb_convert_case(trim($data["category"]), MB_CASE_LOWER, "UTF-8")];
							
							if ($data["usersFK"] != "" && $data["usersFK"] != 0 ) {
								$insert_data["usersFK"] = $data["usersFK"];
							} else {
								$insert_data["usersFK"] = $this->users_model->get_id_by_selector($data["users_selectorFK"]);
							}
//							die($this->users_model->get_id_by_selector($data["users_selectorFK"])." err");
							if ($data["users_selectorFK"] != "") {
								$insert_data["users_selectorFK"] = $data["users_selectorFK"];
							} else {
								$insert_data["users_selectorFK"] = $this->users_model->get_selector_by_id($insert_data["usersFK"]);
							}
							
							$insert_data["image"] = $new_filename . ".jpg";
							if ($insert_data["users_selectorFK"] != "" && $insert_data["usersFK"] != "") {
								$post_date = strtotime("-1 day");
								$rand_time = rand(0, (60*60*23));
								$insert_data["date"] = date("Y-m-d H:i:s", $post_date+$rand_time);
								$post_id = $this->post_model->addPost($insert_data);
								$this->db->query("UPDATE scraper SET date_posted = NOW(), posted = '1' WHERE id=".$data["id"]);

								if ($data["post_currency"] != "" && $data["post_price"] != "") {
									$tips_data = array();
									$tips_data["post_id"] = $post_id;
									$tips_data["cat_id"] = $insert_data["categoriesFK"];
									if ($data["tip_image_url"] != "") {
										$tips_data["tip_image_url"] = $data["tip_image_url"];
									} else {
										$tips_data["tip_image_url"] = $data["image_url"];
									}
									$tips_data["tip_url"] = $data["url"];
									$tips_data["currency"] = $data["post_currency"];
									$tips_data["tip_price"] = $data["post_price"];
									if ($data["brand"] != "") {
										$tips_data["tip_brand"] = $data["brand"];
									} else {
										$tips_data["tip_brand"] = $data["domain"];
									}
									$tips_data["usersFK"] = $insert_data["usersFK"];
									$tips_data["users_selectorFK"] = $insert_data["users_selectorFK"];
									$tips_data["date"] = date("Y-m-d H:i:s", $post_date+$rand_time+120);
									//		[post_id] => 211
									//		[cat_id] => 46
									//		[tip_url] => http://www.ivet.bg/productv/962050/butikov-kolan-beth-red.html
									//		[tip_image_url] => http://www.ivet.bg/userfiles/productimages/product_123730.jpg
									//		[currency] => BGN
									//		[tip_price] => 123
									//		[tip_brand] => Ivet.bg

									$this->tips_model->add_tip($tips_data);
								}
								echo "new post by scrape \n" . $data["image_url"] . " \n\n";
							}
						}
					} else {
						$this->db->query("UPDATE scraper SET error = '1' WHERE id=".$data["id"]);
						echo "error: \n" . $data["id"] . "\n" . $data["image_url"] . " \n\n";
					}
				}
			}
		}
		die("end \n");
	}
}

?>
