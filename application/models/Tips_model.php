<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Tips_model extends CI_Model {

	public $category = array();

	public function __construct() {
		parent::__construct();
	}

	public function add_tip($data) {
//		die(print_r($data));
//		[post_id] => 211
//		[cat_id] => 46
//		[tip_url] => http://www.ivet.bg/productv/962050/butikov-kolan-beth-red.html
//		[old_tip_url] => http://www.ivet.bg/productv/962050/butikov-kolan-beth-red.html
//		[tip_image_url] => http://www.ivet.bg/userfiles/productimages/product_123730.jpg
//		[currency] => BGN
//		[tip_price] => 123
//		[tip_brand] => Ivet.bg
//		[usersFK]
//		[users_selectorFK]


		$this->load->helper('remote-file');

		$remote_url = "";
		if ($data["tip_image_url"] != "") {
			$remote_url = $data["tip_image_url"];
			$remote_url = str_ireplace(" ", "%20", $remote_url);
			$remote_url = addhttp($remote_url);
		}

		if ($remote_url != "" && !isset($result["error"])) {
			$new_filename = $this->upload_tip_image($remote_url);
		}

		if (!isset($new_filename) || !$new_filename) {
			$data["image"] = "";
		} else {
			$data["image"] = $new_filename . ".jpg";
		}

		$url_params = parse_url($data["tip_url"]);
		//get domain priority
		$domain_priority = $this->tips_domain_priority($url_params["host"]);
		$ip = "scraper";
		if ($this->input->server('REMOTE_ADDR') != null && $this->input->server('REMOTE_ADDR') != "") {
			$ip = $this->input->server('REMOTE_ADDR');
		}
		$sql_data = array("users_selectorFK" => $data["users_selectorFK"], "usersFK" => $data["usersFK"], "categoriesFK" => $data["cat_id"], "postsFK" => $data["post_id"], "link" => $data["tip_url"], "provider_domain" => $url_params["host"], "priority" => $domain_priority, "brand" => $data["tip_brand"], "price" => $data["tip_price"], "currency" => $data["currency"], "image" => $data["image"], "ip" => $ip);
		if (isset($data["date"])) {
			$sql_data["date"] = $data["date"];
		}
		if ($this->db->insert('tips', $sql_data)) {
			$new_id = $this->db->insert_id();

			$this->db->where("id =", $data["post_id"]);
			$this->db->set('tips', 'tips+1', FALSE);
			$this->db->update('posts');
			$isCLI = ( php_sapi_name() == 'cli' );
			if ($this->session->userdata('logged_in')) {
				$this->db->where("id=" . $this->session->userdata('id'));
				$this->db->set('tips', 'tips+1', FALSE);
				$this->db->update('users');
			} else if ($isCLI) {
				$this->db->where("id=" . $data["usersFK"]);
				$this->db->set('tips', 'tips+1', FALSE);
				$this->db->update('users');
			}

			$new_tip = $this->get_tips("", $new_id, true);
			if ($this->session->userdata('logged_in')) {
				$this->users_model->update_user_rating($this->session->userdata('id'));
			} else if ($isCLI) {
				$this->users_model->update_user_rating($data["usersFK"]);
			}

			$this->load->model('events_model');
//				//ADD TIP EVENT
			$event_array = array();
			//$event_array["to_users_selectorFK"] = $user_selector;
			$event_array["from_users_selectorFK"] = $data['users_selectorFK'];
			$event_array["event_type"] = "tip";
			$event_array["item1"] = $data["post_id"];
			$event_array["item2"] = $data["cat_id"];
			$event_array["item3"] = $new_id;
			$event_array["user_type"] = "user";
			$this->events_model->add_event($event_array);


			//tips tips rating domains
			$insert_string = "INSERT INTO `tips_domains` (`domain`, `tips`,`rating`) VALUES ('" . $url_params["host"] . "', 1,1) ";
			$insert_string .= " ON DUPLICATE KEY UPDATE tips=tips+1, rating=rating+1;";
			$this->db->query($insert_string);
			return $new_tip;
		} else {
			return false;
		}
	}

	public function session_browser($url) {
		if (!is_dir(FCPATH . $this->config->item("my_cookie_folder"))) {
			mkdir(FCPATH . $this->config->item("my_cookie_folder"), 0777, true);
		};
		$cookie_file_path = FCPATH . $this->config->item("my_cookie_folder") . md5($url) . "cookie.txt";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_POST, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_AUTOREFERER, 0);
		//curl_setopt($curl, CURLOPT_REFERER, $reffer);
		//die($_SERVER['HTTP_USER_AGENT']);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5');
		curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_COOKIESESSION, TRUE);
		curl_setopt($curl, CURLOPT_COOKIE, session_name() . '=' . session_id());
		curl_setopt($curl, CURLOPT_URL, $url);

		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($curl, CURLOPT_TIMEOUT, 20);
		curl_setopt($curl, CURLOPT_MAXREDIRS, 10);


		//curl_setopt($curl, CURLOPT_POSTFIELDS, "username=admin&password=323123&pageaction=login&");
		$data = curl_exec($curl);
		//$signupStr=getSignupString($data);
		//curl_setopt($curl, CURLOPT_URL,"http://localhost/taiba/tfcsadmcp/index.php?component=cman&page=wce.gall.php");//set this URL to wherever the form submits to
		curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookie_file_path);
		//curl_setopt($curl, CURLOPT_POST,1);//yes we want to post
		//curl_setopt($curl, CURLOPT_POSTFIELDS,$signupStr);//tell it where to find our sign up string
		$data = curl_exec($curl);
		curl_close($curl); //close the session
		if (file_exists($cookie_file_path)) {
			unlink($cookie_file_path);
		}
		return $data;
	}

	public function getMicroData($url) {
		$this->load->helper("remote-file");
		$original_url = $url;
		$url = urldecode($url);
		$url = trim($url);

		//check if this domain is forbidden
		$url_status["result"] = "";
		if (!stristr($url, "facebook.com")) {
			$url_status = $this->forbidden_domain($url);
		} else {
			$url_status["url"] = $url;
			$url_status["skip_data"] = true;
		}
		//check if url exists and if it is valid and forbidden
		if ($url_status["result"] === "invalid") {
			$final_result["error"]["url"] = $this->lang->line("fe_tip_error_url");
		} else if ($url_status["result"]) {
			$final_result["error"]["url"] = $this->lang->line("fe_tip_error_forbiden_url");
		}
		if ($url_status["url"] != $url) {
			$url = $url_status["url"];
			$final_result["url"] = $url;
		}

		if (!isset($url_status["skip_data"]) && !isset($final_result["error"]["url"])) {
			error_reporting(0);
			$remote_content = file_get_contents($url);
			if (trim($remote_content) == "") {
				$remote_content = curlGetPage($url);
			}
			$env = ENVIRONMENT;
			enviroment($env);

			if (trim($remote_content == "") || stristr($remote_content, "<h1>An Error Was Encountered</h1>")) {
				$url = $original_url;

				error_reporting(0);
				$remote_content = file_get_contents($url);
				if (trim($remote_content) == "") {
					$remote_content = curlGetPage($url);
				}
				$env = ENVIRONMENT;
				enviroment($env);
			}


			$remote_content_clean = str_ireplace(array("\n", "\r", "	"), "", $remote_content);
			while (stristr($remote_content_clean, "  ")) {
				$remote_content_clean = str_ireplace("  ", " ", $remote_content_clean);
			}

			$remote_content_clean = str_ireplace("> <", "><", $remote_content_clean);
			$array_results = array();

			$reg_ex_og = '/<meta property=[\'"]og:.*?>/i';
			preg_match_all($reg_ex_og, $remote_content_clean, $array_results[0]);
			$array_results[0] = $array_results[0][0];

			$reg_ex_schema = '/<[^<]* itemprop=[^>]*>/i';
			preg_match_all($reg_ex_schema, $remote_content_clean, $array_results[1]);
			$item_prop_array = array_unique($array_results[1][0]);
			$array_results[1] = $item_prop_array;

			//get content between tags
			$forbidden_tags = array(
				"meta",
				"img",
				"input",
				"area",
				"base",
				"br",
				"col",
				"command",
				"embed",
				"hr",
				"input",
				"link",
				"meta",
				"param",
				"source"
			);
			$reg_ex_tag = '/<([\w\d]*) /i';
//			die(print_r($array_results[1] ));
			foreach ($array_results[1] as $tag_index => $tag) {
				$tag_names = array();
				preg_match($reg_ex_tag, $tag, $tag_names);
//				if (!stristr($tag, " content=") && !in_array($tag_names[1], $forbidden_tags)) {
				$escaped_tag = str_ireplace("/", "\/", $tag);
				$escaped_tag = str_ireplace("\\\\/", "\/", $escaped_tag);   // TB: hack to handle escaped urls in tag: http:\/\/
				$reg_ex_tag_content = '/' . $escaped_tag . '(.*?)<\/' . $tag_names[1] . '/i';
				preg_match_all($reg_ex_tag_content, $remote_content_clean, $tag_content);
				unset($tag_content[0]);
				sort($tag_content);
				$tag_content = $tag_content[0];
				if (count($tag_content) > 0) {
					foreach ($tag_content as $t_k => $t_v) {
						$tag_content[$t_k] = strip_tags($t_v);
					}
					$array_results[1][$tag_index] = array("tag" => $tag);
					$array_results[1][$tag_index]["results"] = $tag_content[0];
				}
//				}
			}

			$reg_ex_img_slash = '/<img [^<]*\/\/(.*?)[\'"]/i';
			preg_match_all($reg_ex_img_slash, $remote_content_clean, $array_results[2]);
			unset($array_results[2][0]);
			sort($array_results[2]);
			$array_results[2] = $array_results[2][0];
			foreach ($array_results[2] as $a_k => $a_v) {
				if (stristr($a_v, "'")) {
					$array_results[2][$a_k] = trim($a_v, "'");
				}
			}

			$reg_ex_img_src = '/<img [^<]*src=[\'"](.*?)[\'"]/i';
			preg_match_all($reg_ex_img_src, $remote_content_clean, $array_results[3]);
			unset($array_results[3][0]);
			sort($array_results[3]);
			$array_results[3] = $array_results[3][0];
			foreach ($array_results[3] as $a_k => $a_v) {
				if (stristr($a_v, "'")) {
					$array_results[3][$a_k] = trim($a_v, "'");
				}
			}

			$reg_ex_img_backgrund = '/<[^<]*background-image:.*?url\((.*?)\)/i';
			preg_match_all($reg_ex_img_backgrund, $remote_content_clean, $array_results[4]);
			unset($array_results[4][0]);
			sort($array_results[4]);
			$array_results[4] = $array_results[4][0];
			foreach ($array_results[4] as $a_k => $a_v) {
				if (stristr($a_v, "'")) {
					$array_results[4][$a_k] = trim($a_v, "'");
				}
			}

			$reg_ex_img_og =  '/<meta[\s]+property="og:image"[\s]+content="(.*?)"[^>]+>/i';
			preg_match_all($reg_ex_img_og, $remote_content_clean, $array_results[5]);
			unset($array_results[5][0]);
			sort($array_results[5]);
			$array_results[5] = $array_results[5][0];
			foreach ($array_results[5] as $a_k => $a_v) {
				if (stristr($a_v, "'")) {
					$array_results[5][$a_k] = trim($a_v, "'");
				}
			}

//			die(print_r($array_results));
			//clean each image block
			$url_params = parse_url($url);
			$final_result["domain"] = $url_params["host"];


			//merge all in one images
			$image_array = array_merge($array_results[5], $array_results[2], $array_results[3], $array_results[4]);
			unset($array_results[5], $array_results[2], $array_results[3], $array_results[4]);
			$image_array = array_unique($image_array);
//			die(print_r($url_params));
			//get base
			$base_tag = "";
			if (stristr($remote_content_clean, "<base ")) {
				$reg_ex_base_tag = '/<base [^<]*\/\/(.*?)[\'"]/i';

				preg_match_all($reg_ex_base_tag, $remote_content_clean, $base_tag);
//				die(print_r($base_tag));
				if (isset($base_tag[1][0])) {
					$base_tag = $base_tag[1][0];
					$strpos_http = strpos($base_tag, "http");
					if ($strpos_http !== 0) {
						if (stristr($base_tag, "//") && strpos($base_tag, "//") == 0) {
							$base_tag = "http:" . $base_tag;
						} else if (stristr($base_tag, "/") && strpos($base_tag, "/") == 0) {
							$base_tag = $url_params["scheme"] . "://" . $url_params["host"] . $base_tag;
						} else if (stristr($base_tag, "www") && strpos($base_tag, "www") == 0) {
							$base_tag = "http://" . $base_tag;
						}
					} else {
						$base_tag = "http://" . $base_tag;
					}
				} else {
					$base_tag = "";
				}
			}
			$url_params["base"] = $base_tag;

			$image_array = $this->prepare_images($image_array, $url_params);

//		$this->benchmark->mark('code_end');
//		echo $this->benchmark->elapsed_time('code_start', 'code_end');

			if (count($image_array) > 0) {
				$final_result["images"] = $image_array;
			} else {
				$final_result["images"] = array();
			}

			if (count($array_results[0]) > 0) {
				foreach ($array_results[0] as $og_v) {
					if (stristr($og_v, "og:site_name")) {
						$og_v = str_ireplace("'", '"', $og_v);
						$final_result["site_name"] = $this->xmlstr($og_v, 'content="', '"');
					}
					if (stristr($og_v, "og:price:currency")) {
						$og_v = str_ireplace("'", '"', $og_v);
						$final_result["currency"] = $this->xmlstr($og_v, 'content="', '"');
					}
					if (stristr($og_v, "og:price:amount")) {
						$og_v = str_ireplace("'", '"', $og_v);
						$final_result["price"] = $this->xmlstr($og_v, 'content="', '"');
					}
					if (stristr($og_v, "og:image")) {
						$og_v = str_ireplace("'", '"', $og_v);
						$final_result["image"] = $this->xmlstr($og_v, 'content="', '"');
					}
				}
			}

			if (count($array_results[1]) > 0) {
				foreach ($array_results[1] as $sog_v) {

					if (!is_array($sog_v)) {
						$sog_v = str_ireplace("'", '"', $sog_v);

						if (stristr("priceCurrency", $sog_v)) {
							$temp_currency = $this->currencyCheck($this->xmlstr($sog_v, 'content="', '"'));
							if ($temp_currency != false) {
								$final_result["currency"] = $this->currencyCheck($this->xmlstr($sog_v, 'content="', '"'));
							}
						}
					} else {

						if (stristr('itemprop="price"', $sog_v["tag"])) {
							$final_result["price"] = (real) trim($sog_v["results"]);
						}
					}
				}
			}
			if (isset($final_result["domain"]) && (!isset($final_result["site_name"]) || $final_result["site_name"] == "")) {
				$final_result["site_name"] = $final_result["domain"];
			}
			if (isset($final_result["image"]) && $this->is_image($final_result["image"]) && !in_array($final_result["image"], $final_result["images"])) {
				array_unshift($final_result["images"], $final_result["image"]);
			} else {
				unset($final_result["image"]);
			}
			$final_result["site_name"] = htmlspecialchars_decode($final_result["site_name"]);
		} else if (!isset($final_result["error"]["url"])) {
			$final_result["url"] = $url;
			$url_params = parse_url($url);
			$final_result["domain"] = $url_params["host"];
			$final_result["site_name"] = $url_params["host"];
			$final_result["skip_data"] = true;
//			return $final_result;
		}
//		die(print_r($final_result));
		return $final_result;
	}

	private function xmlstr($string, $start, $end) {
		$string = " " . $string;
		$ini = strpos($string, $start);
		if ($ini == 0)
			return"";
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	private function currencyCheck($currency) {
//		/fe_currency
		if (isset($this->lang->line(fe_currency)[trim($currency)])) {
			return trim($currency);
		} else {
			return false;
		}
	}

	private function prepare_images($images, $url_params) {
		$images_array = array();
		foreach ($images as $i_v) {
			$i_v = trim($i_v);
			if ($i_v != "") {
				if (stristr($i_v, " ")) {
					$i_v = explode(" ", $i_v);
					$i_v = $i_v[0];
				}
				if ($this->is_image($i_v) && !stristr($i_v, "logo") && !stristr($i_v, "../")) {
					if ($url_params["base"] != "") {
						$second_img = $url_params["base"] . $i_v;
					}
					$strpos_http = strpos($i_v, "http");
					if ($strpos_http !== 0) {
						if (stristr($i_v, "//") && strpos($i_v, "//") == 0) {
							$i_v = "http:" . $i_v;
						} else if (stristr($i_v, "/") && strpos($i_v, "/") == 0) {
							$i_v = $url_params["scheme"] . "://" . $url_params["host"] . $i_v;
						} else if (stristr($i_v, "www") && strpos($i_v, "www") == 0) {
							$i_v = "http://" . $i_v;
						} else {
							$temp_url = explode(".", $i_v);
							if (!stristr($temp_url[0], "/")) {
								$i_v = "http://" . $i_v;
							} else {
								$i_v = $url_params["scheme"] . "://" . $url_params["host"] . "/" . $i_v;
							}
						}
					}
					if (!in_array($i_v, $images_array)) {
						$images_array[] = $i_v;
					}
					if (isset($second_img) && !in_array($second_img, $images_array)) {
						$images_array[] = $second_img;
						unset($second_img);
					}
				}
			}
		}
		$images_array = array_slice(array_unique($images_array), 0, 100);
		return $images_array;
	}

	private function is_image($url) {
		foreach (array(".jpg", ".jpeg", ".png") as $ext) {
			if (stristr($url, $ext)) {
				return true;
			}
		}
		return false;
	}

	public function post_exists($id) {
		$this->db->select("id");
		$this->db->where("id = " . $id . " AND del='0'");
		$this->db->from("posts");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function cat_exists($id) {
		$this->db->select("id");
		$this->db->where("id = " . $id);
		$this->db->from("categories");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function check_tip_limit($post_id, $cat_id) {
		$this->db->select("COUNT(id) AS count");
		$this->db->where(array("postsFK" => $post_id, "categoriesFK" => $cat_id, "del" => "0"));
		$this->db->from("tips");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				return $row->count;
			}
		} else {
			return 0;
		}
	}

	public function tip_duplicate($url, $post_id, $cat_id) {
		$query = $this->db->query("SELECT `id` FROM tips WHERE postsFK = " . $post_id . " AND categoriesFK=" . $cat_id . " AND link='" . $url . "' AND del='0'");
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function get_tips($post_id = "", $tip_id = "", $ajax = false) {
		$result = array();
		$this->db->select("a.*, b.username, b.alias, b.selector, b.avatar, c.name, c.param");
		if ($post_id != "") {
			$this->db->where("a.del = '0' AND (a.postsFK=" . $post_id . " OR a.postsFK IS NULL )");
		} else {
			$this->db->where("a.del = '0' AND a.id = " . $tip_id);
		}
		$this->db->from('tips AS a ');
		$this->db->join('users AS b', 'a.usersFK = b.id', "left");
		$this->db->join('categories AS c', 'a.categoriesFK = c.id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				if ($row->username) {
					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//					if ($row->avatar != "") {
//						$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//					} else {
//						$row->avatar = base_url() . $this->config->item("my_avatars_folder") . "user-female.jpg";
//					}
				}
				$row->currency_sign = $this->lang->line("fe_currency")[$row->currency];
				$row->new_link = $row->link;
				$row->cat_big_name = mb_convert_case($row->name, MB_CASE_UPPER, "UTF-8");
				$row->local_image = "";
				if ($row->image != "") {
					$image_folder = substr($row->image, 0, 2);
					$row->local_image = $this->config->item("my_upload_tips_folder") . $image_folder . "/" . $row->image;
					$row->image = $row->image = base_url() . $this->config->item("my_upload_tips_folder") . $image_folder . "/" . $row->image;
				}
				$row->is_thanked = $this->is_thnxd($row->id);
				$row->is_own = false;
				if ($row->selector != "" && $this->session->userdata("selector") == $row->selector) {
					$row->is_own = true;
				}
				if ($row->alias == "") {
					$row->profile_url = base_url() . $this->config->item("my_urlparams_user") . "/" . $row->selector;
				} else if ($row->selector != "") {
					$row->profile_url = base_url() . $row->alias;
				} else {
					$row->profile_url = "";
				}

				$row->can_delete = false;
				$row->can_delete = $this->users_model->access("tips", "delete", array("is_own" => $row->is_own, "date" => $row->date));
				if ($ajax) {
					$result_array = array();
					$result_array["id"] = $row->id;
					$result_array["date"] = $row->date;
					$result_array["provider_domain"] = $row->provider_domain;
					$result_array["user"] = $row->username;
					$result_array["selector"] = $row->selector;
					$result_array["alias"] = $row->alias;
					$result_array["profile_url"] = $row->profile_url;
					$result_array["is_thanked"] = $row->is_thanked;
					$result_array["is_own"] = $row->is_own;
					$result_array["brand"] = $row->brand;
					$result_array["price"] = $row->price;
					$result_array["currency_sign"] = $row->currency_sign;
					$result_array["new_link"] = $row->new_link;
					$result_array["image"] = $row->image;
					$result_array["thanks"] = $row->thanks;
					$result_array["avatar"] = $row->avatar;
					$result_array["cat_name"] = $row->name;
					$result_array["param"] = $row->param;
					$result_array["cat_big_name"] = $row->cat_big_name;
					$result_array["can_delete"] = $row->can_delete;
					$result[$row->categoriesFK][] = $result_array;
				} else {
					$result[$row->categoriesFK][] = (array) $row;
				}
			}
		}
		return $result;
	}

	private function redirected_url($url) {
		$temp_url = $url;
		$headers = @get_headers($url, 1);
		if (is_array($headers)) {
			if ($headers[0] == 'HTTP/1.1 404 Not Found') {
				return false;
			} else {
				if (isset($headers['Location'])) {
					$this_params = parse_url($headers['Location']);
					if (!isset($this_params["scheme"]) || !isset($this_params["host"])) {
						return $url;
					} else {
						return $headers['Location'];
					}
				}
			}
		} else {
			return false;
		}
	}

	public function forbidden_domain($url) {
		$url_array = explode("?", $url);
		$url_params = parse_url($url_array[0]);
		$result["result"] = false;
		$result["url"] = $url;
		if (isset($url_params["scheme"]) && isset($url_params["host"])) {
//			$redirect_result = $this->redirected_url($url, $url_params);
//			if (is_array($redirect_result)) {
//				$result["result"] = false;
//				$result["skip_data"] = true;
//				return $result;
//			}
//			if ($redirect_result != false) {
			if (isset($url_params["host"])) {
				$domain = mb_convert_case($url_params["host"], MB_CASE_LOWER, "UTF-8");
				$this->db->select("domain");
				$this->db->where("domain = '" . $domain . "' AND del='0' AND forbidden='1'");
				$this->db->from("tips_domains");
				$query = $this->db->get();
				$result["url"] = $url;
				if ($query->num_rows() > 0) {
					$result["result"] = true;
				}
			} else {
				$result["result"] = "invalid";
			}
//			} else {
//				echo "here 4"."\n";
//				$result["result"] = "invalid";
//			}
		}
//		die(print_r($result));
		return $result;
	}

	private function tips_domain_priority($domain) {
		$query = $this->db->query("SELECT priority FROM tips_domains WHERE domain = '" . $domain . "' AND del='0'");
		//die($this->db->last_query());
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				return $row->priority;
			}
		}
		return 0;
	}

	private function is_thnxd($tip_id) {
		if ($this->session->userdata('logged_in')) {
			$select_string = 'id';
			$this->db->select($select_string);
			$this->db->from('tips_thanks');
			$where_string = "tipsFK = " . $tip_id . " AND users_selectorFK='" . $this->session->userdata('selector') . "'";
			$this->db->where($where_string);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function add_thnx($tip_id, $user_selector, $domain = "") {
		if ($this->session->userdata('logged_in')) {
			$this->load->model('events_model');
			if (!$this->is_thnxd($tip_id)) {
				$sql_data = array("usersFK" => $this->session->userdata('id'), "tipsFK" => $tip_id, "users_selectorFK" => $this->session->userdata('selector'));
				$this->db->insert('tips_thanks', $sql_data);

				//increase likes in tips
				$this->db->where('id', $tip_id);
				$this->db->set('thanks', 'thanks+1', 'rating+' . $this->config->item("my_rating_thnx_increaser"), FALSE);
				$this->db->update('tips');

				if ($domain != "") {
					//increase likes in tips domain
					$this->db->where('domain', $domain);
					$this->db->set('rating', 'rating+' . $this->config->item("my_rating_thnx_increaser"), FALSE);
					$this->db->set('tips_thanks', 'tips_thanks+1', FALSE);
					$this->db->update('tips_domains');
				}

				//increase tips_likes for author
				$this->db->where('selector', $user_selector);
				$this->db->set('tips_thanks', 'tips_thanks+1', FALSE);
				$this->db->update('users');

				//ADD THANKS EVENT
				$event_array = array();
				$event_array["to_users_selectorFK"] = $user_selector;
				$event_array["from_users_selectorFK"] = $this->session->userdata('selector');
				$event_array["event_type"] = "thanks";
				$event_array["item1"] = $tip_id;
				$event_array["user_type"] = "user";
				$this->events_model->add_event($event_array);

				$this->users_model->update_user_rating("", $user_selector);
				return true;
			}
		}
	}

	public function del_tip($id) {
		$result = array();
		if ($id != "") {
			if (!$this->session->userdata("logged_in")) {
				$result["login"] = true;
			} else {
				$tip = $this->get_tips("", $id);
				sort($tip);
				$tip = $tip[0][0];
				if ($tip['can_delete']) {
					$thanx_array = $this->get_tip_thanks($tip["id"]);
					if (is_array($thanx_array)) {
						//process thnxs
						foreach ($thanx_array as $thnx) {
							$this->demote_tip_domain($tip["provider_domain"], $this->config->item("my_time_to_delete_tip"), true);
							if ($tip["users_selectorFK"] != "") {
								$this->demote_tip_user($tip["users_selectorFK"], $this->config->item("my_time_to_delete_tip"), true);
							}
							$this->db->query("DELETE FROM tips_thanks WHERE tipsFK = " . $tip["id"]);
						}
					}
					//delete tip
					$this->demote_tip_domain($tip["provider_domain"], 1);
					$this->demote_tip_post($tip["postsFK"]);
					$this->demote_tip_user($tip["users_selectorFK"], 1);
					$this->db->query("UPDATE tips SET del='1' WHERE id = " . $tip["id"]);
					//remove events
					$this->db->query("UPDATE events SET del='1' WHERE item3 = " . $tip["id"]);

					//unlink(FCPATH.$tip["local_image"]);
					$result["done"] = true;
					//demote tip domain rating, remove tips, demote tips_thanks from tips_domains // by 1
					//demote tip owner rating, remove tips, demote tips_thanks from tips_domains // by 1
				}
			}
		}
		return $result;
	}

	private function demote_tip_domain($domain, $demotion_by = 1, $with_thanks = false) {
		$thanks_demote = " tips = tips-1, ";
		if ($with_thanks) {
			$thanks_demote = " tips_thanks = tips_thanks-1, ";
		}
		$this->db->query("UPDATE tips_domains SET " . $thanks_demote . " rating = rating - " . $demotion_by . " WHERE domain = '" . $domain . "'");
	}

	private function demote_tip_user($selector, $demotion_by = 1, $with_thanks = false) {
		$thanks_demote = " tips = tips - " . $demotion_by . ", ";
		if ($with_thanks) {
			$thanks_demote = " tips_thanks = tips_thanks - " . $demotion_by . ", ";
		}
		$this->db->query("UPDATE users SET " . $thanks_demote . " rating = rating - " . $demotion_by . " WHERE selector = '" . $selector . "'");
	}

	private function demote_tip_post($post_id, $demotion_by = 1) {
		$demote = " tips = tips - " . $demotion_by;
		$this->db->query("UPDATE posts SET " . $demote . "  WHERE id = " . $post_id);
	}

	public function get_tip_thanks($id) {
		$this->db->from("tips_thanks");
		$this->db->where("tipsFK = " . $id);
		$query = $this->db->get();
		$result = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result[] = (array) $row;
			}
			return $result;
		} else {
			return false;
		}
	}

	private function imageResize($data) {
		$new_filename = $data["file_name"];
		$new_filename = explode(".", $new_filename);
		$new_filename = $new_filename[0] . ".jpg";
		$new_folder = substr($new_filename, 0, 2);

		if (!is_dir(FCPATH . $this->config->item('my_upload_tips_folder') . $new_folder)) {
			mkdir(FCPATH . $this->config->item('my_upload_tips_folder') . $new_folder, 0777, true);
		};
		$destination = FCPATH . $this->config->item('my_upload_tips_folder') . $new_folder . "/";

		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $data['full_path'];
		$this->load->library('image_lib', $config);
		$config['create_thumb'] = TRUE;
		$config['new_image'] = $destination;
		$config['maintain_ratio'] = TRUE;
		$config['thumb_marker'] = '';
		$config['new_image'] = $destination . $new_filename;
		$config['width'] = $this->config->item("my_imageupload_tip_width");
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
//		$this->load->library('sharpimage');
//		$this->sharpimage->sharpen_image_resize($data['full_path'], $destination.$new_filename, $this->config->item("my_imageupload_tip_width"));

	
		$info = array();
		$info['file'] = $new_filename;
		$info['local_file'] = $destination . $new_filename;
		$info['error'] = null;
		$files[] = $info;
		
		if ( !file_exists($destination.$new_filename)) {
//			die("here");
//			die($config['source_image']);
			//MYTODO: !!! fix here - image lib sometimes doesnot resize images from h&m
			echo "ERORR: tip image resize fail\n";
		} else {
			return $files;
		}
	}

	public function upload_tip_image($remote_url) {
		$mime_types = array(
			// images
			'image/png' => 'png',
			'image/jpeg' => 'jpe',
			'image/jpeg' => 'jpeg',
			'image/jpeg' => 'jpg'
				//,'image/gif' => 'gif'
		);

		error_reporting(0);
		$remote_image_info = getimagesize($remote_url);
		$env = ENVIRONMENT;
		enviroment($env);
//							error_reporting(E_ALL);
		if (!is_array($remote_image_info)) {
//			$opts = array('http' => array('header' => "User-Agent:MyAgent/1.0\r\n"));
//			$context = stream_context_create($opts);
			//MYTODO: !!!!!! ОПИТ със снимка чието урл е на кирилица и и празни места
//			$header = file_get_contents($remote_url, false, $context);
			$header = $this->session_browser($remote_url);
			if ($im = imagecreatefromstring($header)) {
				$image = $header;
			}
		}
		//check if it is image
		if (isset($image) || (isset($remote_image_info["mime"]) && stristr($remote_image_info["mime"], "image"))) {
			//check if image type is valid
			if (isset($image) || $mime_types[$remote_image_info["mime"]] != "") {
				//check if it is in allowed dimmensions
				if (!isset($image)) {
					$mime_type = $mime_types[$remote_image_info["mime"]];
					$image = file_get_contents($remote_url);
				} else {
					$mime_type = "jpg";
				}
				if (!is_dir(FCPATH . $this->config->item("my_temp_upload_folder"))) {
					mkdir(FCPATH . $this->config->item("my_temp_upload_folder"), 0777, true);
				};
				$new_filename = md5(time() . rand(1000, time()) . rand());
				$new_upload = FCPATH . $this->config->item("my_temp_upload_folder") . $new_filename . "." . $mime_type;
				//die($new_upload);
				file_put_contents($new_upload, $image);
				$data['full_path'] = $new_upload;
				$data['file_name'] = $new_filename . "." . $mime_type;
				$image_ok = $this->imageResize($data);
				unset($data);
				if (file_exists($new_upload)) {
					unlink($new_upload);
				}
				if (!file_exists($image_ok[0]["local_file"])) {
					return false;
				} else {
					return $new_filename;
				}
			}
		}
		return false;
	}

}

?>
