<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Upload_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->model('post_model');
	}

	public function get_remote_images($url) {

		$this->load->helper("remote-file");
		$original_url = $url;
		$url = urldecode($url);
		$url = trim($url);

		//check if url exists and if it is valid and forbidden
		//check if it is facebook
		$facebook = false;
		if (stristr($url, "facebook.com")) {
			$facebook = true;
		}
		if (!$facebook) {

			error_reporting(0);
			$remote_content = file_get_contents($url);
			if (trim($remote_content) == "") {
				$remote_content = curlGetPage($url);
			}
			$env = ENVIRONMENT;
			enviroment($env);

			if (trim($remote_content == "") || stristr($remote_content, "<h1>An Error Was Encountered</h1>")) {
				$url = $original_url;

				error_reporting(0);
				$remote_content = file_get_contents($url);
				if (trim($remote_content) == "") {
					$remote_content = curlGetPage($url);
				}
				$env = ENVIRONMENT;
				enviroment($env);
			}

			if (trim($remote_content) == "") {
				return array("error" => true);
			}
			$remote_content_clean = str_ireplace(array("\n", "\r", "	"), "", $remote_content);
			while (stristr($remote_content_clean, "  ")) {
				$remote_content_clean = str_ireplace("  ", " ", $remote_content_clean);
			}

			$remote_content_clean = str_ireplace("> <", "><", $remote_content_clean);
			$array_results = array();


			//get content between tags
			$reg_ex_img_slash = '/<img [^<]*\/\/(.*?)[\'"]/i';
			preg_match_all($reg_ex_img_slash, $remote_content_clean, $array_results[2]);
			unset($array_results[2][0]);
			sort($array_results[2]);
			$array_results[2] = $array_results[2][0];

			$reg_ex_img_src = '/<img [^<]*src=[\'"](.*?)[\'"]/i';
			preg_match_all($reg_ex_img_src, $remote_content_clean, $array_results[3]);
			unset($array_results[3][0]);
			sort($array_results[3]);
			$array_results[3] = $array_results[3][0];

			$reg_ex_img_backgrund = '/<[^<]*background-image:.*?url\((.*?)\)/i';
			preg_match_all($reg_ex_img_backgrund, $remote_content_clean, $array_results[4]);
			unset($array_results[4][0]);
			sort($array_results[4]);
			$array_results[4] = $array_results[4][0];

			$reg_ex_img_og = '/<meta[\s]+property="og:image"[\s]+content="(.*?)"[^>]+>/i';
			preg_match_all($reg_ex_img_og, $remote_content_clean, $array_results[5]);
			unset($array_results[5][0]);
			sort($array_results[5]);
			$array_results[5] = $array_results[5][0];

			//clean each image block
			$url_params = parse_url($url);

			//merge all in one images
			$image_array = array_merge($array_results[5], $array_results[2], $array_results[3], $array_results[4]);
			unset($array_results[5], $array_results[2], $array_results[3], $array_results[4]);
			$image_array = array_unique($image_array);
			//get base
			$base_tag = "";
			if (stristr($remote_content_clean, "<base ")) {
				$reg_ex_base_tag = '/<base [^<]*\/\/(.*?)[\'"]/i';
				preg_match_all($reg_ex_base_tag, $remote_content_clean, $base_tag);
				if (isset($base_tag[1][0])) {
					$base_tag = $base_tag[1][0];
					$strpos_http = strpos($base_tag, "http");
					if ($strpos_http !== 0) {
						if (stristr($base_tag, "//") && strpos($base_tag, "//") == 0) {
							$base_tag = "http:" . $base_tag;
						} else if (stristr($base_tag, "/") && strpos($base_tag, "/") == 0) {
							$base_tag = $url_params["scheme"] . "://" . $url_params["host"] . $base_tag;
						} else if (stristr($base_tag, "www") && strpos($base_tag, "www") == 0) {
							$base_tag = "http://" . $base_tag;
						}
					} else {
						$base_tag = "http://" . $base_tag;
					}
				}
			}
			$url_params["base"] = $base_tag;
			$image_array = $this->prepare_images($image_array, $url_params);
		} else {
			$image_array = $this->get_facebook_image($url);
		}

		if (count($image_array) > 0) {
			$final_result["images"] = $image_array;
		} else {
			$final_result["images"] = array();
		}
//		die(print_r($final_result));
		return $final_result;
	}

	private function get_facebook_image($url) {
		//get facebook page $id
		//https://www.facebook.com/FioreMakeUp/photos/pcb.1172458256152076/1172457652818803/?type=3&theater
		if (stristr($url, "?")) {
			$temp = explode("?",trim($url));
			$url = trim($temp[0],"/");
		}
		$url = explode("/",$url);
		$id = $url[count($url)-1];
		if ($id!="") {
			//get facebook access token
			$env = ENVIRONMENT;
			error_reporting(0);
			$fb_token = trim(file_get_contents("https://graph.facebook.com/oauth/access_token?client_id=".$this->config->item("appId")."&client_secret=".$this->config->item("secret")."&grant_type=client_credentials"));
			$post_data = file_get_contents("https://graph.facebook.com/".$id."?fields=name,images,link,from&".$fb_token);
			$json_data = json_decode($post_data,true);
			enviroment($env);
			if (is_array($json_data) && !isset($json_data["error"])) {
				$result["facebook_image"] = $json_data["images"][0];
				if (isset($json_data["name"])) {
					$result["facebook_description"] = $json_data["name"];
				}
				return $result;
			}
			//error
		}
	}

	private function prepare_images($images, $url_params) {
		$images_array = array();
		foreach ($images as $i_v) {
			$i_v = trim($i_v);
			if ($i_v != "") {
				if (stristr($i_v, " ")) {
					$i_v = explode(" ", $i_v);
					$i_v = $i_v[0];
				}
				if ($this->is_image($i_v) && !stristr($i_v, "logo") && !stristr($i_v, "../")) {
					if ($url_params["base"] != "") {
						$second_img = $url_params["base"] . $i_v;
					}
					$strpos_http = strpos($i_v, "http");
					if ($strpos_http !== 0) {
						if (stristr($i_v, "//") && strpos($i_v, "//") == 0) {
							$i_v = "http:" . $i_v;
						} else if (stristr($i_v, "/") && strpos($i_v, "/") == 0) {
							$i_v = $url_params["scheme"] . "://" . $url_params["host"] . $i_v;
						} else if (stristr($i_v, "www") && strpos($i_v, "www") == 0) {
							$i_v = "http://" . $i_v;
						} else {
							$temp_url = explode(".", $i_v);
							if (!stristr($temp_url[0], "/")) {
								$i_v = "http://" . $i_v;
							} else {
								$i_v = $url_params["scheme"] . "://" . $url_params["host"] . "/" . $i_v;
							}
						}
					}
					if (!in_array($i_v, $images_array)) {
						$images_array[] = $i_v;
					}
					if (isset($second_img) && !in_array($second_img, $images_array)) {
						$images_array[] = $second_img;
						unset($second_img);
					}
				}
			}
		}
		$images_array = array_slice(array_unique($images_array), 0, 100);
		return $images_array;
	}

	private function is_image($url) {
		foreach (array(".jpg", ".jpeg", ".png") as $ext) {
			if (stristr($url, $ext)) {
				return true;
			}
		}
		return false;
	}

	private function curl_mime($url) {
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0');
		$query = curl_exec($curl_handle);
		curl_close($curl_handle);
//		$f = finfo_open();
//		$result["mime"] = finfo_buffer($f, $query, FILEINFO_MIME_TYPE);
		$result = getimagesizefromstring($query);
		$result["image_data"] = $query;
		return $result;
	}

	public function upload_web_image($web_image) { //$this->input->post("web_image")
		if ($this->session->userdata('selector') != null) {
			$new_filename = md5($this->session->userdata('selector') . time() . rand(1000, time()) . $this->input->ip_address());
		} else {
			$new_filename = md5(uniqid(mt_rand(), true) . time() . rand(1000, time()) . $this->input->ip_address());
		}
		$out = array();
		$mime_types = array(
			// images
			'image/png' => 'png',
			'image/jpeg' => 'jpe',
			'image/jpeg' => 'jpeg',
			'image/jpeg' => 'jpg'
//					,'image/gif' => 'gif'
		);
		$web_image = str_ireplace(" ", "%20", $web_image);
		error_reporting(0);
		$remote_image_info = getimagesize($web_image);
		$env = ENVIRONMENT;
		enviroment($env);
		if ($remote_image_info == "") {
			$remote_image_info = $this->curl_mime($web_image);
		}
		if (isset($remote_image_info["mime"]) && stristr($remote_image_info["mime"], "image")) {
			//check if image type is valid
			if ($mime_types[$remote_image_info["mime"]] != "") {
				//check if it is in allowed dimmensions
				if ($remote_image_info[0] >= $this->config->item("my_imageupload_min_w") && $remote_image_info[1] >= $this->config->item("my_imageupload_min_h")) {
					if (!isset($remote_image_info["image_data"])) {
						$image = file_get_contents($web_image);
					} else {
						$image = $remote_image_info["image_data"];
					}
					if (!is_dir(FCPATH . $this->config->item("my_temp_upload_folder"))) {
						mkdir(FCPATH . $this->config->item("my_temp_upload_folder"), 0755, true);
					};
					$new_upload = FCPATH . $this->config->item("my_temp_upload_folder") . $new_filename . "." . $mime_types[$remote_image_info["mime"]];
					file_put_contents($new_upload, $image);
					$data['full_path'] = $new_upload;
					$data['file_name'] = $new_filename . "." . $mime_types[$remote_image_info["mime"]];
					$files = $this->imageResize($data);
					$out["files"] = $files;
					$out["new_filename"] = $new_filename;
					if (file_exists($new_upload)) {
						$out["unknown_error"] = true;
					}
				} else {
					$out["unknown_error"] = true;
				}
			} else {
				$out["unknown_error"] = true;
			}
		} else {
			$out["unknown_error"] = true;
		}
		return $out;
	}

	public function imageResize($data) {
		$this->load->library('sharpimage');
		//die($data["file_name"]);
		if (exif_imagetype(FCPATH . $this->config->item("my_temp_upload_folder") . $data["file_name"]) == 2) {
			error_reporting(0);
			$exif = exif_read_data(FCPATH . $this->config->item("my_temp_upload_folder") . $data["file_name"]);
			$env = ENVIRONMENT;
			enviroment($env);
//			error_reporting(E_ALL);
		}

		$new_filename = str_ireplace("tmp_", "", $data["file_name"]);
		$new_filename = explode(".", $new_filename);
		$new_filename = $new_filename[0] . ".jpg";
		$new_folder = substr($new_filename, 0, 2);

		//$this->delTree(FCPATH . $this->config->item('my_upload_small_images'));
		//$this->delTree(FCPATH . $this->config->item('my_upload_big_images'));
		if (!is_dir(FCPATH . $this->config->item('my_upload_small_images') . $new_folder)) {
			mkdir(FCPATH . $this->config->item('my_upload_small_images') . $new_folder, 0777, true);
		};
		$destination_small = FCPATH . $this->config->item('my_upload_small_images') . $new_folder . "/";
		if (!is_dir(FCPATH . $this->config->item('my_upload_big_images') . $new_folder)) {
			mkdir(FCPATH . $this->config->item('my_upload_big_images') . $new_folder, 0777, true);
		};
		$destination_big = FCPATH . $this->config->item('my_upload_big_images') . $new_folder . "/";

		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $data['full_path'];
		$this->load->library('image_lib', $config);
		if (isset($exif['Orientation']) && $exif['Orientation'] != 1) {
			switch ($exif['Orientation']) {
				case 1: // no need to perform any changes
					break;
				case 2: // horizontal flip
					$oris = 'hor';
					break;
				case 3: // 180 rotate left
					$oris = '180';
					break;
				case 4: // vertical flip
					$oris = 'ver';
					break;
				case 5: // vertical flip + 90 rotate right
					$oris = 'ver';
					$oris = '270';
					break;
				case 6: // 90 rotate right
					$oris = '270';
					break;
				case 7: // horizontal flip + 90 rotate right
					$oris = 'hor';
					$oris = '270';
					break;
				case 8: // 90 rotate left
					$oris = '90';
					break;
				default: break;
			}
			$config['rotation_angle'] = $oris;
			$this->image_lib->clear();
			$this->image_lib->initialize($config);
			$this->image_lib->rotate();
		}

		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['thumb_marker'] = '';
		$config['new_image'] = $destination_small . $new_filename;
		$config['width'] = $this->config->item("my_imageupload_min_w");
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->sharpimage->sharpen_image($config['new_image']);

		$config['new_image'] = $destination_big . $new_filename;
		$config['width'] = $this->config->item("my_imageupload_max_w");
		$config['height'] = $this->config->item("my_imageupload_max_h");
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->sharpimage->sharpen_image($config['new_image']);
		unlink($data['full_path']);
		$info = new StdClass;
		$info->deleteType = 'DELETE';
		$info->error = null;

		$files[] = $info;
		return $files;
	}

}

?>