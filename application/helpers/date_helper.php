<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

if (!function_exists('dateToPeriods')) {
	function dateToPeriods($date) {
		$date = time() - strtotime($date);
		$result = array("type" => "", "value" => "");
		if ($date <= (59*60) || $date < 60) { //only minutes has past
			$result["type"] = "minutes";
			$date = ($date - ($date % 60)) / 60;//convert to minutes
			if ($date<1) {
				$date = 1;
			}
			$result["value"] = $date;
		} else if ($date >= (24*60*60)) { //only days
			$result["type"] = "days";
			$date = ($date - ($date % 60)) / 60;//convert to minutes
			$date = ($date - ($date % 60)) / 60;//convert to hours
			$date = ($date - ($date % 24)) / 24;//convert to days
			if ($date < 1) {
				$date = 1;
			}
			$result["value"] = $date;
		} else { //this should be hours
			$result["type"] = "hours";
			$date = ($date - ($date % 60)) / 60;//convert to minutes
			$date = ($date - ($date % 60)) / 60;//convert to hours
			if ($date < 1) {
				$date = 1;
			}
			$result["value"] = $date;
		}
		return $result;
	}
}