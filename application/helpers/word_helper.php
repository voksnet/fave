<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

if (!function_exists('capitalize')) {
	function capitalize($str, $encoding = 'UTF-8') {
		return mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) . mb_strtolower(mb_substr($str, 1, mb_strlen($str), $encoding), $encoding);
	}
}