<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

function curl_get_file_size($url) {
	// Assume failure.
	$result = -1;

	$curl = curl_init($url);

	// Issue a HEAD request and follow any redirects.
	curl_setopt($curl, CURLOPT_NOBODY, true);
	curl_setopt($curl, CURLOPT_HEADER, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	//curl_setopt($curl, CURLOPT_USERAGENT, get_user_agent_string());

	$data = curl_exec($curl);
	curl_close($curl);

	if ($data) {
		$content_length = "unknown";
		$status = "unknown";

		if (preg_match("/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches)) {
			$status = (int) $matches[1];
		}

		if (preg_match("/Content-Length: (\d+)/", $data, $matches)) {
			$content_length = (int) $matches[1];
		}

		// http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
		if ($status == 200 || ($status > 300 && $status <= 308)) {
			$result = $content_length;
		}
	}
	if ($result == "unknown") {
		$result = "-2";
	}
	return $result;
}


function addhttp($url) {
	if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
		$url = "http://" . $url;
	}
	$url = str_ireplace("////", "//", $url);
	return $url;
}

function curlGetPage($urlToScan) {
	/* Get the html contents of a page and return that as $data */
	$crl = curl_init();
	curl_setopt($crl, CURLOPT_URL, $urlToScan);
	curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, 5);
	$data = curl_exec($crl);
	curl_close($crl);

	if (empty($data)) {
		$data = '';
	}

	return $data;
}

?>