<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$config['my_login_remember'] = 62; //days
$comfig['my_username_salt'] = "vPzgK43z4!3^0fWVud1m";
$config['my_user_update_time'] = 15 * 60;
$config['my_google_id'] = "566661129400-mhcs3l4q29ffunn72hdb3hruabtml71e.apps.googleusercontent.com";
$config['my_temp_upload_folder'] = "temp/";
$config['my_upload_folder'] = "pics/";
$config['my_upload_tips_folder'] = "tips/";
$config['my_avatars_folder'] = "avatars/";
$config['my_avatars_users_folder'] = "u/";
$config['my_avatars_packs_folder'] = "s/";
$config['my_cookie_folder'] = "temp/cookies/";
$config['my_upload_small_images'] = $config['my_upload_folder'] . "small/";
$config['my_upload_big_images'] = $config['my_upload_folder'] . "big/";
$config['my_max_filesize_upload'] = 7000000;
$config['my_comment_max_length'] = 600;
$config["my_imageupload_min_w"] = 330;
$config["my_imageupload_min_h"] = 270;
$config["my_imageupload_max_w"] = 610;
$config["my_imageupload_max_h"] = 610;
$config["my_masonry_per_page"] = 40;
$config["my_followers_per_page"] = 40;
$config["my_masonry_large_per_page"] = 800;
$config["my_masonry_session_time"] = 280; //minutes
$config["my_masonry_per_page_related"] = $config["my_masonry_per_page"];
$config["my_max_tags"] = 10;
$config["my_sponsored_positions"] = array(2, 22);

$config["my_max_matrix_sessions"] = 5; // don't touch it
$config["my_rating_thnx_increaser"] = 10;
$config["my_min_tag_length"] = 2;
$config["my_max_tag_length"] = 25;
$config['my_censorship'] = true;
$config['my_periods_for_posts'] = 60 * 60 * 24 * 7;
$config["my_max_cats_per_post"] = 8;
$config["my_max_tips"] = 8;
$config["my_min_tip_price"] = 0;
$config["my_max_tip_price"] = 100000;
$config["my_max_brand_length"] = 50;
$config["my_min_brand_length"] = 2;
$config["my_imageupload_tip_width"] = 100;
$config["my_featured_items"] = 19; //+1
$config["my_max_avatars_user"] = 5;
$config["my_time_to_delete_tip"] = 10; //mins

$config["my_urlparams_myaccount"] = "mc"; //must set manualy in routes.php
$config["my_urlparams_show"] = "sh"; //must set manualy in routes.php
$config["my_urlparams_view"] = "v"; //must set manualy in routes.php
$config["my_urlparams_upload"] = "up"; //must set manualy in routes.php
$config["my_urlparams_search"] = "s"; //must set manualy in routes.php
$config["my_urlparams_user"] = "u"; //must set manualy in routes.php
$config["my_urlparams_blog"] = "b"; //must set manualy in routes.php

$config["my_support_address"] = "ул. Деница №1, офис 12, София, България";
$config["my_support_phone"] = "+359 877 088 744";
$config["my_emails_support"] = "<eml>support</eml>";
$config["my_emails_spam"] = "<eml>support</eml>";
$config["my_emails_brands"] = "<eml>brands</eml>";
$config["my_emails_jobs"] = "<eml>jobs</eml>";
$config["my_emails_media"] = "<eml>media</eml>";
$config["my_terms_last_update"] = "15.07.2016 г.";
$config["my_socialmedia_facebook"] = "https://facebook.com/fave.bg";
$config["my_socialmedia_linkedin"] = "https://www.linkedin.com/in/fave-bg-927294125";
$config["my_socialmedia_twitter"] = "https://twitter.com/BgFave";
$config["my_socialmedia_pinterest"] = "https://www.pinterest.com/favebg/";
$config["my_socialmedia_instagram"] = "https://www.instagram.com/fave.bg/";


$config["my_adsense_responsive_unit"] = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                        <!-- Fave.bg -->
                                        <ins class="adsbygoogle"
                                             style="display:block"
                                             data-ad-client="ca-pub-3416996348473182"
                                             data-ad-slot="8292835950"
                                             data-ad-format="rectangle"></ins>
                                        <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                        </script>';

if (isset($_SERVER["REMOTE_ADDR"])) {
	$config["my_dev_ips"] = array(
		"85.11.140.50", // Tsanko
		"95.42.36.188",
		"46.238.18.97", // Velislav,
		"95.42.9.6"	 // office
	);
	if (!in_array($_SERVER["REMOTE_ADDR"], $config["my_dev_ips"])) {
		$config["my_analytics"] = "<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
						m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
			ga('create', 'UA-80868549-1', 'auto');
			ga('send', 'pageview');
		</script>";
	} else {
		$config["my_analytics"] = "";
	}
} else {
	$config["my_analytics"] = "";
}

$config['fb_social_plugin'] = "<div id='fb-root'></div>
                                <script>(function(d, s, id) {
                                  var js, fjs = d.getElementsByTagName(s)[0];
                                  if (d.getElementById(id)) return;
                                  js = d.createElement(s); js.id = id;
                                  js.src = '//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.7&appId=1151140538265910';
                                  fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>";