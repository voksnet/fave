<?php

class HTMLParser {

	private $document;

	public function __construct($htmlString, $preserveWhiteSpace = false, $formatOutput = false) {
		$original_error_reporting = error_reporting();
		error_reporting(0);
		$this->document = new DOMDocument;
		$this->document->preserveWhiteSpace = $preserveWhiteSpace;
		$this->document->formatOutput = $formatOutput;
		$this->document->loadHTML(mb_convert_encoding($htmlString, 'HTML-ENTITIES', 'UTF-8'));
		error_reporting($original_error_reporting);
	}

	public function getElementsByTagName($tagName) {
		$result = array();
		$elements = $this->document->getElementsByTagName($tagName);
		foreach ($elements as $element) {
			array_push($result, $element);
		}
		return $result;
	}

	public function getElementsByTagAndClassName($tagName, $className) {
		$result = array();
		$elements = $this->document->getElementsByTagName($tagName);

		foreach ($elements as $element) {
			$currentClassName = trim(HTMLParser::getElementAttribute($element, 'class'));
			$classes=array();
			if (stristr($currentClassName, " ")) {
				$currentClassName = str_ireplace("  ", " ", $currentClassName);
				$classes = explode(" ",$currentClassName);
			} else {
				$classes[] = $currentClassName;
			}
			if (in_array($className, $classes)) {
				array_push($result, $element);
			}
		}
		return $result;
	}

	public function getElementsByTagAndSingleClassName($tagName, $className) {
		$result = array();
		$elements = $this->document->getElementsByTagName($tagName);

		foreach ($elements as $element) {
			$currentClassNames = explode(' ', HTMLParser::getElementAttribute($element, 'class'));

			if (in_array($className, $currentClassNames)) {
				array_push($result, $element);
			}
		}
		return $result;
	}
	
	public function getElementsByTagAndSeveralClassNames($tagName, $classNames) {
		$result = array();
		$elements = $this->document->getElementsByTagName($tagName);

		foreach ($elements as $element) {
			$currentClassNames = explode(' ', HTMLParser::getElementAttribute($element, 'class'));
			$found_count=0;
			foreach ($classNames as $a_class ) {
				if (in_array($a_class, $currentClassNames)) {
					$found_count++;
				}
			}
			if ($found_count > 0 && $found_count == count($classNames)) {
				array_push($result, $element);
			}
		}
		return $result;
	}

	public function getElementsByTagAndAttribute($tagName, $attributeName) { //, $attributeValue
		$result = array();
		$elements = $this->document->getElementsByTagName($tagName);
		foreach ($elements as $element) {
			foreach ($element->attributes as $attribute) {
				if ($attribute->name === $attributeName) { // && $attribute->value === $attributeValue
					array_push($result, $element);
				}
			}
		}
		return $result;
	}

	public function getElementById($id) {
		return $this->document->getElementById($id);
	}

	public static function getElementAttribute($element, $attributeName) {
		foreach ($element->attributes as $attribute) {
			if ($attribute->name == $attributeName) {
				return $attribute->value;
			}
		}
		return NULL;
	}

	public static function getElementText($element) {
		return trim($element->textContent);
	}

	public static function getElementHTML($element) {
		return $element->ownerDocument->saveHTML($element);
	}

}