<?php

define("BASEPATH", "scraper");

$isCLI = ( php_sapi_name() == 'cli' );
if ($isCLI) {
	set_time_limit(3600);

//	require_once "htmlfixer.class.php";

	function xmlstr($string, $start, $end) {
		$string = " " . $string;
		$ini = strpos($string, $start);
		if ($ini == 0)
			return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	function del_from_content($text, $start_del, $end_del) {
		$info_del = $start_del . xmlstr($text, $start_del, $end_del) . $end_del;
		$text = str_ireplace($info_del, '', $text);
		return $text;
	}

	function clean_hrefs($description) {
		$prev = 0;
		while (stristr($description, '<a ')) {
			$description = del_from_content($description, '<a ', '>');
			$prev++;
			if ($prev == 50)
				break;
		}
		$description = str_replace('</a>', '', $description);
		return $description;
	}

	function clean_html($description) {
		$description = str_replace(array('<h2', '<h1'), '<h3', $description);
		$description = str_replace(array('</h2', '</h1'), '</h3', $description);
		$description = del_from_content($description, '<fb:like', '</fb:like>');

		$prev = 0;
		while (stristr($description, 'class="')) {
			$description = del_from_content($description, 'class="', '"');
			$prev++;
			if ($prev == 50)
				break;
		}

		$prev = 0;
		while (stristr($description, '<!--')) {
			$description = del_from_content($description, '<!--', '-->');
			$prev++;
			if ($prev == 50)
				break;
		}

		$prev = 0;
		while (stristr($description, '<font')) {
			$description = del_from_content($description, '<font', '>');
			$prev++;
			if ($prev == 50)
				break;
		}
		$description = str_replace('<u>', '<strong>', $description);
		$description = str_replace('</u>', '</strong>', $description);
		$description = str_ireplace('</font>', '', $description);
		$description = str_replace(array('</b>', '<b>'), array('</strong>', '<strong>'), $description);
		$description = str_replace('<strong></strong>', '', $description);

		$prev = 0;
		while (stristr($description, 'style="')) {
			$description = del_from_content($description, 'style="', '"');
			$prev++;
			if ($prev == 50)
				break;
		}

		$prev = 0;
		while (stristr($description, 'id="')) {
			$description = del_from_content($description, 'id="', '"');
			$prev++;
			if ($prev == 50)
				break;
		}

		$prev = 0;
		while (stristr($description, '<script')) {
			$description = del_from_content($description, '<script', '</script>');
			$prev++;
			if ($prev == 50)
				break;
		}

		$prev = 0;
		while (stristr($description, '<iframe')) {
			$description = del_from_content($description, '<iframe', '</iframe>');
			$prev++;
			if ($prev == 50)
				break;
		}

		$prev = 0;
		while (stristr($description, 'lang="')) {
			$description = del_from_content($description, 'lang="', '"');
			$prev++;
			if ($prev == 50)
				break;
		}

		$prev = 0;
		while (stristr($description, 'width="')) {
			$description = del_from_content($description, 'width="', '"');
			$prev++;
			if ($prev == 50)
				break;
		}

		$prev = 0;
		while (stristr($description, 'height="')) {
			$description = del_from_content($description, 'height="', '"');
			$prev++;
			if ($prev == 50)
				break;
		}

		$description = str_ireplace(array('<p>', '</p>'), '<br />', $description);
		$description = str_ireplace('</strong><strong>', '', $description);
		$description = str_replace('<hr>', '', $description);
		$description = del_from_content($description, '<a href="http://www.facebook', '/a>');
		$description = del_from_content($description, '<a href="https://www.facebook', '/a>');
		$description = del_from_content($description, '<a href="http://bg-bg.facebook', '/a>');
		$description = del_from_content($description, '<a href="https://bg-bg.facebook', '/a>');

		$prev = 0;
		while (stristr($description, 'target="')) {
			$description = del_from_content($description, 'target="', '"');
			$prev++;
			if ($prev == 50)
				break;
		}
		$description = str_ireplace('<a ', '<a rel="nofollow" target="_blank" ', $description);
		$description = preg_replace("/[\n\r]/", "", $description);
		$prev = 0;
		while (stristr($description, '  ') || stristr($description, '	')) {
			$description = str_replace(array("  ", '	'), '', $description);
			$prev++;
			if ($prev == 1000)
				break;
		}
		$prev = 0;
		while (stristr($description, '<g:plusone')) {
			$description = del_from_content($description, '<g:plusone', '</g:plusone>');
			$prev++;
			if ($prev == 50)
				break;
		}
		$description = str_replace("< ", "", $description);
		$description = str_replace(" >", ">", $description);
		$description = str_replace("<span>", "", $description);
		$description = str_replace("</span>", "", $description);
		$description = str_replace("<div>", '|||', $description);
		$description = str_replace("<br/>", '<br />', $description);
		$description = str_replace("<br>", "<br />", $description);
		while (strstr($description, '</div></div>')) {
			$description = str_replace("</div></div>", '</div>', $description);
		}
		$description = str_replace('<ul></ul>', '', $description);
		$description = str_replace("</div>", '|||', $description);
		//$description=trim($description,'<br clear="all"/>');
		while (strstr($description, '||||||')) {
			$description = str_replace('||||||', '|||', $description);
		}
		$description = str_ireplace(array('</p>', '<p>'), '', $description);
		$cleaner = new HtmlFixer();
		$description = $cleaner->getFixedHtml($description);
		$description = trim($description, '|||');
		$description = trim($description);
		$description = str_replace('|||', '<br /><br />', $description);
		while (strstr($description, '<br /><br /><br />')) {
			$description = str_replace('<br /><br /><br />', '<br /><br />', $description);
		}
		$description = str_replace('> <', '><', trim($description));
		return($description);
	}

	function xmlstr_to_array($xmlstr) {
		$doc = new DOMDocument();
		$doc->loadXML($xmlstr);
		return domnode_to_array($doc->documentElement);
	}

	function check_dups() {
		
	}

	function session_browser_post($url, $post) {
		$cookie_file_path = "cookie.txt";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_AUTOREFERER, 0);
		//curl_setopt($curl, CURLOPT_REFERER, $reffer);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36"); //$_SERVER['HTTP_USER_AGENT']
		curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_COOKIESESSION, TRUE);
		curl_setopt($curl, CURLOPT_COOKIE, session_name() . '=' . session_id());
		curl_setopt($curl, CURLOPT_URL, $url);
		//curl_setopt($curl, CURLOPT_POSTFIELDS, "username=admin&password=323123&pageaction=login&");
		$data = curl_exec($curl);
		//$signupStr=getSignupString($data);
		//curl_setopt($curl, CURLOPT_URL,"http://localhost/taiba/tfcsadmcp/index.php?component=cman&page=wce.gall.php");//set this URL to wherever the form submits to
		curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookie_file_path);
		//curl_setopt($curl, CURLOPT_POST,1);//yes we want to post
		//curl_setopt($curl, CURLOPT_POSTFIELDS,$signupStr);//tell it where to find our sign up string
		$data = curl_exec($curl);
		curl_close($curl); //close the session
		if (file_exists($cookie_file_path)) {
			unlink($cookie_file_path);
		}
		return $data;
	}

	function curl_browser($url) {
		/* STEP 1. let's create a cookie file */
//		$ckfile = ROOT . "/tools/tmp/" . md5($url) . "cookie.txt";
		/* STEP 2. visit the homepage to set the cookie properly */
		$ch = curl_init($url);
//		curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		/* STEP 3. visit cookiepage.php */
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_USERAGENT, "spider");
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
//		curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
		$output = curl_exec($ch);
		$header = curl_getinfo($ch);
		//$output = curl_exec ($ch);
//		if (file_exists($ckfile)) unlink($ckfile);
		return $output;
	}

	function clean_price($price) {
		$price = clean_spaces($price);
		$price = trim(str_ireplace("лв.", "", $price));
		$price = trim(str_ireplace("лв", "", $price));
		$price = str_replace(',', '.', $price);
		return $price;
	}

	function clean_spaces($string) {
		while (stristr($string, "  ")) {
			$string = str_ireplace("  ", " ", $string);
		}
		return $string;
	}

	require_once "sites.php";


	if (stristr($_SERVER["PWD"], "/var/www/fave.bg/")) {
		$base_url = "fave.bg";
		$protocol = "https://";
	} else {
		$base_url = "tsanko.voksnet.com/fave";
		$protocol = "http://";
	}


	switch ($argv[1]) {
		case "hm":
			$input = array();
			//86, 1z13yoe
			$main_data = array();
			$main_data["base"] = "http://www2.hm.com";
			$main_data["brand"] = "H&M";
			$main_data["users_selectorFK"] = '1z13yoe';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www2.hm.com";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/jaketa-i-palta/jaketa.html?page-size=100000";
			$temp_input["tags"] = "H&M, яке";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/jaketa-i-palta/palta.html?page-size=900000";
			$temp_input["tags"] = "H&M, палто, дамско палто";
			$temp_input["category"] = "палто";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/jaketa-i-palta/bomber-jackets.html?page-size=900000";
			$temp_input["tags"] = "H&M, яке, бомбър, дамско яке, яке бомбър";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/teniski/potnici.html?page-size=900000";
			$temp_input["tags"] = "H&M, потник, дамски потник";
			$temp_input["category"] = "потник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/teniski/s-kas-rakav.html?page-size=900000";
			$temp_input["tags"] = "H&M, тениска, блуза, дамкса блуза, дамска тениска, блуза с къс ръкав, тениска с къс ръкав";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/teniski/s-dalag-rakav.html?page-size=900000";
			$temp_input["tags"] = "H&M, тениска, блуза, дамкса блуза, дамска тениска, дълъг ръкав";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/rizi-i-bluzi/rizi.html?page-size=900000";
			$temp_input["tags"] = "H&M, риза, дамкса риза";
			$temp_input["category"] = "риза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/rizi-i-bluzi/tuniki.html?page-size=900000";
			$temp_input["tags"] = "H&M, туника";
			$temp_input["category"] = "туника";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/jiletki-i-puloveri/jiletki.html?page-size=900000";
			$temp_input["tags"] = "H&M, жилетка, дамска жилетка";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/jiletki-i-puloveri/puloveri.html?page-size=900000";
			$temp_input["tags"] = "H&M, пуловер, дамски пуловер";
			$temp_input["category"] = "пуловер";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/jiletki-i-puloveri/ponchos.html?page-size=900000";
			$temp_input["tags"] = "H&M, пончо";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/jiletki-i-puloveri/puloveri-s-polo-jaka.html?page-size=900000";
			$temp_input["tags"] = "H&M, пуловер, дамски пуловер, поло, дамско поло";
			$temp_input["category"] = "пуловер";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/jiletki-i-puloveri/bluza-s-i-bez-kachulka.html?page-size=900000";
			$temp_input["tags"] = "H&M, суичър, дамски суичър";
			$temp_input["category"] = "суичър";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/pantaloni/klinove.html?page-size=900000";
			$temp_input["tags"] = "H&M, клин, дамски клин";
			$temp_input["category"] = "клин";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/drehi-za-bremenni/belio-i-chorapogashtnici.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, бременни, дамско бельо, бельо за бременни";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/drehi-za-bremenni/teniski.html?page-size=900000";
			$temp_input["tags"] = "H&M, тениска, блуза, дамска блуза, бременни, блуза за бременни, тениска за бременни";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/drehi-za-bremenni/pantaloni.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, панталони за бременни";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/drehi-za-bremenni/danki.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дънки, панталони за бременни, дънки за бременни, дамски панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/drehi-za-bremenni/rokli.html?page-size=900000";
			$temp_input["tags"] = "H&M, рокля, рокля за бременни";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/drehi-za-bremenni/jaketa-i-saka.html?page-size=900000";
			$temp_input["tags"] = "H&M, яке, дамско яке, сако, яке за бременни";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/kasi-pantaloni.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, къси панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/banski/celi-banski.html?page-size=900000";
			$temp_input["tags"] = "H&M, бански, дамски бански, цели бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/banski/dolnishta-na-banski.html?page-size=900000";
			$temp_input["tags"] = "H&M, бански, дамски бански, долнища на бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/banski/gornishta-na-banski.html?page-size=900000";
			$temp_input["tags"] = "H&M, бански, дамски бански, горнища на бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/banski/banski-kostjumi.html?page-size=900000";
			$temp_input["tags"] = "H&M, бански, дамски бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/sportni-drehi/sportni-pantaloni-.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, спортни панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/sportni-drehi/jaketa.html?page-size=900000";
			$temp_input["tags"] = "H&M, яке, дамско яке, спортно яке";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/sportni-drehi/sportni-sutieni.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, сутиен, спортен сутиен";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/sportni-drehi/teniski.html?page-size=900000";
			$temp_input["tags"] = "H&M, тениска, блуза, дамска блуза, дамска тениска, спортна блуза, спортна тениска";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/spalni-drehi/noshtnici-i-halati.html?page-size=900000";
			$temp_input["tags"] = "H&M, нощница, дамска нощница, халат, пижама";
			$temp_input["category"] = "пижама";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/spalni-drehi/pijami.html?page-size=900000";
			$temp_input["tags"] = "H&M, пижама, дамска пижама";
			$temp_input["category"] = "пижама";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/chorapi-i-chorapogasht-nici/oformjashti.html?page-size=900000";
			$temp_input["tags"] = "H&M, чорапогащи, оформящи чорапогащи";
			$temp_input["category"] = "чорапогащи";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/chorapi-i-chorapogasht-nici/chorapogashtnici-i-klinove.html?page-size=900000";
			$temp_input["tags"] = "H&M, чорапогащи";
			$temp_input["category"] = "чорапогащи";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/pantaloni/dzhoging-pantalon.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, спортни панталони, джогинг панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/pantaloni/slim.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, слим панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/pantaloni/flare.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/pantaloni/chinos-slacks.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, памучени панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/pantaloni/culottes.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/dunki/super-skinny.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/dunki/super-skinny-jeggings.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/dunki/skinny.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/dunki/shaping-skinny.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/dunki/flare.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/dunki/boyfriend.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/dunki/kusi-pantaloni-ot-denim.html?page-size=900000";
			$temp_input["tags"] = "H&M, панталони, дамски панталони, къси панталони, дънки, шорти, дамски шорти";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/rokli/kasi-rokli.html?page-size=900000";
			$temp_input["tags"] = "H&M, рокля, къса рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/rokli/midi-dresses.html?page-size=900000";
			$temp_input["tags"] = "H&M, рокля, рокля под коляното";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/rokli/dalgi-rokli.html?page-size=900000";
			$temp_input["tags"] = "H&M, рокля, дълга рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/basics/tops/vests.html?page-size=900000";
			$temp_input["tags"] = "H&M, топ";
			$temp_input["category"] = "топ";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/basics/tops/short-sleeve.html?page-size=900000";
			$temp_input["tags"] = "H&M, топ, топ с къс ръква";
			$temp_input["category"] = "топ";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/basics/tops/long-sleeve.html?page-size=900000";
			$temp_input["tags"] = "H&M, топ, топ с дълъг ръкав";
			$temp_input["category"] = "топ";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/basics/cardigans-and-jumpers.html?page-size=900000";
			$temp_input["tags"] = "H&M, жилетка, дамска жилетка, пуловер, дамски пуловер";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/knitwear.html?page-size=900000";
			$temp_input["tags"] = "H&M, жилетка, дамска жилетка, пуловер, дамски пуловер";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/poli/kasi-poli.html?page-size=900000";
			$temp_input["tags"] = "H&M, пола, къся пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/poli/poli-pod-koljanoto.html?page-size=900000";
			$temp_input["tags"] = "H&M, пола, пола под коляното";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/poli/dalgi-poli.html?page-size=900000";
			$temp_input["tags"] = "H&M, пола, дълга пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/gashterizoni.html?page-size=900000";
			$temp_input["tags"] = "H&M, гащеризон, дамски гащеризон";
			$temp_input["category"] = "гащеризон";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/saka-i-eleci/saka.html?page-size=900000";
			$temp_input["tags"] = "H&M, сако, дамско сако";
			$temp_input["category"] = "сако";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/saka-i-eleci/eleci.html?page-size=900000";
			$temp_input["tags"] = "H&M, сако, дамско сако, елек, дамски елек";
			$temp_input["category"] = "сако";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/saka-i-eleci/kimono.html?page-size=900000";
			$temp_input["tags"] = "H&M, кимоно, дамско кимоно, елек";
			$temp_input["category"] = "сако";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/belio/sutieni.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, сутиен, дамско бельо";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/belio/bikini/bokserki.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, дамско бельо, боксерки, дамкси боксерки";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/belio/bikini/bikini.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, дмаско бельо, бикини";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/belio/bikini/prashki.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, дамско бельо, бикини, прашки";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/belio/bikini/komplekti.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, дамско бельо, бикини, комплект";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/belio/korseti.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, дамско бельо, корсет";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/belio/oformjashto-belio.html?page-size=900000";
			$temp_input["tags"] = "H&M, бельо, дамско бельо, оформящо бельо";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/obuvki/obuvki-na-visok-tok.html?page-size=900000";
			$temp_input["tags"] = "H&M, обувки, дамски, обувки, обувки на висок ток";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/obuvki/ravni-obuvki.html?page-size=900000";
			$temp_input["tags"] = "H&M, обувки, дамски, обувки, равни обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/obuvki/sandali-i-espadrili.html?page-size=900000";
			$temp_input["tags"] = "H&M, обувки, дамски обувки, сандали, дамски сандали";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/obuvki/kecove.html?page-size=900000";
			$temp_input["tags"] = "H&M, обувки, дамски обувки, кецове";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/obuvki/botushi.html?page-size=900000";
			$temp_input["tags"] = "H&M, обувки, дамски обувки, дамски ботуши, ботуши";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/obuvki/boti.html?page-size=900000";
			$temp_input["tags"] = "H&M, обувки, дамски обувки, боти";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/obuvki/premium-quality.html?page-size=900000";
			$temp_input["tags"] = "H&M, обувки, дамски обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/chorapi-i-chorapogasht-nici/chorapi.html?page-size=900000";
			$temp_input["tags"] = "H&M, чорапи, дамски чорапи";
			$temp_input["category"] = "чорапи";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/aksesoari/chanti.html?page-size=900000";
			$temp_input["tags"] = "H&M, чанта, дамска чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/aksesoari/kolani.html?page-size=900000";
			$temp_input["tags"] = "H&M, колан, дамски колан";
			$temp_input["category"] = "колан";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/aksesoari/bijuta/obetsi.html?page-size=900000";
			$temp_input["tags"] = "H&M, обеци, бижута, дамски бижута";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/aksesoari/bijuta/kolieta.html?page-size=900000";
			$temp_input["tags"] = "H&M, бижута, колие, дамски бижута";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/aksesoari/bijuta/grivni.html?page-size=900000";
			$temp_input["tags"] = "H&M, бижута, дамски бижута, гривни, дамски гривни";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/aksesoari/bijuta/prusteni.html?page-size=900000";
			$temp_input["tags"] = "H&M, бижута, дамски бижута, пръстени, дамски пръстени";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www2.hm.com/bg_bg/jeni/pazaruvane-po-vid-produkt/aksesoari/slanchevi-ochila.html?page-size=900000";
			$temp_input["tags"] = "H&M, очила, дамски очила";
			$temp_input["category"] = "слънчеви очила";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = array_merge(get_hm($inp));
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;

		case "hm_1":
			$input = array();
			//86, 1z13yoe
			$main_data = array();
			$main_data["base"] = "http://www2.hm.com";
			$main_data["brand"] = "H&M";
			$main_data["users_selectorFK"] = '1z13yoe';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www2.hm.com";
			$main_data["sex"] = "m";

			$temp_input["link"] = "http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/teniski-i-potnici/potnici.html?page-size=900000";
			$temp_input["tags"] = "H&M, блуза, тениска, мъжка блуза, мъжка тениска";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			//
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/teniski-i-potnici/s-kas-rakav.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/teniski-i-potnici/s-dalag-rakav.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/rizi/ejednevni.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/rizi/oficialni.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/rizi/denim.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/bluza-s-i-bez-kachulka.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/basics/t-shirts.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/basics/tank-tops.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/basics/hoodies-and-sweatshirts.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/basics/bottoms.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/jiletki-i-puloveri/jiletki.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/jiletki-i-puloveri/puloveri.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/saka-i-kostjumi/saka.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/saka-i-kostjumi/kostyumi.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/saka-i-kostjumi/ofitsialni-pantaloni.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/saka-i-kostjumi/premium-quality.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/jaketa-i-palta/jaketa.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/jaketa-i-palta/palta.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/jaketa-i-palta/bomber-jackets.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/pantaloni/pantaloni.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/pantaloni/chino.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/pantaloni/antsug.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/dunki.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/kasi-pantaloni.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/belio.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/chorapi.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/sport/teniski.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/sport/jaketa.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/sport/pantaloni.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/banski.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/aksesoari/scarves.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/aksesoari/ties-bow-tie-and-handkerchiefs.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/aksesoari/belts-and-suspenders.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/aksesoari/bags.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/aksesoari/sunglasses.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/aksesoari/jewellery.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/obuvki/kecove.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/obuvki/botushi.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/obuvki/premium-quality.html?page-size=900000
			//http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/obuvki/drugi.html?page-size=900000
//			$temp_input["link"] = "http://www2.hm.com/bg_bg/maje/pazaruvane-po-vid-produkt/teniski-i-potnici/polo.html?page-size=900000";
//			$temp_input["tags"] = "H&M, блуза, мъжка блуза, поло";
//			$temp_input["category"] = "блуза";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//						$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//						$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//						$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//						$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//						$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "H&M, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);


			foreach ($input as $inp) {
				$result = array_merge(get_hm($inp));
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "bless":
			$result = array();
			//87, 1z13ynn
			//http://www.blessbyzr.com/produkti

			$main_data = array();
			$main_data["base"] = "http://www.blessbyzr.com";
			$main_data["brand"] = "Bless";
			$main_data["sex"] = "f";
			$main_data["users_selectorFK"] = '1z13ynn';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "blessbyzr.com";

			$input[0] = $main_data;
			$input[0]["link"] = "http://www.blessbyzr.com/produkti/eletzi?limit=1000000start=0";
			$input[0]["tags"] = "Bless, елек, топ, златка райкова";
			$input[0]["category"] = "топ";

			$input[1] = $main_data;
			$input[1]["link"] = "http://www.blessbyzr.com/produkti/bluzi?limit=1000000start=0";
			$input[1]["tags"] = "Bless, блуза, златка райкова";
			$input[1]["category"] = "блуза";

			$input[2] = $main_data;
			$input[2]["link"] = "http://www.blessbyzr.com/produkti/pantaloni?limit=1000000start=0";
			$input[2]["tags"] = "Bless, панталони, златка райкова";
			$input[2]["category"] = "панталони";

			$input[3] = $main_data;
			$input[3]["link"] = "http://www.blessbyzr.com/produkti/banski?limit=1000000start=0";
			$input[3]["tags"] = "Bless, бански, златка райкова";
			$input[3]["category"] = "бански";

			$input[4] = $main_data;
			$input[4]["link"] = "http://www.blessbyzr.com/produkti/poli?limit=1000000start=0";
			$input[4]["tags"] = "Bless, пола, златка райкова";
			$input[4]["category"] = "пола";

			$input[5] = $main_data;
			$input[5]["link"] = "http://www.blessbyzr.com/produkti/rokli?limit=1000000start=0";
			$input[5]["tags"] = "Bless, рокля, златка райкова";
			$input[5]["category"] = "рокля";

			$input[6] = $main_data;
			$input[6]["link"] = "http://www.blessbyzr.com/produkti/rizi?limit=1000000start=0";
			$input[6]["tags"] = "Bless, риза, златка райкова";
			$input[6]["category"] = "риза";

			$input[7] = $main_data;
			$input[7]["link"] = "http://www.blessbyzr.com/produkti/saka?limit=1000000start=0";
			$input[7]["tags"] = "Bless, сако, златка райкова";
			$input[7]["category"] = "сако";

			$input[8] = $main_data;
			$input[8]["link"] = "http://www.blessbyzr.com/produkti/topove?limit=1000000start=0";
			$input[8]["tags"] = "Bless, топ, златка райкова";
			$input[8]["category"] = "топ";

			foreach ($input as $inp) {
				$result = array_merge(get_bless($inp));
//				print_r($result);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "urbanize":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.urbanize.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.urbanize.bg";

			$input[0] = $main_data;
			$input[0]["link"] = "http://www.urbanize.bg/drehi/teniski.html?limit=60"; //?limit=60
			$input[0]["sex"] = "m";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			$input[1] = $main_data;
			$input[1]["link"] = "http://www.urbanize.bg/drehi/bluzi-kachulki.html?limit=60"; //?limit=60
			$input[1]["sex"] = "m";
			$input[1]["tags"] = "urbanize, блуза, суичър";
			$input[1]["category"] = "суичър";

			$input[2] = $main_data;
			$input[2]["link"] = "http://www.urbanize.bg/drehi/iaketa.html?limit=60"; //?limit=60
			$input[2]["sex"] = "m";
			$input[2]["tags"] = "urbanize, яке";
			$input[2]["category"] = "яке";

			$input[3] = $main_data;
			$input[3]["link"] = "http://www.urbanize.bg/drehi/pantaloni-bermudi.html?limit=60"; //?limit=60
			$input[3]["sex"] = "m";
			$input[3]["tags"] = "urbanize, панталони";
			$input[3]["category"] = "панталони";


			$input[4] = $main_data;
			$input[4]["link"] = "http://www.urbanize.bg/drehi/bermudi.html?limit=60"; //?limit=60
			$input[4]["sex"] = "m";
			$input[4]["tags"] = "urbanize, панталони, къси панталони, шорти";
			$input[4]["category"] = "панталони";

			$input[5] = $main_data;
			$input[5]["link"] = "http://www.urbanize.bg/drehi/rizi.html?limit=60"; //?limit=60
			$input[5]["sex"] = "m";
			$input[5]["tags"] = "urbanize, риза";
			$input[5]["category"] = "риза";

			$input[6] = $main_data;
			$input[6]["link"] = "http://www.urbanize.bg/obuvki/mujki-obuvki.html?limit=60"; //?limit=60
			$input[6]["sex"] = "m";
			$input[6]["tags"] = "urbanize, обувки, мъжки обувки";
			$input[6]["category"] = "обувки";

			$input[7] = $main_data;
			$input[7]["link"] = "http://www.urbanize.bg/obuvki/damski-obuvki.html?limit=60"; //?limit=60
			$input[7]["sex"] = "f";
			$input[7]["tags"] = "urbanize, обувки, дамски обувки, женски обувки";
			$input[7]["category"] = "обувки";

			foreach ($input as $inp) {
				$result = get_urbanize($inp);
//				print_r($result);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "koketna":
			$input = array();
			$temp_input = array();
			$main_data = array();
			$main_data["base"] = "http://www.koketna.com";
			$main_data["brand"] = "koketna";
			$main_data["sex"] = "f";
			$main_data["users_selectorFK"] = '1z13yhz';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.koketna.com";

			//MYTODO: kokenta must become koketna in posts tags and scrape tags !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/visoki-obuvki";
			$temp_input["tags"] = "koketna, овувки, висок ток, токчета, дамски обувки, високи обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/sportni-oblekla/bustieta-sutieni";
			$temp_input["tags"] = "koketna, сутиен, бюстие, спортне сутиен, спортно бюстие";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/vruhni-oblekla/saka";
			$temp_input["tags"] = "koketna, сако, дамско сако";
			$temp_input["category"] = "сако";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/boti";
			$temp_input["tags"] = "koketna, овувки, боти, дамски боти, дамски обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/botushi";
			$temp_input["tags"] = "koketna, овувки, ботуши, дамски обувки, дамски ботуши";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/obuvki-na-platforma";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, обувки на платформа";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);


			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/obuvki-ot-estestvena-koja";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, естественна кожа";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/ketsove";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, кецове, дамски кецове";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/maratonki";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, маратонки, дамски маратонки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/niski-obuvki";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, ниски обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/sandali";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, сандали, дамски сандали, ниски сандали";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/sandali-na-visok-tok";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, сандали, дамски сандали, високи сандали, сандали на ток";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/espadrili";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, еспадрили, дамски еспадрили";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/sandali-na-platforma";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, сандали, дамски сандали, сандали на платформа";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/chehli-djapanki";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, чехли, дамски чехли, джапанки, дамски джапанки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damski-obuvki/chehli-djapanki";
			$temp_input["tags"] = "koketna, овувки, дамски обувки, чехли, дамски чехли, джапанки, дамски джапанки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/sportni-oblekla/suitchuri";
			$temp_input["tags"] = "koketna, суичър, дамски суичър";
			$temp_input["category"] = "суичър";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/sportni-oblekla/suitchuri";
			$temp_input["tags"] = "koketna, суичър, дамски суичър";
			$temp_input["category"] = "суичър";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/sportni-oblekla/sportni-pantaloni";
			$temp_input["tags"] = "koketna, панталони, дамски панталони, спортни панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/rokli/parti-rokli";
			$temp_input["tags"] = "koketna, рокля, официална рокля, парти рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_koketna($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;

		case "koketna_1":
			$input = array();
			$temp_input = array();
			$main_data = array();
			$main_data["base"] = "http://www.koketna.com";
			$main_data["brand"] = "koketna";
			$main_data["sex"] = "f";
			$main_data["users_selectorFK"] = '1z13yhz';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.koketna.com";

			$temp_input["link"] = "http://www.koketna.com/ezhednevni-rokli"; // 2000
			$temp_input["tags"] = "koketna, рокля, ежедневна рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_koketna($inp);

				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "koketna_2":
			$input = array();
			$temp_input = array();
			$main_data = array();
			$main_data["base"] = "http://www.koketna.com";
			$main_data["brand"] = "koketna";
			$main_data["sex"] = "f";
			$main_data["users_selectorFK"] = '1z13yhz';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.koketna.com";

			$temp_input["link"] = "http://www.koketna.com/kostyumi/pola-i-sako";
			$temp_input["tags"] = "koketna, костюм, дамски костюм, сако, пола";
			$temp_input["category"] = "костюм";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/kostyumi/pola-i-sako";
			$temp_input["tags"] = "koketna, костюм, дамски костюм, сако, пола";
			$temp_input["category"] = "костюм";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/kostyumi/pantalon-i-sako";
			$temp_input["tags"] = "koketna, костюм, дамски костюм, сако, панталони";
			$temp_input["category"] = "костюм";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/dynki-i-pantaloni/danki";
			$temp_input["tags"] = "koketna, панталони, дънки, дамски панталони, дамски панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/dynki-i-pantaloni/dulgi-pantaloni";
			$temp_input["tags"] = "koketna, панталони, дамски панталони, дълги панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/dynki-i-pantaloni/klinove-1";
			$temp_input["tags"] = "koketna, клин";
			$temp_input["category"] = "клин";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/dynki-i-pantaloni/gashterizoni-1";
			$temp_input["tags"] = "koketna, гащеризон, дамски гащеризон";
			$temp_input["category"] = "гащеризон";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/dynki-i-pantaloni/kysi-3-4-i-7-8-pantaloni";
			$temp_input["tags"] = "koketna, панталони, дамски панталони, къси панталони, шорти, дамски шорти";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/dynki-i-pantaloni/kysi-3-4-i-7-8-pantaloni";
			$temp_input["tags"] = "koketna, панталони, дамски панталони, къси панталони, шорти, дамски шорти";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/vruhni-oblekla/palta";
			$temp_input["tags"] = "koketna, палто, дамско палто, манто, дамско манто";
			$temp_input["category"] = "палто";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/vruhni-oblekla/shliferi";
			$temp_input["tags"] = "koketna, шлифер, дамски шлифер";
			$temp_input["category"] = "палто";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/vruhni-oblekla/jaketa";
			$temp_input["tags"] = "koketna, яке, дамско яке";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/vruhni-oblekla/saka";
			$temp_input["tags"] = "koketna, сако, дамско сако";
			$temp_input["category"] = "сако";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/vruhni-oblekla/eleci";
			$temp_input["tags"] = "koketna, жилетка, елек, дамска жилетка, дамски елек";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/vruhni-oblekla/bolera";
			$temp_input["tags"] = "koketna, жилетка, елек, дамска жилетка, дамски елек, болеро";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/vruhni-oblekla/bolera";
			$temp_input["tags"] = "koketna, жилетка, елек, дамска жилетка, дамски елек, болеро";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_koketna($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "koketna_3":
			$input = array();
			$temp_input = array();
			$main_data = array();
			$main_data["base"] = "http://www.koketna.com";
			$main_data["brand"] = "koketna";
			$main_data["sex"] = "f";
			$main_data["users_selectorFK"] = '1z13yhz';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.koketna.com";

			$temp_input["link"] = "http://www.koketna.com/bluzi-i-rizi/bluzi";
			$temp_input["tags"] = "koketna, блуза, дамска блуза, блуза с дълъг ръкав";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/bluzi-i-rizi/rizi1";
			$temp_input["tags"] = "koketna, риза, дамска риза";
			$temp_input["category"] = "риза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/bluzi-i-rizi/polouveri";
			$temp_input["tags"] = "koketna, пуловер, дамски пуловер";
			$temp_input["category"] = "пуловер";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/bluzi-i-rizi/jiletki";
			$temp_input["tags"] = "koketna, жилетка, дамска жилетка";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/bluzi-i-rizi/tuniki";
			$temp_input["tags"] = "koketna, туника";
			$temp_input["category"] = "туника";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/bluzi-i-rizi/bluzi-s-kas-rakav";
			$temp_input["tags"] = "koketna, блуза, дамска блуза, блуза с къс ръкав";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_koketna($inp);

				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "koketna_4":
			$input = array();
			$temp_input = array();
			$main_data = array();
			$main_data["base"] = "http://www.koketna.com";
			$main_data["brand"] = "koketna";
			$main_data["sex"] = "f";
			$main_data["users_selectorFK"] = '1z13yhz';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.koketna.com";

			$temp_input["link"] = "http://www.koketna.com/bluzi-i-rizi/teniski";
			$temp_input["tags"] = "koketna, блуза, дамска блуза, тениска, дамска тениска";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/bluzi-i-rizi/topove";
			$temp_input["tags"] = "koketna, топ";
			$temp_input["category"] = "топ";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/poli/kusi-poli";
			$temp_input["tags"] = "koketna, пола, къса пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/poli/sredni-poli";
			$temp_input["tags"] = "koketna, пола, средна пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/poli/dulgi-poli";
			$temp_input["tags"] = "koketna, пола, дълга пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_koketna($inp);

				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "koketna_5":
			$input = array();
			$temp_input = array();
			$main_data = array();
			$main_data["base"] = "http://www.koketna.com";
			$main_data["brand"] = "koketna";
			$main_data["sex"] = "f";
			$main_data["users_selectorFK"] = '1z13yhz';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.koketna.com";

			$temp_input["link"] = "http://www.koketna.com/damsko-belio/komplekti";
			$temp_input["tags"] = "koketna, бельо, дамско бельо, комплект";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damsko-belio/sutieni";
			$temp_input["tags"] = "koketna, сутиен, дамско бельо";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damsko-belio/stiagashto-belio";
			$temp_input["tags"] = "koketna, бельо, стягащо бельо, дамско бельо";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damsko-belio/bikini-prashki-bokserki";
			$temp_input["tags"] = "koketna, бельо, бикини, прашки, боксерки, дамско бельо";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damsko-belio/chorapi-chorapogasthi";
			$temp_input["tags"] = "koketna, бельо, чорапи, чорапогащи, дамско бельо";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damsko-belio/nosthnici-pijami";
			$temp_input["tags"] = "koketna, пижама, нощница, дамска пижама, дамска нощница";
			$temp_input["category"] = "пижама";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damsko-belio/bodita";
			$temp_input["tags"] = "koketna, боди";
			$temp_input["category"] = "боди";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/damsko-belio/potnitsi";
			$temp_input["tags"] = "koketna, потник, дамски потник";
			$temp_input["category"] = "потник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/banski-kostiumi/banski-polovinki";
			$temp_input["tags"] = "koketna, бански, бански половинки, дамски бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/banski-kostiumi/cial-banski";
			$temp_input["tags"] = "koketna, бански, цял бански, дамски бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/accessories/chanti";
			$temp_input["tags"] = "koketna, чанта, портмоне, дамска чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/accessories/shapki-ushanki";
			$temp_input["tags"] = "koketna, шапка, дамска шапка";
			$temp_input["category"] = "шапка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/accessories/shalove";
			$temp_input["tags"] = "koketna, шал, дамски шал";
			$temp_input["category"] = "шал";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/accessories/kolani";
			$temp_input["tags"] = "koketna, колан, дамски колан";
			$temp_input["category"] = "колан";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/accessories/rukavici";
			$temp_input["tags"] = "koketna, ръкавици, дамски ръкавици";
			$temp_input["category"] = "ръкавици";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.koketna.com/bizhuta";
			$temp_input["tags"] = "koketna, бижута";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_koketna($inp);

				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "ivet":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.ivet.bg";
			$main_data["brand"] = "ivet";
			$main_data["users_selectorFK"] = '1z13yev';
			$main_data["post_currency"] = "BGN";
			$main_data["sex"] = "f";
			$main_data["domain"] = "www.ivet.bg";

			$temp_input["link"] = "http://www.ivet.eu/category/422/dalgi-rokli.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, рокля, дамска рокля, дълга рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/425/elegantni-rokli.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, рокля, дамска рокля, елегентна рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/426/ezhednevni-rokli.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, рокля, дамска рокля, ежедневна рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/198/rokli.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, рокля, дамска рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/393/tuniki.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, туника,";
			$temp_input["category"] = "туника";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/199/damski-suichari.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, суичър, дамски суичър";
			$temp_input["category"] = "суичър";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/202/puloveri-zhiletki-bluzoni.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, пуловер, жилетка, блузон, дамски пуловер, дамска жилетка, дамски блузон";
			$temp_input["category"] = "пуловер";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/200/rizi-i-bluzi.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, риза, жилетка, дамска риза, дамска жилетка";
			$temp_input["category"] = "риза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/205/yaketa-i-palta.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, яке, палто, дамско яке, дамско палто";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/203/gashterizoni-pantaloni-i-poli.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, гащеризон, дамски гащеризон, панталон, дамски панталон, пола";
			$temp_input["category"] = "гащеризон";
			$input[] = array_merge($main_data, $temp_input);


			$temp_input["link"] = "http://www.ivet.eu/category/201/teniski-i-topove.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, блуза, тениска, дамска блуза, дамска тениска";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/204/saka-eletsi-i-nametki.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, сако, дамско сако";
			$temp_input["category"] = "сако";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/387/bluzi-za-bremenni.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, блуза, тениска, дамска блуза, дамска тениска, блуза за бременни, тениска за бременни";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/388/rokli-za-bremenni.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, рокля, рокля за бременни";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/389/suichari-za-bremenni.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, суичър, суичър за бременни";
			$temp_input["category"] = "суичър";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/390/tuniki-za-bremenni.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, туника, туника за бременни";
			$temp_input["category"] = "туника";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/291/rokli-i-tuniki.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, туника";
			$temp_input["category"] = "туника ";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/296/topove-i-teniski.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, блуза, тениска, дамска блуза, дамска тениска";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/292/bluzi-bluzoni-puloveri.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, пуловер, дамски пуловер";
			$temp_input["category"] = "пуловер";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/295/palta-yaketa-saka-eletsi.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, палто, дамско палто";
			$temp_input["category"] = "палто";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/293/klinove-pantaloni-poli.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, клин, дамски клин";
			$temp_input["category"] = "клин";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/299/niski-obuvki.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, обувки, дамски обувки, ниски обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/300/obuvki-na-tok.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, обувки, дамски обувки, обувки на ток";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/418/obuvki-na-platforma.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, обувки, дамски обувки, обувки на платформа";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/102/bizhuta-i-aksesoari.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бижу, дамско бижу";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/304/damski-chanti.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, чанта, дамска чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/207/sutieni.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бельо, дамско бельо, сутиен";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/208/bikini-bokserki-i-prashki.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бельо, дамско бельо, бикини, прашки, боксерки, дамски боксерки";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/209/komplekti.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бельо, дамско бельо, комплекти";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/210/bodita-korseti-i-kolani.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бельо, дамско бельо, боди, корсет";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/211/tsvetni-sanishta.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, пижама, дамска пижама, нощница";
			$temp_input["category"] = "пижама";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/239/halati.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, халат, дамски халат";
			$temp_input["category"] = "халат";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/223/butikovo-damsko-belyo.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бельо, дамско бельо, бутиково бельо";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/70/erotichno-belyo.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бельо, дамско бельо, еротично бельо";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/124/korseti-i-byustieta.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бельо, дамско бельо, корсет";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/143/seksi-kostyumi.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бельо, дамско бельо, секси бельо, секси костюм";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/217/chorapi.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, чорапи, дамски чорапи";
			$temp_input["category"] = "чорапи";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/218/chorapogashtnitsi-i-klinove.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, чорапогащи";
			$temp_input["category"] = "чорапогащи";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/331/damski-shapki.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, шапка, дамска шапка";
			$temp_input["category"] = "шапка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.ivet.eu/category/162/dve-chasti.html?recordsPerPage=100&page=";
			$temp_input["tags"] = "ivet, бански, дамски бански, бански комплект, две части";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

//			$temp_input["link"] = "http://www.ivet.eu/category/169/tseli-banski.html?recordsPerPage=100&page=";
//			$temp_input["tags"] = "ivet, бански, дамски бански, цели бански";
//			$temp_input["category"] = "бански";
//			$input[] = array_merge($main_data, $temp_input);
			//http://www.ivet.eu/category/394/komplekti.html?recordsPerPage=100&page=
			//http://www.ivet.eu/category/287/sportni-stoki.html?recordsPerPage=100&page= - 


			$main_data["sex"] = "m";

			//sex m
			//http://www.ivet.eu/category/247/teniski.html?recordsPerPage=100&page=
			//http://www.ivet.eu/category/248/rizi-i-saka.html?recordsPerPage=100&page=
			//http://www.ivet.eu/category/290/pantaloni-i-danki.html?recordsPerPage=100&page=
			//http://www.ivet.eu/category/246/suichari.html?recordsPerPage=100&page=
			//http://www.ivet.eu/category/245/palta-yaketa-eletsi.html?recordsPerPage=100&page=
			//http://www.ivet.eu/category/322/mazhko-belyo.html?recordsPerPage=100&page=
			//http://www.ivet.eu/category/391/puloveri-zhiletki.html?recordsPerPage=100&page=
			//http://www.ivet.eu/category/392/bluzi-s-dalag-rakav.html?recordsPerPage=100&page=
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "ivet, ";
//			$temp_input["category"] = "блуза";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "ivet, ";
//			$temp_input["category"] = "блуза";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "ivet, ";
//			$temp_input["category"] = "блуза";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "ivet, ";
//			$temp_input["category"] = "блуза";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "ivet, ";
//			$temp_input["category"] = "блуза";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "ivet, ";
//			$temp_input["category"] = "блуза";
//			$input[] = array_merge($main_data, $temp_input);


			foreach ($input as $inp) {
				$result = get_ivet($inp);
//				die(print_r($result));
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "mymall":
			$input = array();
			$main_data = array();
			$main_data["base"] = "https://www.mymall.bg";
			$main_data["brand"] = "mymall";
			$main_data["users_selectorFK"] = '1z13ye6';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.mymall.bg";
			$main_data["sex"] = "f";

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Блузи-и-пуловери/Официални-блузи/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, блуза, дамска блуза, официална блуза";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Блузи-и-пуловери/Пуловери/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, пуловер, дамски пуловер";
			$temp_input["category"] = "пуловер";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Блузи-и-пуловери/Жилетки/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, жилетка, дамска жилетка";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Блузи-и-пуловери/Изрязани-горнища/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, топ, изрязан топ, горнище, изрязано горнище";
			$temp_input["category"] = "топ";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Къси-панталони/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, панталони, дамски панталони, къси панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Панталони/Изрязани-панталони/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, панталони, дамски панталони, изрязани панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Панталони/Официални-панталони/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, панталони, дамски панталони, официални панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Панталони/Панталони-по-тялото/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, панталони, дамски панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Панталони/Потури/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, панталони, дамски панталони, потури";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Панталони/Виж-всички-панталони/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, панталони, дамски панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Гащеризони/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, дамски гащеризон, гащеризон";
			$temp_input["category"] = "гащеризон";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Поли/Ежедневни-поли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, пола, ежедневна пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Поли/Поли-под-коляното/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, пола, пола под коляното";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Поли/Къси-поли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, пола, къса пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Поли/Макси-поли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, пола, макси пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Поли/Поли-по-тялото/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, пола, пола по тялото";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Потници/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, потник, дамски потник";
			$temp_input["category"] = "потник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Якета/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, яке, дамско яке";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Дантелени-рокли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, дантелене рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Рокли-под-коляно/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, рокля под коляното";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Къси-рокли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, къса рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Ежедневни-рокли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, ежедневна рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Официални-рокли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, официална рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Вечерни-рокли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, вечерна рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Рокли-без-презрамки/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, рокля без презрамки";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Макси-рокли/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, макси, макси рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Рокли/Рокли-по-тялото/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, рокля, рокля по тялото";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Ризи/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, риза, дамска риза";
			$temp_input["category"] = "риза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Суичъри/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, суичър, дамски суичър";
			$temp_input["category"] = "суичър";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Дънки/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Тениски/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, тениска, дамска тениска";
			$temp_input["category"] = "тениска";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Грейки/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, яке, дамско яке, грейка, дамска грейка";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Чорапи/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, чорапи, дамски чорапи";
			$temp_input["category"] = "чорапи";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Анцузи/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, анцунг, дамски анцунг";
			$temp_input["category"] = "анцунг";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Клинове/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, клин";
			$temp_input["category"] = "клин";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Пижами/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, пижама, дамска пижама";
			$temp_input["category"] = "пижама";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/Бикини/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, дамско бельо, бикини";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/Боди/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, боди, дамско бельо";
			$temp_input["category"] = "боди";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/Боксерки/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, дамско бельо, боксерки, дамски боксерки";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/Жартиери/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, дамско бельо, жартиери";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/Корсети/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, дамско бельо, корсет";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/Стринг-%28Прашки%29/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, дамско бельо, прашки";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/Сутиени/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, дамско бельо, сутиен";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/Нощтници/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, нощница, пижама, дамска пижама";
			$temp_input["category"] = "пижама";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бански/Бикини-и-танкини/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, дамско бельо, бикини";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бельо/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бельо, дамско бельо";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бански/Цели-бански/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, банси, дамски бански, цели бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бански/Долнища/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бански, дамски бански, бански долнища";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бански/Горнища/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бански, дамски бански, бански горнище";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Облекло/Бански/Други-бански/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бански, дамски бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Маратонки/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, спортни обувки, маратонки, дамски маратонки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Сандали-и-чехли/Сандали-с-висок-ток/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, сандали, дамски сандали, сандали с висок ток";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Сандали-и-чехли/Сандали-на-платформа/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, сандали, дамски сандали, сандали на платформа";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Сандали-и-чехли/Ниски-сандали/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, сандали, дамски сандали, ниски сандали";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Кецове-и-гуменки/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, кецове, дамски кецове, гуменки, дамски гуменки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Обувки-за-ходене/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, спортни обувки, обувки за ходене, туристически обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Пантофи/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, пантофи";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Гумени-ботуши/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, ботуши, дамски ботуши, гумени ботуши";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Обувки/Ботуши/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, обувки, дамски обувки, ботуши, дамски ботуши";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Жени/Аксесоари/Дамски-чанти/Виж-всички/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, чанта, дамска чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-часовници/Дизайнерски/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, часовник, дамски часовник, дизайнерски часовник";
			$temp_input["category"] = "часовник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-часовници/Спортни/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, часовник, дамски часовник, спортен часовник";
			$temp_input["category"] = "часовник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-часовници/Луксозни/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, часовник, дамски часовник, луксозен часовник";
			$temp_input["category"] = "часовник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-часовници/Класически/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, часовник, дамски часовник, класически часовник";
			$temp_input["category"] = "часовник";
			$input[] = array_merge($main_data, $temp_input);
			$temp_input["link"] = "";

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-часовници/Електронни/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, часовник, дамски часовник, електронен часовник";
			$temp_input["category"] = "часовник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-бижута/Висулки/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бижута, дамско бижута, висулка";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-бижута/Гривни/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бижута, дамски бижута, гривна";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-бижута/Колиета-и-огърлици/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бижута, дамски бижута, колие, огърлица";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-бижута/Обеци/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бижута, дамски бижута, обеци";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.mymall.bg/categories/Бижута-и-часовници/Дамски-бижута/Пръстени/?sort=bestselling&page=";
			$temp_input["tags"] = "mymall, бижута, дамски бижута, пръстен";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$main_data["sex"] = "m";

			//https://www.mymall.bg/categories/Мъже/Облекло/Якета/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Блузи-и-пуловери/Блузи/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Блузи-и-пуловери/Жилетки/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Блузи-и-пуловери/Пуловери/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Грейки/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Суичъри/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Ризи/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Тениски/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Потници/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Анцунзи/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Пижами/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Панталони/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Спортни-долнища/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Дънки/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Къси-панталони/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Бански/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Бельо/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Облекло/Чорапи/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Обувки/Маратонки/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Обувки/Кецове-и-платненки/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Обувки/Ботуши-и-боти/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Обувки/Гумени-ботуши/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Мъже/Обувки/Сандали-и-чехли/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-часовници/Електронни/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-часовници/Дизайнерски/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-часовници/Спортни/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-часовници/Луксозни/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-часовници/Класически/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-бижута/Гривни/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-бижута/Колиета/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-бижута/Ръкавели/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-бижута/Пръстени/?sort=bestselling&page=
			//https://www.mymall.bg/categories/Бижута-и-часовници/Мъжки-бижута/Обеци/?sort=bestselling&page=
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "mymall, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "mymall, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "mymall, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "mymall, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "mymall, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//			
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "mymall, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_mymall($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "fashiondays":
			$input = array();
			$main_data = array();
			$main_data["base"] = "https://www.fashiondays.bg";
			$main_data["brand"] = "fashiondays";
			$main_data["users_selectorFK"] = '1z13y9q';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.fashiondays.bg";
			$main_data["sex"] = "f";

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Сутиени?page=";
			$temp_input["tags"] = "fashiondays, бельо, дамско бельо, сутиен";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Бикини?page=";
			$temp_input["tags"] = "fashiondays, бельо, дамско бельо, бикини";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Блузи?page=";
			$temp_input["tags"] = "fashiondays, блуза, дамска блуза";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Гащеризони?page=";
			$temp_input["tags"] = "fashiondays, гащеризон, дамски гащеризон";
			$temp_input["category"] = "гащеризон";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Домашно_облекло?page=";
			$temp_input["tags"] = "fashiondays, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Дънки?page=";
			$temp_input["tags"] = "fashiondays, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Къси_панталони_и_спортни_шорти?page=";
			$temp_input["tags"] = "fashiondays, панталони, дамски панталони, шорти, дамски шорти, къси панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Поли?page=";
			$temp_input["tags"] = "fashiondays, пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Ризи?page=";
			$temp_input["tags"] = "fashiondays, риза, дамска риза";
			$temp_input["category"] = "риза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Рокли?page=";
			$temp_input["tags"] = "fashiondays, рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Сака?page=";
			$temp_input["tags"] = "fashiondays, сако, дамско сако";
			$temp_input["category"] = "сако";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Сутиени?page=";
			$temp_input["tags"] = "fashiondays, бельо, дамско бельо, сутиен";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Тениски?page=";
			$temp_input["tags"] = "fashiondays, тениска, дамска тениска";
			$temp_input["category"] = "тениска";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Топове?page=";
			$temp_input["tags"] = "fashiondays, топ";
			$temp_input["category"] = "топ";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Комплекти_бански?page=";
			$temp_input["tags"] = "fashiondays, бански, дамски бански";
			$temp_input["category"] = "бански";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Якета?page=";
			$temp_input["tags"] = "fashiondays, яке, дамско яке";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Дрехи-Чорапи_и_чорапогащници?page=";
			$temp_input["tags"] = "fashiondays, чорапи, дамски чорапи";
			$temp_input["category"] = "чорапи";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Обувки-Сандали?page=";
			$temp_input["tags"] = "fashiondays, обувки, дамски обувки, сандали, дамски сандали";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Обувки-Спортни_обувки?page=";
			$temp_input["tags"] = "fashiondays, обувки, дамски обувки, спортни обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Обувки-Спортно_елегантни_обувки?page=";
			$temp_input["tags"] = "fashiondays, обувки, дамски обувки, спортни обувки, спортно-елегантни обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Обувки-Домашни_пантофи_и_плажни_чехли?page=";
			$temp_input["tags"] = "fashiondays, обувки, дамски обувки, чехли, дамски чехли, плажни чехли, джапанки, дамски джапанки, пантофи, дамски пантофи";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Аксесоари-Бижута?page=";
			$temp_input["tags"] = "fashiondays, бижута, дамски бижута";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Аксесоари-Колани?page=";
			$temp_input["tags"] = "fashiondays, колан, дамски колан";
			$temp_input["category"] = "колан";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Аксесоари-Слънчеви_очила?page=";
			$temp_input["tags"] = "fashiondays, очила, дамски очила, слънчеви очила";
			$temp_input["category"] = "слънчеви очила";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Аксесоари-Часовници?page=";
			$temp_input["tags"] = "fashiondays, часовник, дамски часовник";
			$temp_input["category"] = "часовник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Аксесоари-Шалове?page=";
			$temp_input["tags"] = "fashiondays, шал, дамски шал";
			$temp_input["category"] = "шал";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Аксесоари-Шапки_с_периферия_и_с_козирка?page=";
			$temp_input["tags"] = "fashiondays, шапка, дамска шапка";
			$temp_input["category"] = "шапка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Чанти-Клъчове?page=";
			$temp_input["tags"] = "fashiondays, чанта, дамска чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Чанти-Раници?page=";
			$temp_input["tags"] = "fashiondays, чанта, дамска чанта, раница, дамска раница";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Чанти-Ръчни_чанти?page=";
			$temp_input["tags"] = "fashiondays, чанта, дамска чанта, ръчна чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Чанти-Чанти_за_рамо?page=";
			$temp_input["tags"] = "fashiondays, чанта, дамска чанта, чанта за рамо";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Чанти-Чанти_през_рамо?page=";
			$temp_input["tags"] = "fashiondays, чанта, дамска чанта, чанта за рамо";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Чанти-Чанти_тип_пазарски?page=";
			$temp_input["tags"] = "fashiondays, чанта, дамска чанта, пазарска чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "https://www.fashiondays.bg/g/Жени-/Чанти-Плажни_чанти";
			$temp_input["tags"] = "fashiondays, чанта, дамска чанта, плажна чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$main_data["sex"] = "m";
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Блузи?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Дънки?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Елеци?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Комплекти_бански?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Къси_панталони_и_спортни_шорти?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Мъжко_бельо?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Панталони?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Ризи?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Сака?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Суитшърти_с_качулка?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Тениски?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Чорапи?page=
			//https://www.fashiondays.bg/g/Мъже-/Дрехи-Якета?page=
			//
			//https://www.fashiondays.bg/g/Мъже-/Обувки-Домашни_пантофи_и_плажни_чехли?page=
			//https://www.fashiondays.bg/g/Мъже-/Обувки-Мокасини?page=
			//https://www.fashiondays.bg/g/Мъже-/Обувки-Сандали?page=
			//https://www.fashiondays.bg/g/Мъже-/Обувки-Спортни_обувки?page=
			//https://www.fashiondays.bg/g/Мъже-/Обувки-Спортно_елегантни_обувки?page=
			//
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Колани?page=
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Папийонки_и_вратовръзки?page=
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Портфейли_и_ключодържатели?page=
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Раници?page=
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Слънчеви_очила?page=
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Спортни_чанти?page=
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Чанти?page=
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Шалове?page=
			//https://www.fashiondays.bg/g/Мъже-/Аксесоари-Шапки_с_периферия_и_с_козирка?page=
			//https://www.fashiondays.bg/g/Мъже-/Часовници-Автоматични_часовници?page=
			//https://www.fashiondays.bg/g/Мъже-/Часовници-Кварцови_часовници?page=
			//https://www.fashiondays.bg/g/Мъже-/Часовници-Смарт_часовници?page=
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);
//
//			$temp_input["link"] = "";
//			$temp_input["tags"] = "fashiondays, ";
//			$temp_input["category"] = "";
//			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_fashindays($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "terranova":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.terranovastyle.com";
			$main_data["brand"] = "terranova";
			$main_data["users_selectorFK"] = '1z13y8h';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.terranovastyle.com";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/t-shirts?p=";
			$temp_input["tags"] = "terranova, тенска, дамска тениска";
			$temp_input["category"] = "тениска";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/underwear/woman-collection/bras?p=";
			$temp_input["tags"] = "terranova, бельо, дамско бельо, сутиен";
			$temp_input["category"] = "сутиен";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/underwear/woman-collection/tops?p=";
			$temp_input["tags"] = "terranova, бельо, дамско бельо, горнище";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/underwear/woman-collection/socks-tights?p=";
			$temp_input["tags"] = "terranova, чорапи, дамски чорапи";
			$temp_input["category"] = "чорапи";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/underwear/woman-collection/knickers?p=";
			$temp_input["tags"] = "terranova, бельо, дамско бельо, бикини";
			$temp_input["category"] = "бельо";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/underwear/woman-collection/nightwear?p=";
			$temp_input["tags"] = "terranova, пижама, дамска пижама, нощница";
			$temp_input["category"] = "пижама";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/long-sleeve-t-shirts?p=";
			$temp_input["tags"] = "terranova, блуза, дамска блуза";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/t-shirts?p=";
			$temp_input["tags"] = "terranova, тениска, дамска тениска";
			$temp_input["category"] = "тениска";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/sweatshirts?p=";
			$temp_input["tags"] = "terranova, суичър, дамски суичър";
			$temp_input["category"] = "суичър";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/dresses?p=";
			$temp_input["tags"] = "terranova, рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/jeans?p=";
			$temp_input["tags"] = "terranova, дънки, дамски дънки, панталони, дамси панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/trousers?p=";
			$temp_input["tags"] = "terranova, панталони, дамски панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/skirts?p=";
			$temp_input["tags"] = "terranova, пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/coats-jackets?p=";
			$temp_input["tags"] = "terranova, яке, дамско яке, палто, дамско палто";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/knitwear?p=";
			$temp_input["tags"] = "terranova, пуловер, дамски пуловер, жилетка, дамска жилетка";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/shirts?p=";
			$temp_input["tags"] = "terranova, риза, дамска риза";
			$temp_input["category"] = "риза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/tops?p=";
			$temp_input["tags"] = "terranova, топ";
			$temp_input["category"] = "топ";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/leggings?p=";
			$temp_input["tags"] = "terranova, клин, дамски клин";
			$temp_input["category"] = "клин";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/joggers?p=";
			$temp_input["tags"] = "terranova, панталон, дамски панталон, спортен панталон";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/shoes?p=";
			$temp_input["tags"] = "terranova, обувки, дамски обувки";
			$temp_input["category"] = "обувки";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/woman/new-collection/jewellery?p=";
			$temp_input["tags"] = "terranova, бижута, дамски бижута";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);


			$main_data["sex"] = "m";

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/coats-jackets?p=";
			$temp_input["tags"] = "terranova, яке, мъжко яке, палто, мъжко палто";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/t-shirts?p=";
			$temp_input["tags"] = "terranova, тениска, мъжка тениска";
			$temp_input["category"] = "тениска";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/long-sleeve-t-shirts?p=";
			$temp_input["tags"] = "terranova, блуза, тениска с дълъг ръкав, мъжка блуза";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/sweatshirts?p=";
			$temp_input["tags"] = "terranova, суичър, мъжки суичър";
			$temp_input["category"] = "суичър";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/knitwear?p=";
			$temp_input["tags"] = "terranova, пуловер, мъжки пуловер";
			$temp_input["category"] = "пуловер";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/shirts?p=";
			$temp_input["tags"] = "terranova, тениска, мъжка тениска";
			$temp_input["category"] = "тениска";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/tops?p=";
			$temp_input["tags"] = "terranova, потник, мъжки потник";
			$temp_input["category"] = "потник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/trousers?p=";
			$temp_input["tags"] = "terranova, панталони, мъжки панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/jeans?p=";
			$temp_input["tags"] = "terranova, дънки, мъжки дънки, панталони, мъжки панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://www.terranovastyle.com/bg_en/man/new-collection/joggers?p=";
			$temp_input["tags"] = "terranova, панталони, мъжки панталони, спортни панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_terranova($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "alexandra":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://e-alexandra.com";
			$main_data["brand"] = "alexandra";
			$main_data["users_selectorFK"] = '1z13y62';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "e-alexandra.com";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://e-alexandra.com/дрехи/блузи.htm?page=";
			$temp_input["tags"] = "alexandra, блуза, дамска блуза";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/дънки.htm?page=";
			$temp_input["tags"] = "alexandra, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/клинове.htm?page=";
			$temp_input["tags"] = "alexandra, клин, дамски клин";
			$temp_input["category"] = "клин";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/панталони.htm?page=";
			$temp_input["tags"] = "alexandra, панталони, дамски панталони";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/поли.htm?page=";
			$temp_input["tags"] = "alexandra, пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/ризи.htm?page=";
			$temp_input["tags"] = "alexandra, риза, дамска риза";
			$temp_input["category"] = "риза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/рокли.htm?page=";
			$temp_input["tags"] = "alexandra, рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/сака.htm?page=";
			$temp_input["tags"] = "alexandra, сако, дамско сако";
			$temp_input["category"] = "сако";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/потници.htm?page=";
			$temp_input["tags"] = "alexandra, потник, дамски потник";
			$temp_input["category"] = "потник";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://e-alexandra.com/дрехи/якета.htm?page=";
			$temp_input["tags"] = "alexandra, яке, дамско яке";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_alexandra($inp);
//				die(print_r($result));
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "shopping_terapia":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://shopping-terapia.com";
			$main_data["brand"] = "shopping-terapia";
			$main_data["users_selectorFK"] = '1z13xv5';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "shopping-terapia.com";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://shopping-terapia.com/jeni-33/rokli-tuniki-37/page_";
			$temp_input["tags"] = "shopping-terapia, рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/jeni-33/bluzi-teniski-34/page_";
			$temp_input["tags"] = "shopping-terapia, блуза, дамска блуза";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/jeni-33/poli-47/page_";
			$temp_input["tags"] = "shopping-terapia, пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/jeni-33/yaketa-palta-40/page_";
			$temp_input["tags"] = "shopping-terapia, палто, дамско палто";
			$temp_input["category"] = "яке";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/jeni-33/pantaloni-dynki-55/page_";
			$temp_input["tags"] = "shopping-terapia, панталони, дамски панталони, дънки, дамски дънки";
			$temp_input["category"] = "панталони";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/jeni-33/klinove-80/page_";
			$temp_input["tags"] = "shopping-terapia, клин";
			$temp_input["category"] = "клин";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/aksesoari-27/obitzi-43/page_";
			$temp_input["tags"] = "shopping-terapia, обеци, бужута, дамски бижута";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/aksesoari-27/kolieta-29/page_";
			$temp_input["tags"] = "shopping-terapia, бижута, дамски бижута, колие";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/aksesoari-27/grivni-28/page_";
			$temp_input["tags"] = "shopping-terapia, бижута, дамски бижута, гривна";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/aksesoari-27/prysteni-99/page_";
			$temp_input["tags"] = "shopping-terapia, бижута, дамски бижута, пръстен, дамски пръстен";
			$temp_input["category"] = "бижута";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/aksesoari-27/ochila-125/page_";
			$temp_input["tags"] = "shopping-terapia, очила, слънчеви очила, дамски очила, дамски слънчеви очила";
			$temp_input["category"] = "слънчеви очила";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/aksesoari-27/shalove-shapki-42/page_";
			$temp_input["tags"] = "shopping-terapia, шапка, дамска шапка";
			$temp_input["category"] = "шапка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/aksesoari-27/kolani-41/page_";
			$temp_input["tags"] = "shopping-terapia, колан, дамски колан";
			$temp_input["category"] = "колан";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://shopping-terapia.com/aksesoari-27/chanti-93/page_";
			$temp_input["tags"] = "shopping-terapia, чанта, дамска чанта";
			$temp_input["category"] = "чанта";
			$input[] = array_merge($main_data, $temp_input);

			$main_data["sex"] = "m";

			$temp_input["link"] = "http://shopping-terapia.com/myje-106/myjki-teniski-bluzi-132/page_";
			$temp_input["tags"] = "shopping-terapia, блуза, мъжка блуза";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);

			foreach ($input as $inp) {
				$result = get_shopping_terapia($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "popolo":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://popolo.bg";
			$main_data["brand"] = "popolo";
			$main_data["users_selectorFK"] = '1z13xtl';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "popolo.bg";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://popolo.bg/c/21/0/16/#pageNumber#/поли";
			$temp_input["tags"] = "popolo, пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://popolo.bg/c/17/0/16/#pageNumber#/клинове";
			$temp_input["tags"] = "popolo, клин";
			$temp_input["category"] = "клин";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://popolo.bg/c/25/0/16/#pageNumber#/жилетки-елеци";
			$temp_input["tags"] = "popolo, жилетка, дамска жилетка";
			$temp_input["category"] = "жилетка";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "http://popolo.bg/c/16/0/16/#pageNumber#/ризи";
			$temp_input["tags"] = "popolo, риза, дамска риза";
			$temp_input["category"] = "риза";
			$input[] = array_merge($main_data, $temp_input);


			//
			//http://popolo.bg/c/24/0/16/#pageNumber#/панталони
			//http://popolo.bg/c/19/0/16/#pageNumber#/сака
			//http://popolo.bg/c/15/0/16/#pageNumber#/спортни-комплекти
			//http://popolo.bg/c/32/0/16/#pageNumber#/рокли-и-туники
			//http://popolo.bg/c/14/0/16/#pageNumber#/дънки
			//http://popolo.bg/c/22/0/16/#pageNumber#/връхно-облекло
			//http://popolo.bg/c/13/0/16/#pageNumber#/блузи-и-топове
			//http://popolo.bg/c/37/0/16/#pageNumber#/корсети-бодита
			//http://popolo.bg/c/40/0/16/#pageNumber#/танги
			//http://popolo.bg/c/38/0/16/#pageNumber#/сутиени
			//http://popolo.bg/c/41/0/16/#pageNumber#/дамски-чорапи
			//http://popolo.bg/c/39/0/16/#pageNumber#/бикини
			//http://popolo.bg/c/42/0/16/#pageNumber#/чорапогащници
			//http://popolo.bg/c/57/0/16/#pageNumber#/дамски-чанти
			//http://popolo.bg/c/28/0/16/#pageNumber#/бански-костюми-жени
			//http://popolo.bg/c/30/0/16/#pageNumber#/парео-плажни-шалове
			//http://popolo.bg/c/34/0/16/#pageNumber#/дамски-джапанки-и-чехли
			//http://popolo.bg/c/29/0/16/#pageNumber#/долнища-на-бански
			//http://popolo.bg/c/33/0/16/#pageNumber#/плажни-чанти
			//http://popolo.bg/c/52/0/16/#pageNumber#/дамски-пижами
			//http://popolo.bg/c/53/0/16/#pageNumber#/дамски-домашни-чехли-и-пантофи








			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$temp_input["link"] = "";
			$temp_input["tags"] = "popolo, ";
			$temp_input["category"] = "";
			$input[] = array_merge($main_data, $temp_input);

			$main_data["sex"] = "m";
			//http://popolo.bg/c/43/0/16/#pageNumber#/мъжки-боксери
			//http://popolo.bg/c/44/0/16/#pageNumber#/мъжки-слипове
			//http://popolo.bg/c/67/0/16/#pageNumber#/мъжки-чорапи
			//http://popolo.bg/c/80/0/16/#pageNumber#/бански-костюми-мъже
			//http://popolo.bg/c/35/0/16/#pageNumber#/мъжки-джапанки
			//http://popolo.bg/c/71/0/16/#pageNumber#/мъжки-пижами
			//http://popolo.bg/c/54/0/16/#pageNumber#/мъжки-домашни-чехли-и-пантофи

			foreach ($input as $inp) {
				$result = get_popolo($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "laleto":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.laleto.org";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.laleto.org";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://www.laleto.org/bluzi/manufacturer/results";
			$temp_input["tags"] = "laleto, блуза";
			$temp_input["category"] = "блуза";
			$input[] = array_merge($main_data, $temp_input);
			//http://www.laleto.org/rokli/manufacturer/results1-150
			//http://www.laleto.org/magazin/rizi/manufacturer/results1-150
			//http://www.laleto.org/magazin/gashterizoni/manufacturer/results1-150
			//http://www.laleto.org/magazin/plajno-obleklo/manufacturer/results1-150
			//http://www.laleto.org/poli/manufacturer/results1-150
			//http://www.laleto.org/magazin/topove/manufacturer/results1-150

			foreach ($input as $inp) {
				$result = get_laleto($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "francoferucci":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.francoferucci.eu";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.francoferucci.eu";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://www.francoferucci.eu/5-rokli?selected_filters=page-";
			$temp_input["tags"] = "franco ferucci, рокля";
			$temp_input["category"] = "рокля";
			$input[] = array_merge($main_data, $temp_input);
			//http://www.francoferucci.eu/6-pantaloni?selected_filters=page-
			//http://www.francoferucci.eu/7-damski-rizi?selected_filters=page-
			//http://www.francoferucci.eu/9-tuniki?selected_filters=page-
			//http://www.francoferucci.eu/10-bluzi?selected_filters=page-
			//http://www.francoferucci.eu/11-damski-saka?selected_filters=page-
			//http://www.francoferucci.eu/12-byustieta?selected_filters=page-
			//http://www.francoferucci.eu/13-damski-potnici?selected_filters=page-
			//http://www.francoferucci.eu/14-poli?selected_filters=page-
			//http://www.francoferucci.eu/15-yaketa?selected_filters=page-
			//http://www.francoferucci.eu/16-danki?selected_filters=page-
			//http://www.francoferucci.eu/17-obuvki?selected_filters=page-
			//http://www.francoferucci.eu/25-gashterizoni?selected_filters=page-

			$main_data["sex"] = "m";
			//http://www.francoferucci.eu/19-bluzi?selected_filters=page-
			//http://www.francoferucci.eu/20-danki?selected_filters=page-
			//http://www.francoferucci.eu/21-teniski?selected_filters=page-
			//http://www.francoferucci.eu/22-yaketa?selected_filters=page-

			foreach ($input as $inp) {
				$result = get_francoferucci($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "megz":
			//MYTODO: several currencyes - maybe gets location
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://megz.eu";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "megz.eu";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://megz.eu/dami/poli?limit=9999";
			$temp_input["tags"] = "megz, пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);
			//http://megz.eu/dami/rokli?limit=9999
			//http://megz.eu/dami/bluzi-tuniki?limit=9999
			//http://megz.eu/dami/pants-63?limit=9999
			//http://megz.eu/dami/gashterizoni?limit=9999

			foreach ($input as $inp) {
				$result = get_megz($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "sofiaoutletcenter":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.sofiaoutletcenter.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.sofiaoutletcenter.bg";
			$main_data["sex"] = "f";

			$temp_input["link"] = "http://www.sofiaoutletcenter.bg/jeni/drehi/rokli/kasi-rokli?p=";
			$temp_input["tags"] = "sofiaoutletcenter, пола";
			$temp_input["category"] = "пола";
			$input[] = array_merge($main_data, $temp_input);
			//http://www.sofiaoutletcenter.bg/jeni/drehi/rokli/dalgi-rokli?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/poli/dalgi-poli?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/bluzi/bluzi-s-kas-rakav?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/bluzi/bluzi-s-dalag-rakav?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/saka/oficialni-saka?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/saka/sportni-saka?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/danki?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/teniski?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/pantaloni/kasi-pantaloni?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/pantaloni/gashterizoni?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/pantaloni/dalgi-pantaloni?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/pletiva/zhiletki?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/pletiva/puloveri?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/rizi/riza-s-k-s-r-kav?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/rizi/riza-s-d-l-g-r-kav?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/jaketa/shliferi?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/jaketa/dankovi-jaketa?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/jaketa/puheni-jaketa?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/jaketa/eleci?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/jaketa/kozheni-jaketa?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/palta/kasi-palta?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/palta/dalgi-palta?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/suichari?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/spalni-drehi?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/belyo?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/banski?p=
			//http://www.sofiaoutletcenter.bg/jeni/drehi/top?p=
			//
			//http://www.sofiaoutletcenter.bg/jeni/obuvki/klasicheski-obuvki?p=
			//http://www.sofiaoutletcenter.bg/jeni/obuvki/sportni-obuvki?p=
			//http://www.sofiaoutletcenter.bg/jeni/obuvki/mokasini?p=
			//http://www.sofiaoutletcenter.bg/jeni/obuvki/boti?p=
			//http://www.sofiaoutletcenter.bg/jeni/obuvki/platformi?p=
			//http://www.sofiaoutletcenter.bg/jeni/obuvki/obuvki-visok-tok?p=
			//http://www.sofiaoutletcenter.bg/jeni/obuvki/obuvki-nisak-tok?p=
			//http://www.sofiaoutletcenter.bg/jeni/obuvki/ravni-obuvki?p=
			//
			//http://www.sofiaoutletcenter.bg/jeni/aksesoari/kolani?p=
			//http://www.sofiaoutletcenter.bg/jeni/aksesoari/shalove?p=
			//http://www.sofiaoutletcenter.bg/jeni/aksesoari/rakavitsi?p=
			//http://www.sofiaoutletcenter.bg/jeni/aksesoari/shapki?p=
			//http://www.sofiaoutletcenter.bg/jeni/aksesoari/ochila?p=
			//
			//http://www.sofiaoutletcenter.bg/jeni/damski-chanti/dalgi-drazhki?p=
			//http://www.sofiaoutletcenter.bg/jeni/damski-chanti/kasi-drazhki?p=
			//http://www.sofiaoutletcenter.bg/jeni/damski-chanti/klach?p=
			//http://www.sofiaoutletcenter.bg/jeni/damski-chanti/ofitsialni?p=
			//http://www.sofiaoutletcenter.bg/jeni/damski-chanti/portfeyli?p=

			$main_data["sex"] = "m";
			//http://www.sofiaoutletcenter.bg/muje/drehi/suichari?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/danki?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/palta?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/saka?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/denim?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/belyo?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/banski?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/kostyumi?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/teniski?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/rizi/rizi-s-dalag-rakav?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/rizi/rizi-s-k-s-r-kav?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/iaketa/eleci?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/iaketa/kozheni-yaketa?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/bluzi?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/puloveri?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/jiletki?p=
			//http://www.sofiaoutletcenter.bg/muje/drehi/pantaloni?p=
			//
			//http://www.sofiaoutletcenter.bg/muje/obuvki/klasicheski-obuvki?p=
			//http://www.sofiaoutletcenter.bg/muje/obuvki/sportni-obuvki?p=
			//http://www.sofiaoutletcenter.bg/muje/obuvki/boti?p=
			//http://www.sofiaoutletcenter.bg/muje/obuvki/mokasini?p=
			//http://www.sofiaoutletcenter.bg/muje/obuvki/gumenki-i-kecove?p=
			//
			//http://www.sofiaoutletcenter.bg/muje/aksesoari/shapki?p=
			//http://www.sofiaoutletcenter.bg/muje/aksesoari/shalove?p=
			//http://www.sofiaoutletcenter.bg/muje/aksesoari/vratovrazki-i-papionki?p=
			//http://www.sofiaoutletcenter.bg/muje/aksesoari/rakavitsi?p=
			//http://www.sofiaoutletcenter.bg/muje/aksesoari/grivni?p=
			//http://www.sofiaoutletcenter.bg/muje/aksesoari/ochila?p=
			//http://www.sofiaoutletcenter.bg/muje/chanti/za-laptop?p=
			//http://www.sofiaoutletcenter.bg/muje/chanti/ezhednevni?p=

			foreach ($input as $inp) {
				$result = get_sofiaoutletcenter($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "elmaira":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://elmaira.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "elmaira.bg";

			$input[0] = $main_data;
			$input[0]["link"] = "http://elmaira.bg/jeni/rokli/?limit=96&p=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_elmaira($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "ivon":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://ivon.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "ivon.bg";

			$input[0] = $main_data;
			$input[0]["link"] = "http://ivon.bg/damcko-bel-o/bikini?limit=100&page=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_ivon($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "outletzone":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://outletzone.org";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "outletzone.org";

			$input[0] = $main_data;
			$input[0]["link"] = "http://outletzone.org/index.php?route=product/category&path=61_234&limit=100&page=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_outletzone($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;



			
		case "finestini":
			//MYTODO: fix pagination
			// there is link without pages. Pages are with # which is ajax
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.finestini.com";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.finestini.com";

			$input[0] = $main_data;
			$input[0]["link"] = "http://www.finestini.com/shop/65-poli#/page-";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_finestini($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "elza":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://elza.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "elza.bg";

			$input[0] = $main_data;
			// NOTE!!! the link has a pageNumber parameter which is INSIDE the link.
			// You should replace the page number (usually 1) with the placeholder #pageNumber#
			$input[0]["link"] = "http://elza.bg/%D0%BD%D0%BE%D0%B2%D0%BE_%D0%BC%D1%8A%D0%B6%D0%BA%D0%B8_%D0%BF%D0%B0%D0%BD%D1%82%D0%B0%D0%BB%D0%BE%D0%BD%D0%B8,_%D1%88%D0%BE%D1%80%D1%82%D0%B8_%D0%B8_%D0%B1%D0%B0%D0%BD%D1%81%D0%BA%D0%B8-#pageNumber#-501.html";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_elza($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "replica_moda":
			//MYTODO: too many "sold out"
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.replica-moda.com";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.replica-moda.com";

			$input[0] = $main_data;
			$input[0]["link"] = "http://www.replica-moda.com/%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD/%D0%B4%D1%80%D0%B5%D1%85%D0%B8/%D1%80%D0%BE%D0%BA%D0%BB%D0%B8/results,1-9999.html";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_replica_moda($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "dreshki":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.dreshki.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.dreshki.bg";

			$input[0] = $main_data;
			// NOTE: The default mode of the site is 'infinite scroll'. If you add query param 'page', then it turns to paging mode.
			$input[0]["link"] = "http://www.dreshki.bg/rokli?page=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_dreshki($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "bestcode":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://bestcode.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "bestcode.bg";

			$input[0] = $main_data;
			// NOTE: With query param 'limit = 9999, practically all items will be displayed on the first page.
			$input[0]["link"] = "http://bestcode.bg/teniski?limit=9999";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_bestcode($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "simall":
			$input = array();
			$main_data = array();
			$main_data["base"] = "https://www.simall.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.simall.bg";

			$input[0] = $main_data;
			// NOTE: With query param 'f_per_page' = 9999, practically all items will be displayed on the first page.
			$input[0]["link"] = "https://www.simall.bg/%D0%B6%D0%B5%D0%BD%D0%B8/%D0%B4%D1%80%D0%B5%D1%85%D0%B8-%D0%B7%D0%B0-%D0%B6%D0%B5%D0%BD%D0%B8/%D1%82%D1%83%D0%BD%D0%B8%D0%BA%D0%B8/%D1%82%D1%83%D0%BD%D0%B8%D0%BA%D0%B8-%D0%B4%D1%8A%D0%BB%D1%8A%D0%B3-%D1%80%D1%8A%D0%BA%D0%B0%D0%B2?pg=1&f_per_page=9999";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_simall($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "rosifashion":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://shop.rosifashion.com";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "shop.rosifashion.com";

			$input[0] = $main_data;
			$input[0]["link"] = "http://shop.rosifashion.com/pantaloni/";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_rosifashion($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "frash":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.frash.eu";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.frash.eu";

			$input[0] = $main_data;
			$input[0]["link"] = "http://www.frash.eu/%D0%B6%D0%B5%D0%BD%D0%B8?limitstart=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_frash($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "dpoint":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://dpoint.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "dpoint.bg";

			$input[0] = $main_data;
			$input[0]["link"] = "http://dpoint.bg/jenski-drehi/rokli.html?limit=72&p=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_dpoint($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "clothink":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://clothink.eu";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "clothink.eu";

			$input[0] = $main_data;
			// NOTE: Setting query param 'limit' = 9999 should ensure all items are displayed on single page.
			$input[0]["link"] = "http://clothink.eu/index.php?route=product/category&path=62&limit=9999";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_clothink($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "ninoconti":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://ninoconti.com";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "ninoconti.com";

			$input[0] = $main_data;
			$input[0]["link"] = "http://ninoconti.com/bg/113-%D0%B4%D0%B0%D0%BC%D1%81%D0%BA%D0%B8-%D0%BE%D0%B1%D1%83%D0%B2%D0%BA%D0%B8?n=240&p=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_ninoconti($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "dve":
			//MYTODO: vtora upotreba
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://dve.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "dve.bg";

			$input[0] = $main_data;
			$input[0]["link"] = "http://dve.bg/Bluzi-cat2528.html?limit=140&page=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_dve($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "katonovi":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://katonovi.com";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "katonovi.com";

			$input[0] = $main_data;
			$input[0]["link"] = "http://katonovi.com/scat/1/moda/damski-drehi/bg/";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_katonovi($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "jivot":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://jivot.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "jivot.bg";

			$input[0] = $main_data;
			$input[0]["link"] = "http://jivot.bg/%D0%94%D0%B0%D0%BC%D1%81%D0%BA%D0%B8-%D0%BF%D0%BE%D0%BB%D0%B8-411?p=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_jivot($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "sportdepot":
			$input = array();
			$main_data = array();
			$main_data["base"] = "http://www.sportdepot.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.sportdepot.bg";

			$input[0] = $main_data;
			// NOTE! Don't forget to replace # with ?
			$input[0]["link"] = "http://www.sportdepot.bg/bg/jeni-obleklo/bluzi-1_35_1?showBy=200&page=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_sportdepot($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "mollydress":
			$input = array();
			$main_data = array();
			$main_data["base"] = "https://molly-dress.com";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "molly-dress.com";

			$input[0] = $main_data;
			$input[0]["link"] = "https://molly-dress.com/dresses.html?max=45&page=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_mollydress($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
		case "lily":
			$input = array();
			$main_data = array();
			$main_data["base"] = "https://www.lily.bg";
			$main_data["brand"] = "urbanize";
			$main_data["users_selectorFK"] = '1z13ykj';
			$main_data["post_currency"] = "BGN";
			$main_data["domain"] = "www.lily.bg";

			$input[0] = $main_data;
			$input[0]["link"] = "https://www.lily.bg/rokli?limit=100&page=";
			$input[0]["sex"] = "f";
			$input[0]["tags"] = "urbanize, тениска, блуза";
			$input[0]["category"] = "блуза";

			foreach ($input as $inp) {
				$result = get_lily($inp);
				$result = json_encode($result);
				echo session_browser_post($protocol . $base_url . "/scrape/scrape", array("data" => $result)) . "\n\n";
			}
			break;
	}
}
?>
