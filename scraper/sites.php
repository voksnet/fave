<?php

require_once ('HTMLParser.php');

function get_outletzone($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing outletzone " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$pagination = $document->getElementsByTagAndClassName('div', 'links');

	if (count($pagination)) {
		$pageLinks = (new HTMLParser(HTMLParser::getElementHTML($pagination[0])))->getElementsByTagName('a');
		$lastPageLink = $pageLinks[count($pageLinks) - 1];
		$numberOfPages = (int) explode('page=', HTMLParser::getElementAttribute($lastPageLink, 'href'))[1];
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = (new HTMLParser(HTMLParser::getElementHTML($document->getElementsByTagAndClassName('div', 'product-grid')[0])))->getElementsByTagAndClassName('a', 'img_conteiner');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$temp_data['url'] = HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_outletzone($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	var_dump($result);
	return;

	return $result;
}

function process_outletzone($link) {
	echo "procesing outletzone: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceHolder = $document->getElementsByTagAndClassName('span', 'price-new');
	if (count($priceHolder)) {
		$priceHolder = $priceHolder[0];
	} else {
		$priceHolder = $document->getElementsByTagAndClassName('p', 'regular-price')[0];
	}
	$imageHolder = $document->getElementsByTagAndClassName('a', 'img_conteiner')[0];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementsByTagAndClassName('div', 'product-info-short-description');

	$result["post_price"] = number_format(floatval(str_replace(',', '.',HTMLParser::getElementText($priceHolder))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= count($descriptionHolder) ? HTMLParser::getElementText($descriptionHolder[0]) : '';

	return $result;
}

function get_ivon($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing ivon " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$pagination = $document->getElementsByTagAndClassName('div', 'links');

	if (count($pagination)) {
		$pageLinks = (new HTMLParser(HTMLParser::getElementHTML($pagination[0])))->getElementsByTagName('a');
		$lastPageLink = $pageLinks[count($pageLinks) - 1];
		$numberOfPages = (int) explode('page=', HTMLParser::getElementAttribute($lastPageLink, 'href'))[1];
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndClassName('div', 'image');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$urls = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a');
			$temp_data['url'] = HTMLParser::getElementAttribute(array_pop($urls), 'href');
			$temp_data = array_merge($temp_data, process_ivon($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_ivon($link) {
	echo "procesing ivon: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceHolder = $document->getElementsByTagAndClassName('div', 'product-buy')[0];
	$imageHolder = $document->getElementById('image');
	$titleHolder = $document->getElementsByTagName('h1')[1];
	$rawDescriptionHolder = $document->getElementById('tab-description');
	if ($rawDescriptionHolder) {
		$descriptionHolder = (new HTMLParser(HTMLParser::getElementHTML($rawDescriptionHolder)))->getElementsByTagName('p') ;
	} else {
		$descriptionHolder = false;
	}

	$result["post_price"] = number_format(floatval(str_replace(',', '.',HTMLParser::getElementText($priceHolder))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= count($descriptionHolder) ? HTMLParser::getElementText($descriptionHolder[0]) : '';

	return $result;
}

function get_elmaira($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing elmaira " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$lastPageLinks = $document->getElementsByTagAndSingleClassName('a', 'i-last');

	if (count($lastPageLinks)) {
		$lastPageLink = $lastPageLinks[0];
		$numberOfPages = (int) explode('p=', HTMLParser::getElementAttribute($lastPageLink, 'href'))[1];
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('li', 'item');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$specialPriceHolders = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagAndClassName('span', 'newpriceg');
			if (count($specialPriceHolders)) {
				$priceHolder = $specialPriceHolders[0];
			} else {
				$priceHolder = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagAndSingleClassName('span', 'price')[0];
			}
			$temp_data["post_price"] = number_format(floatval(str_replace(',', '.',HTMLParser::getElementText($priceHolder))), 2);
			$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagAndClassName('a', 'product-image')[0], 'href');
			$temp_data = array_merge($temp_data, process_elmaira($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_elmaira($link) {
	echo "procesing elmaira: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$imageHolder = $document->getElementById('cloud_zoom');
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('div', 'std')[0];

	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_lily($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing lily " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$pagination = $document->getElementsByTagAndSingleClassName('div', 'pagination')[0];
	$pages = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('a');

	if (count($pages)) {
		$lastPage = $pages[count($pages) - 1];
		$lastPageLinkFragments = explode('=',HTMLParser::getElementAttribute($lastPage, 'href'));
		$numberOfPages = (int) $lastPageLinkFragments[count($lastPageLinkFragments) - 1];
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'image');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$link = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0];
			$temp_data['url'] = HTMLParser::getElementAttribute($link, 'href');
			$temp_data = array_merge($temp_data, process_lily($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_lily($link) {
	echo "procesing lily: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceHolder = $document->getElementsByTagAndSingleClassName('span', 'price-new')[0];
	$imageHolder = $document->getElementsByTagAndSingleClassName('img', 'fancybox-img')[0];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementsByTagAndSingleClassName('div', 'tab-content')[0])))->getElementsByTagName('p')[0];

	$result["post_price"] = number_format(floatval(str_replace(',', '.',HTMLParser::getElementText($priceHolder))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_mollydress($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing mollydress " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$pagination = $document->getElementById('navi');
	$pages = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('a');

	if (count($pages)) {
		$lastPage = $pages[count($pages) - 2];
		$numberOfPages = (int) HTMLParser::getElementText($lastPage);
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'item');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$link = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0];
			$temp_data['url'] = $base . HTMLParser::getElementAttribute($link, 'href');
			$temp_data = array_merge($temp_data, process_mollydress($temp_data['url'], $base));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_mollydress($link, $base) {
	echo "procesing mollydress: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceHolder = $document->getElementsByTagAndSingleClassName('span', 'cena')[0];
	$imageHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementsByTagAndSingleClassName('div', 'big-photo')[0])))->getElementsByTagName('a')[0] ;
	$titleHolder = $document->getElementsByTagName('h1')[1];
	$descriptionHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementById('description-content'))))->getElementsByTagName('p')[0];

	$result["post_price"] = number_format(floatval(HTMLParser::getElementText($priceHolder)), 2);
	$result['image_url'] = $base . HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_sportdepot($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing sportdepot " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$paginations = $document->getElementsByTagAndSingleClassName('div', 'pagination');

	if (count($paginations)) {
		$pagination = $paginations[0];
		$pages = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('li');
		$lastPage = $pages[count($pages) - 1];
		$numberOfPages = (int) HTMLParser::getElementText($lastPage);
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'product-list-img');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = $base . HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_sportdepot($temp_data['url'], $base));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_sportdepot($link, $base) {
	echo "procesing sportdepot: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$imageHolder = $document->getElementsByTagAndSingleClassName('a', 'jqzoom')[0];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('div', 'tab')[0];
	$descriptionHolderFragments = (new HTMLParser(HTMLParser::getElementHTML($descriptionHolder)))->getElementsByTagName('p');

	$result["post_price"] = number_format(floatval(xmlstr($data, 'price: ', ',')), 2);
	$result['image_url'] = $base . HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	foreach ($descriptionHolderFragments as $index => $descriptionHolderFragment) {
		if (!in_array($index, array(0, 1, count($descriptionHolderFragments) - 1))) {
			$result["comment"] .= "\n";
			$result["comment"] .= HTMLParser::getElementText($descriptionHolderFragment);
		}
	}

	return $result;
}

function get_jivot($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing jivot " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$paginations = $document->getElementsByTagAndSingleClassName('ul', 'pagination');

	if (count($paginations)) {
		$pagination = $paginations[0];
		$pages = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('li');
		$lastPage = $pages[count($pages) - 2];
		$numberOfPages = (int) HTMLParser::getElementText($lastPage);
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'product_img_link');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_jivot($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_jivot($link) {
	echo "procesing jivot: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementById('our_price_display');
	$imageHolder = $document->getElementById('bigpic');
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('div', 'short-description')[0];
	$descriptionHolderHTML = HTMLParser::getElementHTML($descriptionHolder);
	$newDescriptionHolderHTML = str_replace('</h1>', "\n\n</h1>", $descriptionHolderHTML);
	$descriptionHolder = (new HTMLParser($newDescriptionHolderHTML))->getElementsByTagAndSingleClassName('div', 'short-description')[0];

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'data-cfsrc');
	$result["comment"] = HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_katonovi($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing katonovi " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$pagination = $document->getElementById('pagination');
	$displayedPages = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('a');


	if (count($displayedPages)) {
		$lastPage = $displayedPages[count($displayedPages) - 2];
		$numberOfPages = (int) HTMLParser::getElementText($lastPage);
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'prod');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[1], 'href');
			$temp_data = array_merge($temp_data, process_katonovi($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_katonovi($link) {
	echo "procesing katonovi: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('span', 'price')[0];
	$imageHolder = $document->getElementById('zoom_03');
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementById('info');
	$descriptionHolderFragments = (new HTMLParser(HTMLParser::getElementHTML($descriptionHolder)))->getElementsByTagName('p');

	$result["post_price"] = number_format(floatval(HTMLParser::getElementText($priceContainer)), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n";
	foreach ($descriptionHolderFragments as $descriptionHolderFragment) {
		$result["comment"] .= "\n";
		$result["comment"] .= HTMLParser::getElementText($descriptionHolderFragment);
	}

	return $result;
}

function get_dve($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing dve " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$paginations = $document->getElementsByTagAndSingleClassName('div', 'pages');

	if (count($paginations)) {
		$pagination = $paginations[0];
		$lastPage = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagAndSingleClassName('a', 'next')[0];
		$lastPageLinkFragments = explode('=', HTMLParser::getElementAttribute($lastPage, 'href'));
		$numberOfPages = (int) $lastPageLinkFragments[count($lastPageLinkFragments) - 1];
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('form', 'item_form');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0], 'href');
			$temp_data = array_merge($temp_data, process_dve($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_dve($link) {
	echo "procesing dve: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('span', 'promoRed')[0];
	$priceText = HTMLParser::getElementText($priceContainer);
	$priceTextFragments = explode('лв.', $priceText);
	$imageHolder = $document->getElementsByTagAndSingleClassName('img', 'cloudzoom')[0];
	$titleHolder = $document->getElementsByTagName('h3')[0];
	$sizesHolders = $document->getElementsByTagAndSingleClassName('div', 'sizes');
	$descriptionHolders = $document->getElementsByTagAndSingleClassName('span', 'short_description');

	$result["post_price"] = number_format(floatval($priceTextFragments[count($priceTextFragments) - 2]), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = xmlstr(HTMLParser::getElementHTML($titleHolder), 'h3>', '<span') . "\n\n";
	if (count($sizesHolders)) {
		$sizeHolderFragments = (new HTMLParser(HTMLParser::getElementHTML($sizesHolders[0])))->getElementsByTagName('span');

		foreach ($sizeHolderFragments as $sizeHolderFragment) {
			$result["comment"] .= HTMLParser::getElementText($sizeHolderFragment);
			$result["comment"] .= "\n";
		}
	}

	if (count($descriptionHolders)) {
		$result["comment"] .= HTMLParser::getElementText($descriptionHolders[0]);
	}

	return $result;
}

function get_ninoconti($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing ninoconti " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$paginations = $document->getElementsByTagAndSingleClassName('ul', 'pagination');

	if (count($paginations)) {
		$pagination = $paginations[0];
		$pageListItems = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('li');
		$numberOfPages = count($pageListItems) - 2;
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'product_img_link');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_ninoconti($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_ninoconti($link) {
	echo "procesing ninoconti: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementById('our_price_display');
	$imageHolder = $document->getElementById('bigimg');
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementById('short_description_content');

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_clothink($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing clothink " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$result = array();

	$data = file_get_contents($link);

	$document = new HTMLParser($data);

	$itemHolder = $document->getElementById('content');
	$items = (new HTMLParser(HTMLParser::getElementHTML($itemHolder)))->getElementsByTagAndSingleClassName('div', 'name');

	foreach ($items as $index => $item) {
		$temp_data = array();

		$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0], 'href');
		$temp_data = array_merge($temp_data, process_clothink($temp_data['url']));
		$result[] = array_merge($temp_data, $init_data);
	}

	return $result;
}

function process_clothink($link) {
	echo "procesing clothink: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$regularPriceContainers = $document->getElementsByTagAndSingleClassName('div', 'price-regular');
	$specialPriceContainers = $document->getElementsByTagAndSingleClassName('span', 'price-new');
	if (count($regularPriceContainers)) {
		$priceContainer = $regularPriceContainers[0];
	} else {
		$priceContainer = $specialPriceContainers[0];
	}
	$imageHolder = $document->getElementById('image');
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementById('tab-description');

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_dpoint($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing dpoint " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$result = array();

	$currentPage = 0;

	do {
		$currentPage++;
		$data = file_get_contents($link . $currentPage);
		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'product-image');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_dpoint($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);

			var_dump($index);
			if ($index > 10) {
				var_dump($result);
				return;
			}
		}

		$hasNext = count($document->getElementsByTagAndSingleClassName('a', 'next'));
	} while ($hasNext);

	return $result;
}

function process_dpoint($link) {
	echo "procesing dpoint: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$productDetailsHolder = $document->getElementsByTagAndSingleClassName('div', 'price-box')[0];
	$priceContainers = (new HTMLParser(HTMLParser::getElementHTML($productDetailsHolder)))->getElementsByTagAndSingleClassName('span', 'price');
	$priceContainer = $priceContainers[count($priceContainers) - 1];
	$imageHolder = $document->getElementsByTagAndAttribute('img', 'itemprop', 'image')[0];
	$titleHolder = $document->getElementsByTagName('h1')[1];
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('div', 'std')[0];

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_sofiaoutletcenter($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing sofiaoutletcenter " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$result = array();

	$currentPage = 0;

	do {
		$currentPage++;
		$data = file_get_contents($link . $currentPage);
		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'productBoxIMG');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_sofiaoutletcenter($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}

		$hasNext = count($document->getElementsByTagAndSingleClassName('a', 'next'));
	} while ($hasNext);

	return $result;
}

function process_sofiaoutletcenter($link) {
	echo "procesing sofiaoutletcenter: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$productDetailsHolder = $document->getElementsByTagAndSingleClassName('section', 'productDetailedInfo')[0];
	$priceContainers = (new HTMLParser(HTMLParser::getElementHTML($productDetailsHolder)))->getElementsByTagAndSingleClassName('span', 'price');
	$priceContainer = $priceContainers[count($priceContainers) - 1];
	$imageHolder = $document->getElementById('preview');
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('section', 'description')[0];

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_frash($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing frash " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '0');

	$document = new HTMLParser($data);

	$pagination = $document->getElementsByTagAndSingleClassName('div', 'FlexiblePagination')[0];
	$pages = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('li');

	if (count($pages)) {
		$numberOfPages = count($pages) - 4;
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 0; $i < $numberOfPages; $i++) {
		$data = file_get_contents($link . ($i * 150));

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'FlexibleBrowseThumbProductInner');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = $base . HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0], 'href');
			$temp_data = array_merge($temp_data, process_frash($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_frash($link) {
	echo "procesing frash: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('span', 'PricesalesPrice')[0];
	$imageHolder = $document->getElementById('FlexibleMainImage');
	$titleHolder = $document->getElementsByTagAndSingleClassName('div', 'FlexibleProductDetailProductName')[0];
	$descriptionHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementsByTagAndSingleClassName('div', 'FlexibleShortDesc')[0])))->getElementsByTagName('p')[1];

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_rosifashion($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing rosifashion " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '0');

	$document = new HTMLParser($data);

	$pagination = $document->getElementsByTagAndSingleClassName('div', 'zd_pagination')[0];
	$pages = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('a');

	if (count($pages)) {
		$lastPage = $pages[count($pages) - 1];
		$lastPageLink = HTMLParser::getElementAttribute($lastPage, 'href');
		$lastPageLinkFragments = explode('/', $lastPageLink);
		$numberOfPages = ((int) $lastPageLinkFragments[count($lastPageLinkFragments) - 1]) / 8 + 1;
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 0; $i < $numberOfPages; $i++) {
		$data = file_get_contents($link . ($i * 8));

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'product_box');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0], 'href');
			$temp_data = array_merge($temp_data, process_rosifashion($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_rosifashion($link) {
	echo "procesing rosifashion: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$normalPriceContainers = $document->getElementsByTagAndClassName('span', 'price_reg_podr');
	$salePriceContainers = $document->getElementsByTagAndClassName('span', 'price_sale');
	$imageHolder = $document->getElementById('zoom1');
	$titleHolder = $document->getElementsByTagAndSingleClassName('div', 'product_title_podr')[0];
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('div', 'product_descr')[0];

	if (count($normalPriceContainers)) {
		$result["post_price"] = number_format(floatval(explode(' ', HTMLParser::getElementText($normalPriceContainers[0]))[1]), 2);
	} else {
		$result["post_price"] = number_format(floatval(HTMLParser::getElementText($salePriceContainers[0])), 2);
	}

	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_francoferucci($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing francoferucci " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$paginations = $document->getElementsByTagAndSingleClassName('ul', 'pagination');

	if (count($paginations)) {
		$pagination = $paginations[0];
		$pageListItems = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('li');
		$pageListLastItem = $pageListItems[count($pageListItems) - 2];
		$numberOfPages = (int) HTMLParser::getElementText($pageListLastItem);
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'product_img_link');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_francoferucci($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_francoferucci($link) {
	echo "procesing francoferucci: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementById('our_price_display');
	$imageHolder = $document->getElementsByTagAndAttribute('img', 'itemprop', 'image')[0];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementById('short_description_content');
	$descriptionFragments = (new HTMLParser(HTMLParser::getElementHTML($descriptionHolder)))->getElementsByTagName('p');

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n";
	for ($i = 0; $i < count($descriptionFragments); $i++) {
		$result["comment"] .= "\n";
		$result["comment"] .= HTMLParser::getElementText($descriptionFragments[$i]);
	}

	return $result;
}

function get_megz($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing megz " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$result = array();

	$data = file_get_contents($link);

	$document = new HTMLParser($data);

	$items = array_slice($document->getElementsByTagAndClassName('div', 'product'), 4);

	foreach ($items as $index => $item) {
		$temp_data = array();

		$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0], 'href');
		$temp_data = array_merge($temp_data, process_megz($temp_data['url']));
		$result[] = array_merge($temp_data, $init_data);
	}

	return $result;
}

function process_megz($link) {
	echo "procesing megz: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndAttribute('meta', 'itemprop', 'price')[0];
	$imageHolder = $document->getElementsByTagAndAttribute('meta', 'itemprop', 'image')[1];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementById('tab-description');
	$descriptionFragments = (new HTMLParser(HTMLParser::getElementHTML($descriptionHolder)))->getElementsByTagName('span');

	$result["post_price"] = number_format(floatval(HTMLParser::getElementAttribute($priceContainer, 'content')), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'content');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n";
	foreach ($descriptionFragments as $fragment) {
		$result["comment"] .= "\n";
		$result["comment"] .= HTMLParser::getElementText($fragment);
	}

	return $result;
}

function get_simall($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing simall " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$result = array();

	$data = file_get_contents($link);

	$document = new HTMLParser($data);

	$items = (new HTMLParser(HTMLParser::getElementHTML($document->getElementById('content'))))->getElementsByTagAndSingleClassName('div', 'grid_3');

	foreach ($items as $index => $item) {
		$temp_data = array();

		$temp_data['url'] = $base . HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0], 'href');
		$temp_data = array_merge($temp_data, process_simall($temp_data['url'], $base));
		$result[] = array_merge($temp_data, $init_data);
	}

	return $result;
}

function process_simall($link, $base) {
	echo "procesing bestcode: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('div', 'price')[0];
	$imageHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementsByTagAndSingleClassName('div', 'product-info')[0])))->getElementsByTagName('img')[0];
	$titleHolder = $document->getElementsByTagAndSingleClassName('h1', 'page_title')[0];
	$descriptionHolder = $document->getElementById('tab-description');
	$descriptionFragments = (new HTMLParser(HTMLParser::getElementHTML($descriptionHolder)))->getElementsByTagName('p');

	$result["post_price"] = number_format(floatval(HTMLParser::getElementText($priceContainer)), 2);
	$result['image_url'] = $base . HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n";
	foreach ($descriptionFragments as $fragment) {
		$result["comment"] .= "\n";
		$result["comment"] .= HTMLParser::getElementText($fragment);
	}

	return $result;
}

function get_bestcode($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing bestcode " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$result = array();

	$data = file_get_contents($link);

	$document = new HTMLParser($data);

	$items = $document->getElementsByTagAndSingleClassName('div', 'product-thumb__primary');

	foreach ($items as $index => $item) {
		$temp_data = array();

		$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0], 'href');
		$temp_data = array_merge($temp_data, process_bestcode($temp_data['url'], $base));
		$result[] = array_merge($temp_data, $init_data);
	}

	return $result;
}

function process_bestcode($link) {
	echo "procesing bestcode: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('li', 'product-price')[0];
	$imageHolder = $document->getElementById('main-image');
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementById('tab-description');

	$result["post_price"] = number_format(floatval(HTMLParser::getElementText($priceContainer)), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_dreshki($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing dreshki " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$pager = $document->getElementsByTagAndSingleClassName('div', 'pager')[0];
	$links = (new HTMLParser(HTMLParser::getElementHTML($pager)))->getElementsByTagName('a');
	$numberOfLinks = count($links);

	if ($numberOfLinks) {
		$numberOfPages = $numberOfLinks;
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {

		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'product-quick-view');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = $base . HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_dreshki($temp_data['url'], $base));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_dreshki($link) {
	echo "procesing dreshki: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('span', 'price-value')[0];
	$imageHolder = $document->getElementsByTagAndSingleClassName('a', 'product-image-link')[0];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$metaTags = $document->getElementsByTagName('meta');

	$result["post_price"] = number_format(floatval(HTMLParser::getElementText($priceContainer)), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementAttribute($metaTags[17], 'content');

	return $result;
}

function get_replica_moda($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing replica moda " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$numberOfPages = 1;

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'img-wrapper');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$linkHolder = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0];

			$temp_data['url'] = $base . HTMLParser::getElementAttribute($linkHolder, 'href');
			$temp_data = array_merge($temp_data, process_replica_moda($temp_data['url'], $base));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_replica_moda($link, $base) {
	echo "procesing replica moda: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('span', 'PricesalesPrice')[0];
	$imageHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementsByTagAndSingleClassName('div', 'main-image')[0])))->getElementsByTagName('img')[0];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementsByTagAndSingleClassName('div', 'product-description')[0])))->getElementsByTagName('ul')[0];
	$descriptionFragments = (new HTMLParser(HTMLParser::getElementHTML($descriptionHolder)))->getElementsByTagName('li');

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = $base . HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n";
	foreach ($descriptionFragments as $descriptionFragment) {
		$result["comment"] .= "\n";
		$result["comment"] .= HTMLParser::getElementText($descriptionFragment);
	}

	return $result;
}

function get_elza($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing elza " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents(str_replace('#pageNumber#', '1', $link));

	$document = new HTMLParser($data);

	$paginations = $document->getElementsByTagAndSingleClassName('ul', 'pages');

	if (count($paginations)) {
		$pagination = $paginations[0];
		$pageListItems = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('li');
		$numberOfPages = count($pageListItems) - 1;
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents(str_replace('#pageNumber#', $i, $link));

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'buybtngrey');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = $base . '/' . HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_elza($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_elza($link) {
	echo "procesing elza: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('div', 'pricenova')[0];
	$imageHolder = $document->getElementById('mainpic');
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('div', 'deskr')[0];
	$descriptionFragments = (new HTMLParser(HTMLParser::getElementHTML($descriptionHolder)))->getElementsByTagName('div');

	$result["post_price"] = number_format(floatval(xmlstr(HTMLParser::getElementText($priceContainer), 'Нова цена:', 'лв.')), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	for ($i = 1; $i < count($descriptionFragments) - 2; $i++) {
		$result["comment"] .= "\n";
		$result["comment"] .= HTMLParser::getElementText($descriptionFragments[$i]);
	}

	return $result;
}

function get_finestini($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing finestini " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link);

	$document = new HTMLParser($data);

	$paginationContainers = $document->getElementsByTagAndSingleClassName('ul', 'pagination');
	if (count($paginationContainers)) {
		$pages = (new HTMLParser(HTMLParser::getElementHTML($paginationContainers[0])))->getElementsByTagName('li');
		$numberOfPages = (int) HTMLParser::getElementText($pages[count($pages) - 2]);
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('a', 'product_img_link');

		foreach ($items as $index => $item) {
			$temp_data = array();

			$temp_data['url'] = HTMLParser::getElementAttribute($item, 'href');
			$temp_data = array_merge($temp_data, process_finestini($temp_data['url'], $base));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_finestini($link, $base) {
	echo "procesing finestini: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementById('our_price_display');
	$imageHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementById('view_full_size'))))->getElementsByTagName('img')[0];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementById('short_description_content');
	$descriptionFragments = (new HTMLParser(HTMLParser::getElementHTML($descriptionHolder)))->getElementsByTagName('p');

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = $base . HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n";
	foreach ($descriptionFragments as $descriptionFragment) {
		$result["comment"] .= "\n";
		$result["comment"] .= HTMLParser::getElementText($descriptionFragment);
	}

	return $result;
}

function get_laleto($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing laleto " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1-150');

	$document = new HTMLParser($data);

	$pageCounterContainers = $document->getElementsByTagAndSingleClassName('span', 'page-counter');

	if (count($pageCounterContainers)) {
		$pageCounterText = HTMLParser::getElementText($pageCounterContainers[0]);
		$pageCounterTextFragments = explode(' ', $pageCounterText);
		$numberOfPages = (int) $pageCounterTextFragments[count($pageCounterTextFragments) - 1];
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {

		$data = file_get_contents($link . (150 * ($i - 1) + 1) . '-' . (150 * $i));

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'product-item');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$linkHolder = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0];

			$temp_data['url'] = $base . HTMLParser::getElementAttribute($linkHolder, 'href');
			$temp_data = array_merge($temp_data, process_laleto($temp_data['url'], $base));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_laleto($link, $base) {
	echo "procesing laleto: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('span', 'PricesalesPrice')[0];
	$imageHolder = $document->getElementById('product-image-gallery');
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolders = $document->getElementsByTagAndSingleClassName('div', 'product-short-desc-cont');
	$descriptionHolder = count($descriptionHolders) ? $descriptionHolders[0] : false;

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = $base . HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= ($descriptionHolder ? HTMLParser::getElementText($descriptionHolder) : '');

	return $result;
}

function get_popolo($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing popolo " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents(str_replace('#pageNumber#', '1', $link));

	$document = new HTMLParser($data);

	$paginations = $document->getElementsByTagAndSingleClassName('ul', 'pagination_top');

	if (count($paginations)) {
		$pagination = $paginations[0];
		$pageListItems = (new HTMLParser(HTMLParser::getElementHTML($pagination)))->getElementsByTagName('li');
		$lastPageListItem = $pageListItems[count($pageListItems) - 1];
		$lastPageLink = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($lastPageListItem)))->getElementsByTagName('a')[0], 'href');
		$lastPageLinkFragments = explode('/', $lastPageLink);
		$numberOfPages = (int) $lastPageLinkFragments[count($lastPageLinkFragments) - 2];
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents(str_replace('#pageNumber#', $i, $link));

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'product');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$linkHolder = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0];
			$imageHolder = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagAndSingleClassName('img', 'product_image')[0];

			$temp_data['url'] = $base . HTMLParser::getElementAttribute($linkHolder, 'href');
			$temp_data['image_url'] = $base . HTMLParser::getElementAttribute($imageHolder, 'src');
			$temp_data = array_merge($temp_data, process_popolo($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_popolo($link) {
	echo "procesing popolo: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$priceContainer = $document->getElementsByTagAndSingleClassName('span', 'price')[0];
	$titleHolder = $document->getElementsByTagAndSingleClassName('h2', 'page_heading')[0];
	$descriptionHolder = $document->getElementById('singleItemOptions');

	$result["post_price"] = number_format(floatval(str_replace('\'', '.', explode(' ', HTMLParser::getElementText($priceContainer))[1])), 2);
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$commentRaw = preg_replace('/<a.*<\/b>/', '', HTMLParser::getElementHTML($descriptionHolder));
	$commentRaw = '<form' . xmlstr($commentRaw, '<form', '<select') . '</form>';
	$commentRaw = str_replace(array('<b>', '</b>', '</span>'), array('', "\n", "</span>\n"), $commentRaw);
	$commentContainer = (new HTMLParser($commentRaw))->getElementsByTagName('form')[0];

	$result["comment"] .= HTMLParser::getElementText($commentContainer);

	return $result;
}

function get_shopping_terapia($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing shopping terapia " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1.html');

	$document = new HTMLParser($data);

	$pagesContainers = $document->getElementsByTagAndSingleClassName('div', 'pages');
	if (count($pagesContainers)) {
		$pagesContainer = $pagesContainers[0];
		$numberOfPages = count((new HTMLParser(HTMLParser::getElementHTML($pagesContainer)))->getElementsByTagName('li'));
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i . '.html');

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('table', 'one-product');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$linkHolder = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0];

			$temp_data['url'] = HTMLParser::getElementAttribute($linkHolder, 'href');
			$temp_data = array_merge($temp_data, process_shopping_terapia($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_shopping_terapia($link) {
	echo "procesing shopping terapia: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$imageHolder = $document->getElementById('zoom1');
	$priceContainer = $document->getElementsByTagAndSingleClassName('p', 'view-price')[0];
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('div', 'prod-desc')[0];

	$result["post_price"] = number_format(floatval(str_replace('\'', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);

	return $result;
}

function get_alexandra($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "\n"."procesing alexandra " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$pagesTextContainer = $document->getElementsByTagAndSingleClassName('div', 'results')[0];
	$numberOfPages = (int) xmlstr(HTMLParser::getElementHTML($pagesTextContainer), 'Страници:', '</div>');

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);

		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('div', 'image');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$linkHolders = (new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a');

			if (count($linkHolders)) {
				$temp_data['url'] = HTMLParser::getElementAttribute($linkHolders[0], 'href');
				$temp_data = array_merge($temp_data, process_alexandra($temp_data['url']));
				$result[] = array_merge($temp_data, $init_data);
			}
		}
	}

	return $result;
}

function process_alexandra($link) {
	echo "\n"."procesing alexandra: \n" . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$imageHolder = $document->getElementById('image');
	$priceContainer = $document->getElementsByTagAndSingleClassName('span', 'price-new')[0];
	$titleHolder = $document->getElementsByTagAndSingleClassName('h1', 'heading-title')[0];
	$descriptionHolder = $document->getElementById('tab-description');

	$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceContainer))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= str_replace("\n\n", "\n", HTMLParser::getElementText($descriptionHolder));

	return $result;
}

function get_terranova($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing terranova " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link);

	$document = new HTMLParser($data);

	$paginators = $document->getElementsByTagAndSingleClassName('div', 'pages');

	if (count($paginators) === 0) {
		$numberOfPages = 1;
	} else {
		$pages = (new HTMLParser(HTMLParser::getElementHTML($paginators[0])))->getElementsByTagName('li');
		$numberOfPages = count($pages) - 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);
		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('li', 'item');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagAndSingleClassName('a', 'full_link')[0], 'href');
			$temp_data = array_merge($temp_data, process_terranova($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}
//	var_dump($result);
	return $result;
}

function process_terranova($link) {
	echo "\n"."procesing terranova: \n" . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$slideContainer = $document->getElementsByTagAndSingleClassName('div', 'swiper-slide')[0];
	$imageHolder = (new HTMLParser(HTMLParser::getElementHTML($slideContainer)))->getElementsByTagName('img')[0];
	$priceBox = $document->getElementsByTagAndSingleClassName('div', 'price-box')[0];
	$regularPriceContainers = (new HTMLParser(HTMLParser::getElementHTML($priceBox)))->getElementsByTagAndSingleClassName('span', 'regular-price');
	$specialPriceContainers = (new HTMLParser(HTMLParser::getElementHTML($priceBox)))->getElementsByTagAndSingleClassName('p', 'special-price');
	if (count($regularPriceContainers)) {
		$priceContainer = (new HTMLParser(HTMLParser::getElementHTML($regularPriceContainers[0])))->getElementsByTagAndSingleClassName('span', 'price')[0];
	} else {
		$priceContainer = (new HTMLParser(HTMLParser::getElementHTML($specialPriceContainers[0])))->getElementsByTagAndSingleClassName('span', 'price')[0];
	}
	$titleHolder = $document->getElementsByTagName('h1')[0];
	$descriptionHolder = $document->getElementsByTagAndSingleClassName('span', 'short-description')[0];

	$result["post_price"] = number_format(floatval(str_replace(',', '.', substr(HTMLParser::getElementText($priceContainer), 2))), 2);
	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'src');
	$result["comment"] = HTMLParser::getElementText($titleHolder) . "\n\n";
	$result["comment"] .= HTMLParser::getElementText($descriptionHolder);
	return $result;
}

function get_fashindays($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing fashiondays " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link);

	$document = new HTMLParser($data);

	$paginators = $document->getElementsByTagAndSingleClassName('ul', 'pagination');
	if (count($paginators) === 0) {
		$numberOfPages = 1;
	} else {
		$pages = (new HTMLParser(HTMLParser::getElementHTML($paginators[0])))->getElementsByTagName('li');
		$numberOfPages = count($pages) - 2;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = "";
		$data = file_get_contents($link . $i);
		if ($data != "") {
			$document = new HTMLParser($data);
			$items = $document->getElementsByTagAndSingleClassName('a', 'campaign-item');
			foreach ($items as $index => $item) {
				$temp_data = array();
				$temp_data['url'] = HTMLParser::getElementAttribute($item, 'href');
				$n_temp_data = process_fashiondays($temp_data['url']);
				if ($n_temp_data != false && is_array($n_temp_data)) {
					$temp_data = array_merge($temp_data, $n_temp_data);
					$result[] = array_merge($temp_data, $init_data);
				}
			}
		}
	}
	return $result;
}

function process_fashiondays($link) {
	echo "\n procesing fashiondays: \n" . $link . "\n";

	$result = array();
	$data = "";
	$data = file_get_contents($link);
	if ($data != "") {
		$document = new HTMLParser($data);
		$priceHolder = $document->getElementsByTagAndSingleClassName('span', 'new-price')[0];
		$brandNameHolder = $document->getElementsByTagAndSingleClassName('div', 'product-brand-name')[0];
		$brandDescriptionHolder = $document->getElementsByTagAndSingleClassName('h1', 'product-brand-desc')[0];
		$descriptionHolder = $document->getElementById('content-product_details');

		$result["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceHolder))), 2);
		$result['image_url'] = xmlstr($data, '"image"     : "', '",');
		$result["comment"] = HTMLParser::getElementText($brandNameHolder) . "\n" . HTMLParser::getElementText($brandDescriptionHolder) . "\n\n";
		$result["comment"] .= HTMLParser::getElementText((new HTMLParser(str_replace('<br>', "\n", HTMLParser::getElementHTML($descriptionHolder))))->getElementById('content-product_details'));
		return $result;
	} else {
		return false;
	}
}

function get_mymall($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing mymall " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link);

	$document = new HTMLParser($data);

	$paginations = $document->getElementsByTagAndClassName('ul', 'PagingList');

	if (count($paginations)) {
		$pages = (new HTMLParser(HTMLParser::getElementHTML($paginations[0])))->getElementsByTagName('li');
		$numberOfPages = (int) HTMLParser::getElementText($pages[count($pages) - 1]);
	} else {
		$numberOfPages = 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);
		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndSingleClassName('li', 'product');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$rawPrice = explode(' ', HTMLParser::getElementText((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagAndSingleClassName('div', 'price')[0]));
			$rawPrice = array_pop($rawPrice);

			$temp_data["post_price"] = number_format(floatval($rawPrice), 2);
			$temp_data['url'] = HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagName('a')[0], 'href');
			$temp_data = array_merge($temp_data, process_mymall($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_mymall($link) {
	echo "procesing mymall: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link);
	$document = new HTMLParser($data);

	$topTitleHolder = $document->getElementsByTagName('h1')[0];
	$bottomTitleHolder = $document->getElementsByTagAndSingleClassName('div', 'ProductDescriptionContainer')[0];

	$result['image_url'] = xmlstr($data, '"zoomImage":"', '"}');
	$result["comment"] = (HTMLParser::getElementText($topTitleHolder)) . "\n\n" . HTMLParser::getElementText($bottomTitleHolder);

	return $result;
}

function get_ivet($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing ivet " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '1');

	$document = new HTMLParser($data);

	$pages = $document->getElementsByTagAndClassName('span', 'pager-count')[0];
	$numberOfPages = (int) explode(' ', HTMLParser::getElementText($pages))[3];

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . $i);
		$document = new HTMLParser($data);
		$items = $document->getElementsByTagAndClassName('div', 'product-image-bg');

		foreach ($items as $index => $item) {
			$temp_data = array();
			$temp_data['url'] = $base . HTMLParser::getElementAttribute((new HTMLParser(HTMLParser::getElementHTML($item)))->getElementsByTagAndClassName('a', 'product-image-link')[0], 'href');
			$temp_data = array_merge($temp_data, process_ivet($temp_data['url']));
			$result[] = array_merge($temp_data, $init_data);
		}
	}

	return $result;
}

function process_ivet($link) {
	echo "procesing ivet: " . $link . "\n";

	$result = array();
	$data = file_get_contents($link."?CurrencyID=7");
	$document = new HTMLParser($data);
	$imageHolder = $document->getElementById('product-zoom');
	$topTitleHolder = $document->getElementsByTagAndSingleClassName('h1', 'title-right')[0];
	$bottomTitleHolderV1 = $document->getElementsByTagAndSingleClassName('h2', 'product-description')[0];
	$bottomTitleHolderV2 = $bottomTitleHolderV1->parentNode;
	$bottomTitleText = HTMLParser::getElementText($bottomTitleHolderV1) ? HTMLParser::getElementText($bottomTitleHolderV1) : HTMLParser::getElementText($bottomTitleHolderV2);
	
//	$priceHolder = (new HTMLParser(HTMLParser::getElementHTML($document->getElementById('ProductPricesHolder'))))->getElementsByTagAndSingleClassName('span', 'price-value')[0];
	$priceHolder = $document->getElementsByTagAndAttribute('span', 'itemprop','price')[0];

	$result['image_url'] = HTMLParser::getElementAttribute($imageHolder, 'href');
	$result["comment"] = (HTMLParser::getElementText($topTitleHolder)) . "\n\n" . (str_replace(array('<p>', '</p>', 'p>'), '', $bottomTitleText));
	$result["post_price"] = number_format(floatval(HTMLParser::getElementText($priceHolder)), 2);
	return $result;
}

function get_urbanize($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "procesing urbanize " . "\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '&p=');

	$document = new HTMLParser($data);
	$pages = $document->getElementsByTagAndClassName('div', 'pages');
	if (count($pages) > 0) {
		$pages = (new HTMLParser(HTMLParser::getElementHTML($pages[0])))->getElementsByTagName('ol');
		$pages = (new HTMLParser(HTMLParser::getElementHTML($pages[0])))->getElementsByTagName('li');
	} else {
		$pages = false;
	}

	$numberOfPages = 1;

	if ($pages && count($pages) > 0) {
		$numberOfPages = count($pages) - 1;
	}

	$result = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . '&p=' . $i);
		$document = new HTMLParser($data);
		$items = $document->getElementsByTagAndClassName('li', 'item');
		foreach ($items as $item) {
			$itemDocument = new HTMLParser(HTMLParser::getElementHTML($item));
			$image = $itemDocument->getElementsByTagName('img')[0];
			$productManufacturer = $itemDocument->getElementsByTagAndClassName('a', 'product-manufacturer')[0];
			$productNameHolder = new HTMLParser(HTMLParser::getElementHTML($itemDocument->getElementsByTagAndClassName('h2', 'product-name')[0]));
			$priceHolder = new HTMLParser(HTMLParser::getElementHTML($itemDocument->getElementsByTagAndClassName('p', 'special-price')[0]));

			$temp_data = array();
			$temp_data['image_url'] = HTMLParser::getElementAttribute($image, 'src');
			$temp_data["comment"] = HTMLParser::getElementText($productManufacturer) . "\n" . HTMLParser::getElementText($productNameHolder->getElementsByTagName('a')[0]);
			$temp_data["post_price"] = number_format(floatval(str_replace(',', '.', HTMLParser::getElementText($priceHolder->getElementsByTagAndClassName('span', 'price')[0]))), 2);
			$temp_data["url"] = HTMLParser::getElementAttribute($productManufacturer, 'href');

			$result[] = array_merge($temp_data, $init_data);
		}
	}
	return $result;
}

function get_hm($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$result = array();
	$data_prim = file_get_contents($link);
	$data_prim = explode('<div class="grid col-3">', $data_prim);
	for ($i = 0; $i < 3; $i++) {
		unset($data_prim[count($data_prim) - 1]);
	}
	unset($data_prim[0]);
	unset($data_prim[1]);
	foreach ($data_prim as $item_data) {
		$temp_data = array();
		$temp_data["image_url"] = "http:" . xmlstr($item_data, 'src="', '"');
		$temp_data["comment"] = xmlstr($item_data, 'alt="', '"');
		$temp_data["post_price"] = clean_price(trim(str_ireplace("лв", "", xmlstr($item_data, '<div class="product-item-price">', '<'))));
		$temp_data["url"] = $base . xmlstr($item_data, '<a href="', '"');
		$result[] = array_merge($temp_data, $init_data);
	}
	foreach ($result as $res_key => $res) {
		$temp_res = process_hm($res["url"]);
		$result[$res_key] = array_merge($result[$res_key], $temp_res);
	}
	return $result;
}

function process_hm($link) {
	echo "procesing hm: " . $link . "\n";
	$result = array();
	$data = file_get_contents($link);
	$comment = trim(xmlstr($data, 'itemprop="description">', '</p>'));
	$result["comment"] = $comment;
	return $result;
}

function get_bless($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$result = array();
	$data_prim = curl_browser($link);
	$data_prim = xmlstr($data_prim, '<div id="GridView"', '<div id="MultiView"');
	$data_prim = explode('FlexibleThumbBrowseV1Pic', $data_prim);
	unset($data_prim[0]);
	foreach ($data_prim as $item_data) {
		$temp_data = array();
		$temp_data["url"] = $base . xmlstr($item_data, '<a href="', '"');
		$result[] = array_merge($temp_data, $init_data);
	}
	$final = array();
	foreach ($result as $res_key => $res) {
		$result = process_bless($res["url"], $base);
		if ($result!==false) {
			$final[] = array_merge($res, $result);
		}
	}
	return $final;
}

function process_bless($link) {
	echo "procesing bless: " . $link . "\n";
	$result = array();
	$data = curl_browser($link);
	$comment = trim(xmlstr($data, 'FlexibleProductDetailProductName', '</div>'));
	$comment = trim(xmlstr($comment, '<h1>', '</h1>'));
	$result["comment"] = $comment . "\n\n";
	$comment = trim(xmlstr($data, '<div class="manufacturer">', '</div>'));
	while (stristr($comment, "<")) {
		$comment = del_from_content($comment, "<", ">");
	}
	$result["comment"] .= $comment;
	$price = clean_price(trim(xmlstr($data, '<span class="PricesalesPrice" >', 'лв')));
	$result["post_price"] = $price;
	$image = xmlstr($data, 'og:image" content="', '"');
	$result["image_url"] = $image;
	if (trim($price)!="" && trim($price)!=0) {
		return $result;
	} else {
		return false;
	}
}

function get_koketna($init_data) {
	//INIT DATA
	//link
	//sex
	//brand
	//tags
	//categories
	//usersFK || users_selectorFK
	//currency
	echo "\n procesing koketna " . "\n\n";
	$link = $init_data["link"];
	$base = $init_data["base"];
	unset($init_data["base"]);
	unset($init_data["link"]);

	$data = file_get_contents($link . '/1');

	$document = new HTMLParser($data);
	echo "get pages\n";
	$numberOfPages = $document->getElementsByTagAndClassName('span', 'lastPage');
	$numberOfPages = $numberOfPages[0]->nodeValue;

	$result_links = array();

	for ($i = 1; $i <= $numberOfPages; $i++) {
		$data = file_get_contents($link . '/' . $i);
		echo "koketna get page " . $i . " of " . $numberOfPages . "\n\n";
		$document = new HTMLParser($data);

		$items = $document->getElementsByTagAndClassName('a', 'productItemLink');
		foreach ($items as $item) {
			$temp_data = array();
			$itemDocument = new HTMLParser(HTMLParser::getElementHTML($item));

			$image = $itemDocument->getElementsByTagAndClassName('img', 'firstImg');
			if (!isset($image[0])) {
				$image = $itemDocument->getElementsByTagName('img');
			}
			$temp_data['image_url'] = HTMLParser::getElementAttribute($image[0], 'src');
//			echo $temp_data['image_url']."\n\n";
//			$temp_data['image_url'] = HTMLParser::getElementAttribute($image[0], 'src');
//			$image = $itemDocument->getElementsByTagName('img')[1];
//			$temp_data['image_url'] = HTMLParser::getElementAttribute($image, 'src');
			//productBrand
			$productTitle = trim($itemDocument->getElementsByTagAndClassName('div', 'productBrand')[0]->textContent);
			$temp_data["comment"] = $productTitle . "\n";
			$productDescription = trim(clean_spaces(trim($itemDocument->getElementsByTagAndClassName('span', 'productTitle')[0]->textContent)));
			$temp_data["comment"] .= $productDescription;
			$price_array = $itemDocument->getElementsByTagAndClassName('span', 'price');
			if (isset($price_array[0])) {
				$priceHolder = clean_price(trim($price_array[0]->textContent));
			} else {
				$price_array = $itemDocument->getElementsByTagAndClassName('span', 'priceNoPromo');
				$priceHolder = clean_price(trim($price_array[0]->textContent));
			}
			$temp_data["post_price"] = $priceHolder;
			$temp_data["url"] = $base . HTMLParser::getElementAttribute($item, 'href');
			$more_description = process_koketna($temp_data["url"], "[page " . $i . " of " . $numberOfPages . "]");
			if ($more_description != "") {
				$temp_data["comment"] .="\n" . $more_description;
			}
			$result[] = array_merge($temp_data, $init_data);
//			die(print_r($result));
		}
	}
	return $result;
}

function process_koketna($link, $page_text = "") {
	echo "koketna " . $page_text . " getting description: \n" . $link . "\n\n";
	$data = file_get_contents($link);
	$document = new HTMLParser($data);
	$descriptionHolder = $document->getElementsByTagAndClassName('span', 'description');
	if (isset($descriptionHolder[0])) {
		$description = trim($descriptionHolder[0]->nodeValue);
		$description = str_ireplace(array("<p>", "</p>"), array("", "\n"), $description);
		return $description;
	} else {
		return "";
	}
}

?>
